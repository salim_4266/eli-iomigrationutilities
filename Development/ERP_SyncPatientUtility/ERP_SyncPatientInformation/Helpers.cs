﻿using GlobalPortal.Api.Client.ClientAuthentication;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_SyncPatientInformation
{
    internal class Helpers
    {
        public Helpers()
        {

        }

        public static string _ConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["PracticeRepository"].ToString();
            }
        }

        public static string _SyncUrl
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["SyncUrl"].ToString();
            }
        }

        private static string SyncUserName { get; set; }
        private static string SyncPassword { get; set; }

        public static IServerAuthentication GetAuthenticator()
        {
            var username = SyncUserName;
            var password = SyncPassword;
            return GlobalPortalRestClient.GetAuthenticator(username, password, Helpers._SyncUrl);
        }

        internal static bool SetCredentials()
        {
            bool retStatus = false;
            try
            {
                string _Query = "Select SynchronizationUserName, SynchronizationPassword From MVE.PP_AccountDetails Where IsActive = 1";
                using (SqlConnection Con = new SqlConnection(Helpers._ConnectionString))
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand(_Query, Con);
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        retStatus = true;
                        SyncUserName = rdr["SynchronizationUserName"].ToString();
                        SyncPassword = rdr["SynchronizationPassword"].ToString();
                    }
                    rdr.Close();
                    Con.Close();
                }
            }
            catch (Exception ee)
            {
                SyncUserName = "";
                SyncPassword = "";
                retStatus = false;
            }
            return retStatus;
        }
    }
}
