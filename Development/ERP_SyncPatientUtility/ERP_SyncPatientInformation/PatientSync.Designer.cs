﻿namespace ERP_SyncPatientInformation
{
    partial class PatientSync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTotalPatients = new System.Windows.Forms.Label();
            this.lblProcessedPatients = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRemainingPatients = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblSyncMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(40, 236);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(439, 33);
            this.button1.TabIndex = 0;
            this.button1.Text = "Click here to start the synchronization";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Total Patients";
            // 
            // lblTotalPatients
            // 
            this.lblTotalPatients.AutoSize = true;
            this.lblTotalPatients.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPatients.Location = new System.Drawing.Point(197, 116);
            this.lblTotalPatients.Name = "lblTotalPatients";
            this.lblTotalPatients.Size = new System.Drawing.Size(19, 21);
            this.lblTotalPatients.TabIndex = 2;
            this.lblTotalPatients.Text = "0";
            // 
            // lblProcessedPatients
            // 
            this.lblProcessedPatients.AutoSize = true;
            this.lblProcessedPatients.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcessedPatients.Location = new System.Drawing.Point(198, 152);
            this.lblProcessedPatients.Name = "lblProcessedPatients";
            this.lblProcessedPatients.Size = new System.Drawing.Size(19, 21);
            this.lblProcessedPatients.TabIndex = 4;
            this.lblProcessedPatients.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "Processed 2 ERP";
            // 
            // lblRemainingPatients
            // 
            this.lblRemainingPatients.AutoSize = true;
            this.lblRemainingPatients.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemainingPatients.Location = new System.Drawing.Point(198, 192);
            this.lblRemainingPatients.Name = "lblRemainingPatients";
            this.lblRemainingPatients.Size = new System.Drawing.Size(19, 21);
            this.lblRemainingPatients.TabIndex = 6;
            this.lblRemainingPatients.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(36, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 21);
            this.label6.TabIndex = 5;
            this.label6.Text = "Remaining Patients";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(34, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(290, 21);
            this.label7.TabIndex = 7;
            this.label7.Text = "Utility to sync patient information to erp.";
            // 
            // lblSyncMessage
            // 
            this.lblSyncMessage.AutoSize = true;
            this.lblSyncMessage.Font = new System.Drawing.Font("Segoe UI Emoji", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSyncMessage.Location = new System.Drawing.Point(37, 58);
            this.lblSyncMessage.Name = "lblSyncMessage";
            this.lblSyncMessage.Size = new System.Drawing.Size(0, 17);
            this.lblSyncMessage.TabIndex = 9;
            // 
            // PatientSync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(543, 338);
            this.Controls.Add(this.lblSyncMessage);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblRemainingPatients);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblProcessedPatients);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblTotalPatients);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "PatientSync";
            this.Text = "Patient Sync";
            this.Load += new System.EventHandler(this.PatientSync_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTotalPatients;
        private System.Windows.Forms.Label lblProcessedPatients;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRemainingPatients;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblSyncMessage;
    }
}

