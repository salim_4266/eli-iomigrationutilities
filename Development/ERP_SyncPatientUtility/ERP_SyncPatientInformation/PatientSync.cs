﻿using System;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_SyncPatientInformation
{
    public partial class PatientSync : Form
    {
        private long TotalPatient = 0;

        private long ProcessedPatient = 0;

        private long RemainingPatient = 0;

        public PatientSync()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int totalPatient = Convert.ToInt32(Patient.getPatientCount());
            int processedPatient = Convert.ToInt32(Patient.getProcessedCount());
            lblTotalPatients.Text = totalPatient.ToString();
            lblProcessedPatients.Text = processedPatient.ToString();
            lblRemainingPatients.Text = (totalPatient - processedPatient).ToString();
            if (Helpers._ConnectionString != "" && Helpers._SyncUrl != "")
            {
                sendPatientInformation();
            }
            else
            {
                lblSyncMessage.ForeColor = System.Drawing.Color.Red;
                lblSyncMessage.Text = "There is some issue with the configuration file, Please check the connection string as well as url.";
            }
        }

        private async void sendPatientInformation()
        {
            button1.Enabled = false;
            bool status = true;
            while (status)
            {
                DataTable dtPatientList = Patient.getPatientList();
                if (dtPatientList.Rows.Count > 0)
                {
                    var progressHandler = new Progress<string>(value =>
                    {
                        if (!string.IsNullOrEmpty(value))
                        {
                            lblProcessedPatients.Text = value.Split('~')[0];
                            lblRemainingPatients.Text = value.Split('~')[1];
                        }
                    });
                    var progress = progressHandler as IProgress<string>;
                    await Task.Run(() =>
                    {
                        foreach (DataRow dr in dtPatientList.Rows)
                        {
                            Process2ERP.SyncPatient2ERP(dr);
                            if (progress != null)
                            {
                                if (TotalPatient == ProcessedPatient)
                                {
                                    lblSyncMessage.ForeColor = System.Drawing.Color.Green;
                                    lblSyncMessage.Text = "Patient synchornization is completed.";
                                    button1.Enabled = true;
                                    progress.Report(ProcessedPatient + "~" + RemainingPatient);
                                }
                                else
                                {
                                    ProcessedPatient = ProcessedPatient + 1;
                                    RemainingPatient = RemainingPatient - 1;
                                    progress.Report(ProcessedPatient + "~" + RemainingPatient);
                                }
                            }
                        }
                    });
                }
                else
                {
                    status = false;
                }
            }
        }

        private void PatientSync_Load(object sender, EventArgs e)
        {
            lblSyncMessage.Text = "";
            Helpers.SetCredentials();
            TotalPatient = Convert.ToInt32(Patient.getPatientCount());
            ProcessedPatient = Convert.ToInt32(Patient.getProcessedCount());
            RemainingPatient = TotalPatient - ProcessedPatient;
            lblTotalPatients.Text = TotalPatient.ToString();
            lblProcessedPatients.Text = ProcessedPatient.ToString();
            lblRemainingPatients.Text = RemainingPatient.ToString();
        }
    }
}
