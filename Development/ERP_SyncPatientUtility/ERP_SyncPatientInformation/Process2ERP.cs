﻿using GlobalPortal.Api.Client.Patients;
using GlobalPortal.Api.Models.Contacts;
using GlobalPortal.Api.Models.Patients;
using System;
using System.Collections.Generic;
using System.Data;

namespace ERP_SyncPatientInformation
{
    internal class Process2ERP
    {
        public static bool SyncPatient2ERP(DataRow dtRow)
        {
            bool returnStatus = false;
            string externalId = string.Empty;
            string patientId = string.Empty;
            string responseType = string.Empty;
            string errorMessage = string.Empty;
            try
            {
                if (dtRow["ExternalId"].ToString() != "")
                {
                    patientId = dtRow["ExternalId"].ToString();
                    PatientModel patientModel = CreatePatientModel(dtRow);
                    var success = PatientProvider.Save(Helpers.GetAuthenticator(), patientModel);
                    if (success != null)
                    {
                        responseType = "S";
                        errorMessage = "";
                        externalId = success.Id.ToString();
                    }
                    else
                    {
                        errorMessage = "ERP returns null model in response.";
                        externalId = "0";
                        responseType = "F";
                    }

                    // Will add in the Log
                    new PatientLog(patientId, externalId, responseType, errorMessage).Add();
                }
            }
            catch(Exception ee)
            {
                // Will add in the Log
                new PatientLog(patientId, externalId, "E", ee.ToString()).Add();
            }

            // Set to Default
            errorMessage = "";
            externalId = "";
            responseType = "";
            returnStatus = false;

            return returnStatus;
        }

        private static PatientModel CreatePatientModel(DataRow dtRow)
        {
            PatientModel model = new PatientModel();
            ContactModel contactmodel = new ContactModel();
            EmailAddressModel EAModelitem = new EmailAddressModel();
            PhoneNumberModel PhoneModelNos = new PhoneNumberModel();
            PostalAddressModel PostalAddressitem = new PostalAddressModel();
            List<string> listLocations = new List<string>();
            List<EmailAddressModel> EAModel = new List<EmailAddressModel>();
            List<PhoneNumberModel> PhoneModel = new List<PhoneNumberModel>();
            List<PostalAddressModel> PostalAddress = new List<PostalAddressModel>();
            try
            {
                if (dtRow != null)
                {
                    model.NextLogOnSecurityVerificationPatient = true;
                    model.NextLogOnSecurityVerificationRepresentative = true;
                    contactmodel.Id = Guid.NewGuid();
                    contactmodel.FirstName = dtRow["FirstName"].ToString();
                    contactmodel.LastName = dtRow["LastName"].ToString();
                    if (dtRow["MiddleName"].ToString() != "")
                    { contactmodel.MiddleName = dtRow["MiddleName"].ToString(); }
                    if (dtRow["Suffix"].ToString() != "")
                    { contactmodel.Suffix = dtRow["Suffix"].ToString(); }
                    if (dtRow["Prefix"].ToString() != "")
                    { contactmodel.Prefix = dtRow["Prefix"].ToString(); }
                    if (dtRow["FullName"].ToString() != "")
                    { contactmodel.FullName = dtRow["FullName"].ToString(); }
                    if (dtRow["CompanyName"].ToString() != "")
                    { contactmodel.CompanyName = dtRow["CompanyName"].ToString(); }
                    if (dtRow["JobTitle"].ToString() != "")
                    { contactmodel.JobTitle = dtRow["JobTitle"].ToString(); }

                    model.Contact = contactmodel;
                    listLocations.Add(dtRow["LocationID"].ToString());
                    model.Locations = listLocations;
                    model.Active = true;

                    if (dtRow["EmailAddresses"].ToString() != "")
                    {
                        EAModelitem.Address = dtRow["EmailAddresses"].ToString();
                        EAModelitem.Default = true;
                        EAModel.Add(EAModelitem);
                        model.Contact.EmailAddresses = EAModel;
                    }

                    if (dtRow["PhoneNumbers"].ToString() != "")
                    {
                        PhoneModelNos.Number = dtRow["PhoneNumbers"].ToString();
                        PhoneModel.Add(PhoneModelNos);
                        model.Contact.PhoneNumbers = PhoneModel;
                    }

                    if (dtRow["Address1"].ToString() != "")
                    { PostalAddressitem.Address1 = dtRow["Address1"].ToString(); }
                    if (dtRow["Address2"].ToString() != "")
                    { PostalAddressitem.Address2 = dtRow["Address2"].ToString(); }
                    if (dtRow["City"].ToString() != "")
                    { PostalAddressitem.City = dtRow["City"].ToString(); }
                    if (dtRow["State"].ToString() != "")
                    { PostalAddressitem.State = dtRow["State"].ToString(); }
                    if (dtRow["Country"].ToString() != "")
                    { PostalAddressitem.CountryName = dtRow["Country"].ToString(); }
                    if (dtRow["Zip"].ToString() != "")
                    { PostalAddressitem.Zip = dtRow["Zip"].ToString(); }
                    PostalAddressitem.Default = true;
                    PostalAddress.Add(PostalAddressitem);
                    model.Contact.PostalAddresses = PostalAddress;

                    model.Birthday = Convert.ToDateTime(dtRow["Birthdate"] == DBNull.Value ? "" : dtRow["Birthdate"]);
                    if (dtRow["Notes"].ToString() != "")
                    {
                        model.Notes = dtRow["Notes"] == DBNull.Value ? "" : dtRow["Notes"].ToString();
                    }

                    if (dtRow["LanguageName"].ToString() != "")
                    {
                        model.LanguageName = Convert.ToString(dtRow["LanguageName"] == DBNull.Value ? "" : dtRow["LanguageName"]);
                    }

                    model.Active = true;
                    model.ExternalId = Convert.ToString(dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"]); ;
                    model.ContactLensesReorderDate = null;
                    model.ContactLensesReorderName = "";
                    model.RecallDate1 = null;
                    model.RecallDate2 = null;
                    model.AllowEmailReminders = false;
                    model.AllowEmailBirthday = false;
                    model.AllowEmailThankYou = false;
                    model.AllowEmailEyeWear = false;
                    model.AllowEmailPromotions = false;
                    model.AllowSmsReminders = false;
                    model.AllowSmsBirthday = false;
                    model.AllowSmsThankYou = false;
                    model.AllowSmsEyewear = false;
                    model.AllowSmsPromotions = false;
                    model.AllowVoiceReminders = false;
                    model.AllowVoiceBirthday = false;
                    model.AllowVoiceThankYou = false;
                    model.AllowVoiceEyewear = false;
                    model.AllowVoicePromotions = false;
                    model.InvitedToPortal = true;
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
    }
}
