﻿using System.Data;
using System.Data.SqlClient;

namespace ERP_SyncPatientInformation
{
    internal class Patient
    {
        public static DataTable getPatientList()
        {
            DataTable dtTable = new DataTable();
            try
            {
                string _selectQry = "Exec [MVE].[PatientQueue4ERP2Sync]";
                using (SqlConnection con = new SqlConnection(Helpers._ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                    {
                        using(SqlDataAdapter Adapter = new SqlDataAdapter(cmd))
                        {
                            Adapter.Fill(dtTable);
                        }
                    }
                }
            }
            catch
            {

            }
            return dtTable;
        }

        public static string getPatientCount()
        {
            string patientCount = "0";
            try
            {
                string _selectQry = "Select Count(Id) as TotalPatients From model.Patients";
                using (SqlConnection con = new SqlConnection(Helpers._ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                    {
                        SqlDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            patientCount = rdr["TotalPatients"].ToString();
                        }
                    }
                    con.Close();
                }
            }
            catch
            {

            }
            return patientCount;
        }

        public static string getProcessedCount()
        {
            string processedCount = "0";
            try
            {
                string _selectQry = "Select Count(PatientId) as ProcessedPatients From PP_PatientSync2ERP";
                using (SqlConnection con = new SqlConnection(Helpers._ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                    {
                        SqlDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            processedCount = rdr["ProcessedPatients"].ToString();
                        }
                    }
                    con.Close();
                }
            }
            catch
            {

            }
            return processedCount;
        }
    }
}
