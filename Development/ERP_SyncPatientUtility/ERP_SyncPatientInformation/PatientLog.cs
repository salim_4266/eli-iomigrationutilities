﻿using System;
using System.Data.SqlClient;

namespace ERP_SyncPatientInformation
{
    internal class PatientLog
    {
        internal PatientLog(string patientId, string externalId, string responseType, string errorMessage)
        {
            this.PatientId = patientId;
            this.ExternalId = externalId;
            this.ResponseType = responseType;
            this.ExceptionMessage = errorMessage;
            this.CreatedDate = DateTime.Now.ToString();
        }

        public string PatientId { get; set; }

        public string ExternalId { get; set; }

        public string ResponseType { get; set; }

        public string ExceptionMessage { get; set; }

        public string CreatedDate { get; set; }

        public void Add()
        {
            string qry = "Insert into PP_PatientSync2ERP(PatientId, ExternalId, ResponseType, ExceptionMessage, CreatedDate) Values ('" + this.PatientId + "', '" + this.ExternalId + "', '" + this.ResponseType + "', '" + this.ExceptionMessage + "', GETDATE())";
            using (SqlConnection Con = new SqlConnection(Helpers._ConnectionString))
            {
                Con.Open();
                using (SqlCommand cmd = new SqlCommand(qry, Con))
                {
                    cmd.ExecuteNonQuery();
                }
                Con.Close();
            }
        }
    }
}
