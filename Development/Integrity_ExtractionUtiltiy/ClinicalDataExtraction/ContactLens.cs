﻿using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
	public class ContactLens
	{
		public static void Extract(string _appointmentId)
		{
			try
			{
				string FindingDetail = "";
				string Comment = "";
				string _selectQry = "Select FindingDetail, ImageDescriptor from dbo.PatientClinical with (nolock) Where AppointmentId = '" + _appointmentId + "' and ClinicalType = 'A' and FindingDetail Like 'DISPENSE CL%' and Status != 'D'";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_selectQry, con))
					{
						SqlDataReader rdr = cmd.ExecuteReader();
						if (rdr.Read())
						{
							FindingDetail = rdr["FindingDetail"].ToString();
							Comment = rdr["ImageDescriptor"].ToString();
						}
					}
					con.Close();
				}

				if (FindingDetail != "")
				{
					string OD_Sphere = "";
					string OS_Sphere = "";
					string OD_Cylinder = "";
					string OS_Cylinder = "";
					string OD_Axis = "";
					string OS_Axis = "";
					string OD_AddReading = "";
					string OS_AddReading = "";
					string OD_BaseCurve = "";
					string OS_BaseCurve = "";
					string OD_Diameter = "";
					string OS_Diameter = "";
					string OD_PeriphCurve = "";
					string OS_PeriphCurve = "";
					string OD_DORN = "";
					string OS_DORN = "";

					FindingDetail = FindingDetail.Replace("DISPENSE CL RX", "").Replace("BASE CURVE", "BASE_CURVE").Replace("PERIPH CURVE", "PERIPH_CURVE").Replace("D OR N", "D_OR_N");
					int startIndex = FindingDetail.IndexOf("-2/");
					int endIndex = FindingDetail.IndexOf("-3/");
					if (startIndex != 0 && endIndex != 0)
					{
						string OD_Findings = FindingDetail.Substring(startIndex + 3, (endIndex - startIndex) - 3);
						OD_Sphere = OD_Findings.Split(' ')[0];
						OD_Cylinder = OD_Findings.Split(' ')[1];
						OD_Axis = OD_Findings.Split(' ')[2];
						if (OD_Cylinder.Contains("ADD-READING") || OD_Cylinder.Contains("BASE_CURVE") || OD_Cylinder.Contains("DIAMETER") || OD_Cylinder.Contains("PERIPH_CURVE") || OD_Cylinder.Contains("D_OR_N"))
						{
							OD_Cylinder = string.Empty;
						}
						if(OD_Axis.Contains("ADD-READING") || OD_Axis.Contains("BASE_CURVE") || OD_Axis.Contains("DIAMETER") || OD_Axis.Contains("PERIPH_CURVE") || OD_Axis.Contains("D_OR_N"))
						{
							OD_Axis = string.Empty;
						}
						if (OD_Findings.Contains("ADD-READING"))
							OD_AddReading = OD_Findings.Substring(OD_Findings.IndexOf("ADD-READING:") + 12, 5);
						if (OD_Findings.Contains("BASE_CURVE"))
							OD_BaseCurve = OD_Findings.Substring(OD_Findings.IndexOf("BASE_CURVE:") + 11, 3);
						if (OD_Findings.Contains("DIAMETER:"))
							OD_Diameter = OD_Findings.Substring(OD_Findings.IndexOf("DIAMETER:") + 9, 2);
						if (OD_Findings.Contains("PERIPH_CURVE"))
							OD_PeriphCurve = OD_Findings.Substring(OD_Findings.IndexOf("PERIPH_CURVE:") + 13, 5);
						if (OD_Findings.Contains("D_OR_N"))
							OD_DORN = OD_Findings.Substring(OD_Findings.IndexOf("D_OR_N") + 7, 1);
					}
					startIndex = FindingDetail.IndexOf("-3/");
					endIndex = FindingDetail.IndexOf("-4/");
					if (startIndex != 0 && endIndex != 0)
					{
						string OS_Findings = FindingDetail.Substring(startIndex + 3, (endIndex - startIndex) - 3);
						OS_Sphere = OS_Findings.Split(' ')[0];
						OS_Cylinder = OS_Findings.Split(' ')[1];
						OS_Axis = OS_Findings.Split(' ')[2];
						if (OS_Cylinder.Contains("ADD-READING") || OS_Cylinder.Contains("BASE_CURVE") || OS_Cylinder.Contains("DIAMETER") || OS_Cylinder.Contains("PERIPH_CURVE") || OS_Cylinder.Contains("D_OR_N"))
						{
							OS_Cylinder = string.Empty;
						}
						if (OS_Axis.Contains("ADD-READING") || OS_Axis.Contains("BASE_CURVE") || OS_Axis.Contains("DIAMETER") || OS_Axis.Contains("PERIPH_CURVE") || OS_Axis.Contains("D_OR_N"))
						{
							OS_Axis = string.Empty;
						}
						if (OS_Findings.Contains("ADD-READING"))
							OS_AddReading = OS_Findings.Substring(OS_Findings.IndexOf("ADD-READING:") + 12, 5);
						if (OS_Findings.Contains("BASE_CURVE"))
							OS_BaseCurve = OS_Findings.Substring(OS_Findings.IndexOf("BASE_CURVE:") + 11, 3);
						if (OS_Findings.Contains("DIAMETER:"))
							OS_Diameter = OS_Findings.Substring(OS_Findings.IndexOf("DIAMETER:") + 9, 2);
						if (OS_Findings.Contains("PERIPH_CURVE"))
							OS_PeriphCurve = OS_Findings.Substring(OS_Findings.IndexOf("PERIPH_CURVE:") + 13, 5);
						if (OS_Findings.Contains("D_OR_N"))
							OS_DORN = OS_Findings.Substring(OS_Findings.IndexOf("D_OR_N") + 7, 1);
					}

					startIndex = FindingDetail.IndexOf("-5/");
					endIndex = FindingDetail.IndexOf("-6/");
					string OD_ModelId = FindingDetail.Substring(startIndex + 3, (endIndex - startIndex - 3));

					startIndex = FindingDetail.IndexOf("-6/");
					endIndex = FindingDetail.IndexOf("-7/");
					string OS_ModelId = FindingDetail.Substring(startIndex + 3, (endIndex - startIndex - 3));

					string OD_Model = "";
					string OS_Model = "";

					if (OD_ModelId.Trim() != "")
					{
						OD_Model = new CLInventory(OD_ModelId).ModelName;
					}

					if (OS_ModelId.Trim() != "")
					{
						OS_Model = new CLInventory(OS_ModelId).ModelName;
					}

					string insertQry = "Insert into ExtractedContactLensDetails(AppointmentId, OD_Sphere, OS_Sphere, OD_Cylinder, OS_Cylinder, OD_Axis, OS_Axis, OD_AddReading, OS_AddReading, OD_BaseCurve, OS_BaseCurve, OD_Diameter, OS_Diameter, OD_PeriphCurve, OS_PeriphCurve, OD_DORN, OS_DORN, OD_Comment, OD_Model, OS_Model) Values ('" + _appointmentId + "', '" + OD_Sphere.Replace("'", "''") + "', '" + OS_Sphere.Replace("'", "''") + "', '" + OD_Cylinder.Replace("'", "''") + "', '" + OS_Cylinder.Replace("'", "''") + "', '" + OD_Axis.Replace("'", "''") + "', '" + OS_Axis.Replace("'", "''") + "', '" + OD_AddReading.Replace("'", "''") + "', '" + OS_AddReading.Replace("'", "''") + "', '" + OD_BaseCurve.Replace("'", "''") + "', '" + OS_BaseCurve.Replace("'", "''") + "', '" + OD_Diameter.Replace("'", "''") + "', '" + OS_Diameter.Replace("'", "''") + "', '" + OD_PeriphCurve.Replace("'", "''") + "', '" + OS_PeriphCurve.Replace("'", "''") + "', '" + OD_DORN.Replace("'", "''") + "', '" + OS_DORN.Replace("'", "''") + "', '" + Comment.Replace("'", "''") + "', '" + OD_Model.Replace("'", "''") + "','" + OS_Model.Replace("'", "''") + "')";
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}

			}
			catch
			{

			}
		}

	}
}
