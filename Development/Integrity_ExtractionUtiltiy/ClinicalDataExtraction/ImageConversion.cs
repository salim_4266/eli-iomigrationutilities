﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace ClinicalDataExtraction
{
    public partial class ImageConversion : Form
    {
        public static string _path2pinpointDirectory { get; set; }

        public ImageConversion()
        {
            _path2pinpointDirectory = ConfigurationManager.AppSettings["ServerDathPath"].ToString();
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string path2ImageDirectory = _path2pinpointDirectory;
            string path2ConvertedImagesDirectory = _path2pinpointDirectory + @"ConvertedImages\";
            DirectoryInfo baseDiretcory = new DirectoryInfo(path2ImageDirectory);
            if (baseDiretcory.Exists)
            {
                if (!Directory.Exists(path2ConvertedImagesDirectory))
                {
                    Directory.CreateDirectory(path2ConvertedImagesDirectory);
                }
                foreach (FileInfo f in baseDiretcory.GetFiles())
                {
                    new FileConversion()
                    {
                        path2Current = f.FullName,
                        path2NewDirectory = path2ConvertedImagesDirectory,
                    }.Convert("image");
                }
                MessageBox.Show("Image conversion has been completed");
            }
            else
            {
                MessageBox.Show(path2ImageDirectory + "is not valid, Please check!");
            }
        }
    }
}
