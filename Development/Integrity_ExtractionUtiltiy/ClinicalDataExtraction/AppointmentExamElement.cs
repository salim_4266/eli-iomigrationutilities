﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
	public class AppointmentExamElement
	{
		public string AppointmentId { get; set; }
		public List<ExamElement> ElementDetail { get; set; }

	}

	public class AppointmentExtractionStatus
	{
		public string AppointmentId { get; set; }
		public string Status { get; set; }
		public string ExceptionMessage { get; set; }

		public void Save()
		{
			string _insertQry = "Insert into AppointmentExtractionStatus(AppointmentId, Status, ExceptionMessage, CreatedDate) Values ('" + this.AppointmentId + "', '" + this.Status + "', '" + this.ExceptionMessage.Replace("'", "''") + "', GETDATE())";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				con.Open();
				using (SqlCommand cmd = new SqlCommand(_insertQry, con))
				{
					cmd.ExecuteNonQuery();
				}
				con.Close();
			}
		}
	}

	public class ExamElement
	{
		public string Name { get; set; }

		public EyeContext Content { get; set; }

		public void Save(string _appointmentId)
		{
			#region First Phase 45 Elements

			if (Name == "/DILATION")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedDilationDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CATARACT")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCATARACTDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/AC PARACENT W DX ASP"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedAntChamberWDXDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/AC PARACENT W INJECTION"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedAntChamberWInjectionDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/AC PARACENT W REM OF BLOOD"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedAntChamberREMBLOODDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/AC PARACENT W THERAP"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedAntChamberWTherapyDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/CORNEAL ABRASION"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCorneaAbrasionDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/CORNEAL EPITH REMOVAL, CHELATION"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCORNEALEPITHREMOVALDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/CORNEAL EPITHELIAL REMOVAL"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCORNEALEPITHELIALREMOVALDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/CORNEAL FB REMOVAL"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCorneaFBRemovalDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/CORNEAL HYSTERESIS"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCorneaHYSTERSISDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CORNEAL TOPOGRAPHY")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCorneaTopographyDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CORNEAL TOPOGRAPHY, TECH")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCorneaTopographyTechDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CORNEAL TRANSPLANT")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCorneaTransplantDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CONJUNCTIVAL ADVANCEMENT")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCONJUNCTIVALAdvancementDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CONJUNCTIVAL FB REMOVAL")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedConjuctivalFBremovalDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CONJUNCTIVOPLASTY")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedCONJUNCTIVALPLASTYDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CONJUNCTIVA CHECK")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedConjunctivoCheckDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/GENERAL MEDICAL OBSERVATION")
			{
				string Time = "";
				string Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "typeValue")
					{
						Value = a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedGeneralMedicationObservationDetails(AppointmentId, Time, Value) Values ('" + _appointmentId + "', '" + Time + "', '" + Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/AUTOREFRACTION")
			{
				string Time = "";
				string OD_Sphere = "";
				string OS_Sphere = "";
				string OD_Cylinder = "";
				string OS_Cylinder = "";
				string OD_Axis = "";
				string OS_Axis = "";
				string OD_Value = "";
				string OS_Value = "";
				string OD_Va = "";
				string OS_Va = "";
				string OD_PD = "";
				string OS_PD = "";
				string OD_VisionNear = "";
				string OS_VisionNear = "";
				string OD_Reliability = "";
				string OS_Reliability = "";
				string Time1 = "";
				string OD_Sphere1 = "";
				string OS_Sphere1 = "";
				string OD_Cylinder1 = "";
				string OS_Cylinder1 = "";
				string OD_Axis1 = "";
				string OS_Axis1 = "";
				string OD_Value1 = "";
				string OS_Value1 = "";
				string OD_Va1 = "";
				string OS_Va1 = "";
				string OD_PD1 = "";
				string OS_PD1 = "";
				string OD_VisionNear1 = "";
				string OS_VisionNear1 = "";
				string OD_Reliability1 = "";
				string OS_Reliability1 = "";
				string OD_Comment = "";
				string OS_Comment = "";
				string OD_Comment1 = "";
				string OS_Comment1 = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						if (Time == "")
							Time = a.Value;
						else
							Time1 = a.Value;
					}
					else if (a.Key == "OD_Sphere")
					{
						if (Time1 != "")
							OD_Sphere1 = a.Value;
						else
							OD_Sphere = a.Value;
					}
					else if (a.Key == "OS_Sphere")
					{
						if (Time1 != "")
							OS_Sphere1 = a.Value;
						else
							OS_Sphere = a.Value;
					}
					else if (a.Key == "OD_Cylinder")
					{
						if (Time1 != "")
							OD_Cylinder1 = a.Value;
						else
							OD_Cylinder = a.Value;
					}
					else if (a.Key == "OS_Cylinder")
					{
						if (Time1 != "")
							OS_Cylinder1 = a.Value;
						else
							OS_Cylinder = a.Value;
					}
					else if (a.Key == "OD_Axis")
					{
						if (Time1 != "")
							OD_Axis1 = a.Value;
						else
							OD_Axis = a.Value;
					}
					else if (a.Key == "OS_Axis")
					{
						if (Time1 != "")
							OS_Axis1 = a.Value;
						else
							OS_Axis = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (Time1 != "")
							OD_Value1 = a.Value;
						else
							OD_Value = a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						if (Time1 != "")
							OS_Value1 = a.Value;
						else
							OS_Value = a.Value;
					}
					else if (a.Key == "OD_Va")
					{
						if (Time1 != "")
							OD_Va1 = a.Value;
						else
							OD_Va = a.Value;
					}
					else if (a.Key == "OS_Va")
					{
						if (Time1 != "")
							OS_Va1 = a.Value;
						else
							OS_Va = a.Value;
					}
					else if (a.Key == "OD_PD")
					{
						if (Time1 != "")
							OD_PD1 = a.Value;
						else
							OD_PD = a.Value;
					}
					else if (a.Key == "OS_PD")
					{
						if (Time1 != "")
							OS_PD1 = a.Value;
						else
							OS_PD = a.Value;
					}
					else if (a.Key == "OD_VisionNear")
					{
						if (Time1 != "")
							OD_VisionNear1 = a.Value;
						else
							OD_VisionNear = a.Value;
					}
					else if (a.Key == "OS_VisionNear")
					{
						if (Time1 != "")
							OS_VisionNear1 = a.Value;
						else
							OS_VisionNear = a.Value;
					}
					else if (a.Key == "OD_Reliability")
					{
						if (Time1 != "")
							OD_Reliability1 = a.Value;
						else
							OD_Reliability = a.Value;
					}
					else if (a.Key == "OS_Reliability")
					{
						if (Time1 != "")
							OS_Reliability1 = a.Value;
						else
							OS_Reliability = a.Value;
					}
					else if (a.Key == "OD_Comment")
					{
						if (Time1 != "")
							OD_Comment1 = a.Value;
						else
							OD_Comment = a.Value;
					}
					else if (a.Key == "OS_Comment")
					{
						if (Time1 != "")
							OS_Comment1 = a.Value;
						else
							OS_Comment = a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedAutoRefractionDetails(AppointmentId, Time, OD_Sphere, OS_Sphere, OD_Cylinder, OS_Cylinder, OD_Axis, OS_Axis, OD_Value, OS_Value, OD_Va, OS_Va, OD_PD, OS_PD, OD_VisionNear, OS_VisionNear, OD_Reliability, OS_Reliability, OD_Comment, OS_Comment) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Sphere.Replace("'", "''") + "', '" + OS_Sphere.Replace("'", "''") + "',  '" + OD_Cylinder.Replace("'", "''") + "',  '" + OS_Cylinder.Replace("'", "''") + "',  '" + OD_Axis.Replace("'", "''") + "',  '" + OS_Axis.Replace("'", "''") + "',  '" + OD_Value.Replace("'", "''") + "',  '" + OS_Value.Replace("'", "''") + "', '" + OD_Va.Replace("'", "''") + "', '" + OS_Va.Replace("'", "''") + "', '" + OD_PD.Replace("'", "''") + "', '" + OS_PD.Replace("'", "''") + "', '" + OD_VisionNear.Replace("'", "''") + "', '" + OS_VisionNear.Replace("'", "''") + "', '" + OD_Reliability.Replace("'", "''") + "', '" + OS_Reliability.Replace("'", "''") + "', '" + OD_Comment.Replace("'", "''") + "', '" + OS_Comment.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}

				if (Time1 != "")
				{
					_insertQry = "Insert into ExtractedAutoRefractionType2Details(AppointmentId, Time, OD_Sphere, OS_Sphere, OD_Cylinder, OS_Cylinder, OD_Axis, OS_Axis, OD_Value, OS_Value, OD_Va, OS_Va, OD_PD, OS_PD, OD_VisionNear, OS_VisionNear, OD_Reliability, OS_Reliability, OD_Comment, OS_Comment) Values ('" + _appointmentId + "', '" + Time1 + "', '" + OD_Sphere1.Replace("'", "''") + "', '" + OS_Sphere1.Replace("'", "''") + "',  '" + OD_Cylinder1.Replace("'", "''") + "',  '" + OS_Cylinder1.Replace("'", "''") + "',  '" + OD_Axis1.Replace("'", "''") + "',  '" + OS_Axis1.Replace("'", "''") + "',  '" + OD_Value1.Replace("'", "''") + "',  '" + OS_Value1.Replace("'", "''") + "', '" + OD_Va1.Replace("'", "''") + "', '" + OS_Va1.Replace("'", "''") + "', '" + OD_PD1.Replace("'", "''") + "', '" + OS_PD1.Replace("'", "''") + "', '" + OD_VisionNear1.Replace("'", "''") + "', '" + OS_VisionNear1.Replace("'", "''") + "', '" + OD_Reliability1.Replace("'", "''") + "', '" + OS_Reliability1.Replace("'", "''") + "', '" + OD_Comment1.Replace("'", "''") + "', '" + OS_Comment1.Replace("'", "''") + "')";
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(_insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}
			}
			else if (Name == "/AUTOKERATOMETRY")
			{
				string Time = "";
				string OD_Reading1 = "";
				string OS_Reading1 = "";
				string OD_Reading2 = "";
				string OS_Reading2 = "";
				string OD_Axis1 = "";
				string OS_Axis1 = "";
				string OD_Axis2 = "";
				string OS_Axis2 = "";
				string OD_AVE = "";
				string OS_AVE = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Reading1")
					{
						OD_Reading1 += " " + a.Value;
					}
					else if (a.Key == "OS_Reading1")
					{
						OS_Reading1 += " " + a.Value;
					}
					else if (a.Key == "OD_Reading2")
					{
						OD_Reading2 += " " + a.Value;
					}
					else if (a.Key == "OS_Reading2")
					{
						OS_Reading2 += " " + a.Value;
					}
					else if (a.Key == "OD_Axis1")
					{
						OD_Axis1 += " " + a.Value;
					}
					else if (a.Key == "OS_Axis1")
					{
						OS_Axis1 += " " + a.Value;
					}
					else if (a.Key == "OD_Axis2")
					{
						OD_Axis2 += " " + a.Value;
					}
					else if (a.Key == "OS_Axis2")
					{
						OS_Axis2 += " " + a.Value;
					}
					else if (a.Key == "OD_AVE")
					{
						OD_AVE += " " + a.Value;
					}
					else if (a.Key == "OS_AVE")
					{
						OS_AVE += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedAutokeratometryDetails(AppointmentId, Time, OD_Reading1, OS_Reading1, OD_Reading2, OS_Reading2, OD_Axis1, OS_Axis1, OD_Axis2, OS_Axis2, OD_AVE, OS_AVE) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Reading1.Replace("'", "''") + "', '" + OS_Reading1.Replace("'", "''") + "',  '" + OD_Reading2.Replace("'", "''") + "',  '" + OS_Reading2.Replace("'", "''") + "',  '" + OD_Axis1.Replace("'", "''") + "',  '" + OS_Axis1.Replace("'", "''") + "',  '" + OD_Axis2.Replace("'", "''") + "',  '" + OS_Axis2.Replace("'", "''") + "', '" + OD_AVE.Replace("'", "''") + "', '" + OS_AVE.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CUP/DISC")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedCupDiscRatioDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "',  '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/OCT TECH")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedOctTechDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/OCT")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedOCTDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/OPHTHALMOSCOPY INITIAL")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedOPHTHALMOSCOPYINITIALDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/VISION CORRECTED"))
			{
				bool recordexists = false;
				string _insertQry = "Select * From ExtractedVISIONCORRECTEDDetails Where AppointmentId = '" + _appointmentId + "'";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						SqlDataReader rdr = cmd.ExecuteReader();
						if (rdr.Read())
						{
							recordexists = true;
						}
					}
					con.Close();
				}

				string Time = "";
				string OD_Value = "";
				string OS_Value = "";
				string PH_OD_Value = "";
				string PH_OS_Value = "";
				string typeValue = "";
				if (Name == "/VISION CORRECTED")
				{
					foreach (KeyValuePair<string, string> a in Content.keyElement)
					{
						if (a.Key == "TIME")
						{
							Time = a.Value;
							typeValue = "";
						}
						if (a.Key == "typeValue")
						{
							typeValue = a.Value;
						}
						else if (a.Key == "OD_Value")
						{
							if (OD_Value == "")
								OD_Value = typeValue + "-" + a.Value;
							else
								OD_Value += " " + a.Value;
						}
						else if (a.Key == "OS_Value")
						{
							if (OS_Value == "")
								OS_Value = typeValue + "-" + a.Value;
							else
								OS_Value += " " + a.Value;
						}
					}

					if (recordexists)
						_insertQry = "Update ExtractedVISIONCORRECTEDDetails Set OD_Value = '" + OD_Value.Replace("'", "''") + "', OS_Value = '" + OS_Value.Replace("'", "''") + "', B_OD_Value = '" + PH_OD_Value.Replace("'", "''") + "', B_OS_Value = '" + PH_OS_Value.Replace("'", "''") + "' Where AppointmentId = '" + _appointmentId + "'";
					else
						_insertQry = "Insert into ExtractedVISIONCORRECTEDDetails(AppointmentId, Time, OD_Value, OS_Value, B_OD_Value, B_OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "', '"+ PH_OD_Value + "', '"+PH_OS_Value+"')";
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(_insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}
			}
			else if (Name.Contains("/VISION UNCORRECTED"))
			{
				bool recordexists = false;
				string _insertQry = "Select * From ExtractedVISIONUNCORRECTEDDetails Where AppointmentId = '" + _appointmentId + "'";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						SqlDataReader rdr = cmd.ExecuteReader();
						if (rdr.Read())
						{
							recordexists = true;
						}
					}
					con.Close();
				}

				string Time = "";
				string OD_Value = "";
				string OS_Value = "";
				string typeValue = "";
				string PH_OD_Value = "";
				string PH_OS_Value = "";
				if (Name == "/VISION UNCORRECTED")
				{
					foreach (KeyValuePair<string, string> a in Content.keyElement)
					{
						if (a.Key == "TIME")
						{
							Time = a.Value;
							typeValue = "";
						}
						if (a.Key == "typeValue")
						{
							typeValue = a.Value;
						}
						else if (a.Key == "OD_Value")
						{
							if (OD_Value == "")
								OD_Value = typeValue + "-" + a.Value;
							else
								OD_Value += " " + a.Value;
						}
						else if (a.Key == "OS_Value")
						{
							if (OS_Value == "")
								OS_Value = typeValue + "-" + a.Value;
							else
								OS_Value += " " + a.Value;
						}
						else if (a.Key == "PH_OD_Value")
						{
							if (PH_OD_Value == "")
								PH_OD_Value = typeValue + "-" + a.Value;
							else
								PH_OD_Value += " " + a.Value;
						}
						else if (a.Key == "PH_OS_Value")
						{
							if (PH_OS_Value == "")
								PH_OS_Value = typeValue + "-" + a.Value;
							else
								PH_OS_Value += " " + a.Value;
						}
					}

					if (recordexists)
					{
						_insertQry = "Update ExtractedVISIONUNCORRECTEDDetails Set OD_Value = '" + OD_Value.Replace("'", "''") + "', OS_Value = '" + OS_Value.Replace("'", "''") + "', B_OD_Value = '" + PH_OD_Value.Replace("'", "''") + "', B_OS_Value = '" + PH_OS_Value.Replace("'", "''") + "' Where AppointmentId = '" + _appointmentId + "'";
					}
					else
					{
						_insertQry = "Insert into ExtractedVISIONUNCORRECTEDDetails(AppointmentId, Time, OD_Value, OS_Value, B_OD_Value, B_OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "', '" + PH_OD_Value + "', '" + PH_OS_Value + "')";
					}
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(_insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}
			}
			else if (Name == "/IOP")
			{
				string Time = "";
				string OD_IOP = "";
				string OS_IOP = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_IOP")
					{
						OD_IOP = a.Value;
					}
					else if (a.Key == "OS_IOP")
					{
						OS_IOP = a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedIOPDetails(AppointmentId, Time, OD_IOP, OS_IOP) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_IOP.Replace("'", "''") + "', '" + OS_IOP.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/FUNDUS PHOTO (ON HEAD)")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedFundusPhotoOnHeadDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/FUNDUS PHOTO (ON HEAD) TECH")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedFundusPhotoOnHeadTechDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/FUNDUS PHOTO TECH")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedFundusPhotoTechDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/FUNDUS PHOTO")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedFundusPhotoDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/GLASSES")
			{
				string Time = "";
				string OD_Sphere = "";
				string OS_Sphere = "";
				string OD_Cylinder = "";
				string OS_Cylinder = "";
				string OD_Axis = "";
				string OS_Axis = "";
				string OD_Value = "";
				string OS_Value = "";
				string OD_AddReading = "";
				string OS_AddReading = "";
				string OD_AddInetrmed = "";
				string OS_AddInetrmed = "";
				string OD_VisionNear = "";
				string OS_VisionNear = "";
				string OD_PrismOfAngel1 = "";
				string OS_PrismOfAngel1 = "";
				string OD_PrismOfAngel2 = "";
				string OS_PrismOfAngel2 = "";
				string OD_VertexDistance = "";
				string OS_VertexDistance = "";
				string OD_Comment = "";
				string OS_Comment = "";
				string OD_Type = "";
				string OS_Type = "";
				string OD_VisionDistance = "";
				string OS_VisionDistance = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Sphere")
					{
						OD_Sphere = a.Value;
					}
					else if (a.Key == "OS_Sphere")
					{
						OS_Sphere = a.Value;
					}
					else if (a.Key == "OD_Cylinder")
					{
						OD_Cylinder = a.Value;
					}
					else if (a.Key == "OS_Cylinder")
					{
						OS_Cylinder = a.Value;
					}
					else if (a.Key == "OD_Axis")
					{
						OD_Axis = a.Value;
					}
					else if (a.Key == "OS_Axis")
					{
						OS_Axis = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value = a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value = a.Value;
					}
					else if (a.Key == "OD_ADDReading")
					{
						OD_AddReading = a.Value;
					}
					else if (a.Key == "OS_ADDReading")
					{
						OS_AddReading = a.Value;
					}
					else if (a.Key == "OD_ADDIntermed")
					{
						OD_AddInetrmed = a.Value;
					}
					else if (a.Key == "OS_ADDIntermed")
					{
						OS_AddInetrmed = a.Value;
					}
					else if (a.Key == "OD_VisionNear")
					{
						OD_VisionNear = a.Value;
					}
					else if (a.Key == "OS_VisionNear")
					{
						OS_VisionNear = a.Value;
					}
					else if (a.Key == "OD_PrismOfAngle1")
					{
						OD_PrismOfAngel1 = a.Value;
					}
					else if (a.Key == "OS_PrismOfAngle1")
					{
						OS_PrismOfAngel1 = a.Value;
					}
					else if (a.Key == "OD_PrismOfAngle2")
					{
						OD_PrismOfAngel2 = a.Value;
					}
					else if (a.Key == "OS_PrismOfAngle2")
					{
						OS_PrismOfAngel2 = a.Value;
					}
					else if (a.Key == "OD_VertexDistance")
					{
						OD_VertexDistance = a.Value;
					}
					else if (a.Key == "OS_VertexDistance")
					{
						OS_VertexDistance = a.Value;
					}
					else if (a.Key == "OD_Comment")
					{
						OD_Comment = a.Value;
					}
					else if (a.Key == "OS_Comment")
					{
						OS_Comment = a.Value;
					}
					else if (a.Key == "OD_Type")
					{
						OD_Type = a.Value;
					}
					else if (a.Key == "OS_Type")
					{
						OS_Type = a.Value;
					}
					else if (a.Key == "OD_VisionDistance")
					{
						OD_VisionDistance = a.Value;
					}
					else if (a.Key == "OS_VisionDistance")
					{
						OS_VisionDistance = a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedGlassesDetails(AppointmentId, Time, OD_Sphere, OS_Sphere, OD_Cylinder, OS_Cylinder, OD_Axis, OS_Axis, OD_Value, OS_Value, OD_AddReading, OS_AddReading, OD_AddIntermed, OS_AddIntermed, OD_VisionNear, OS_VisionNear, OD_PrismOfAngel1, OS_PrismOfAngel1, OD_PrismOfAngel2, OS_PrismOfAngel2, OD_VertexDistance, OS_VertexDistance, OD_Comment, OS_Comment, OD_Type, OS_Type, OD_VisionDistance, OS_VisionDistance) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Sphere.Replace("'", "''") + "', '" + OS_Sphere.Replace("'", "''") + "',  '" + OD_Cylinder.Replace("'", "''") + "',  '" + OS_Cylinder.Replace("'", "''") + "',  '" + OD_Axis.Replace("'", "''") + "',  '" + OS_Axis.Replace("'", "''") + "',  '" + OD_Value.Replace("'", "''") + "',  '" + OS_Value.Replace("'", "''") + "', '" + OD_AddReading.Replace("'", "''") + "', '" + OS_AddReading.Replace("'", "''") + "', '" + OD_AddInetrmed.Replace("'", "''") + "', '" + OS_AddInetrmed.Replace("'", "''") + "', '" + OD_VisionNear.Replace("'", "''") + "', '" + OS_VisionNear.Replace("'", "''") + "', '" + OD_PrismOfAngel1.Replace("'", "''") + "', '" + OS_PrismOfAngel1.Replace("'", "''") + "', '" + OD_PrismOfAngel2.Replace("'", "''") + "', '" + OS_PrismOfAngel2.Replace("'", "''") + "', '" + OD_VertexDistance.Replace("'", "''") + "', '" + OS_VertexDistance.Replace("'", "''") + "', '" + OD_Comment.Replace("'", "''") + "', '" + OS_Comment.Replace("'", "''") + "', '" + OD_Type.Replace("'", "''") + "', '" + OS_Type.Replace("'", "''") + "', '" + OD_VisionDistance.Replace("'", "''") + "', '" + OS_VisionDistance.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/MANIFEST REFRACTION")
			{
				string Time = "";
				string OD_Sphere = "";
				string OS_Sphere = "";
				string OD_Cylinder = "";
				string OS_Cylinder = "";
				string OD_Axis = "";
				string OS_Axis = "";
				string OD_Value = "";
				string OS_Value = "";
				string OD_AddReading = "";
				string OS_AddReading = "";
				string OD_AddInetrmed = "";
				string OS_AddInetrmed = "";
				string OD_VisionNear = "";
				string OS_VisionNear = "";
				string OD_PrismOfAngel1 = "";
				string OS_PrismOfAngel1 = "";
				string OD_PrismOfAngel2 = "";
				string OS_PrismOfAngel2 = "";
				string OD_VertexDistance = "";
				string OS_VertexDistance = "";
				string OD_Comment = "";
				string OS_Comment = "";
				string OD_Type = "";
				string OS_Type = "";
				string OD_VisionDistance = "";
				string OS_VisionDistance = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Sphere")
					{
						OD_Sphere = a.Value;
					}
					else if (a.Key == "OS_Sphere")
					{
						OS_Sphere = a.Value;
					}
					else if (a.Key == "OD_Cylinder")
					{
						OD_Cylinder = a.Value;
					}
					else if (a.Key == "OS_Cylinder")
					{
						OS_Cylinder = a.Value;
					}
					else if (a.Key == "OD_Axis")
					{
						OD_Axis = a.Value;
					}
					else if (a.Key == "OS_Axis")
					{
						OS_Axis = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value = a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value = a.Value;
					}
					else if (a.Key == "OD_ADDReading")
					{
						OD_AddReading = a.Value;
					}
					else if (a.Key == "OS_ADDReading")
					{
						OS_AddReading = a.Value;
					}
					else if (a.Key == "OD_ADDIntermed")
					{
						OD_AddInetrmed = a.Value;
					}
					else if (a.Key == "OS_ADDIntermed")
					{
						OS_AddInetrmed = a.Value;
					}
					else if (a.Key == "OD_VisionNear")
					{
						OD_VisionNear = a.Value;
					}
					else if (a.Key == "OS_VisionNear")
					{
						OS_VisionNear = a.Value;
					}
					else if (a.Key == "OD_PrismofAngle1")
					{
						OD_PrismOfAngel1 = a.Value;
					}
					else if (a.Key == "OS_PrismofAngle1")
					{
						OS_PrismOfAngel1 = a.Value;
					}
					else if (a.Key == "OD_PrismofAngle2")
					{
						OD_PrismOfAngel2 = a.Value;
					}
					else if (a.Key == "OS_PrismofAngle2")
					{
						OS_PrismOfAngel2 = a.Value;
					}
					else if (a.Key == "OD_VertexDistance")
					{
						OD_VertexDistance = a.Value;
					}
					else if (a.Key == "OS_VertexDistance")
					{
						OS_VertexDistance = a.Value;
					}
					else if (a.Key == "OD_Comment")
					{
						OD_Comment = a.Value;
					}
					else if (a.Key == "OS_Comment")
					{
						OS_Comment = a.Value;
					}
					else if (a.Key == "OD_Type")
					{
						OD_Type = a.Value;
					}
					else if (a.Key == "OS_Type")
					{
						OS_Type = a.Value;
					}
					else if (a.Key == "OD_VisionDistance")
					{
						OD_VisionDistance = a.Value;
					}
					else if (a.Key == "OS_VisionDistance")
					{
						OS_VisionDistance = a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedMANIFESTREFERACTIONDetails(AppointmentId, Time, OD_Sphere, OS_Sphere, OD_Cylinder, OS_Cylinder, OD_Axis, OS_Axis, OD_Value, OS_Value, OD_AddReading, OS_AddReading, OD_AddIntermed, OS_AddIntermed, OD_VisionNear, OS_VisionNear, OD_PrismOfAngel1, OS_PrismOfAngel1, OD_PrismOfAngel2, OS_PrismOfAngel2, OD_VertexDistance, OS_VertexDistance, OD_Comment, OS_Comment, OD_Type, OS_Type, OD_VisionDistance, OS_VisionDistance) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Sphere.Replace("'", "''") + "', '" + OS_Sphere.Replace("'", "''") + "',  '" + OD_Cylinder.Replace("'", "''") + "',  '" + OS_Cylinder.Replace("'", "''") + "',  '" + OD_Axis.Replace("'", "''") + "',  '" + OS_Axis.Replace("'", "''") + "',  '" + OD_Value.Replace("'", "''") + "',  '" + OS_Value.Replace("'", "''") + "', '" + OD_AddReading.Replace("'", "''") + "', '" + OS_AddReading.Replace("'", "''") + "', '" + OD_AddInetrmed.Replace("'", "''") + "', '" + OS_AddInetrmed.Replace("'", "''") + "', '" + OD_VisionNear.Replace("'", "''") + "', '" + OS_VisionNear.Replace("'", "''") + "', '" + OD_PrismOfAngel1.Replace("'", "''") + "', '" + OS_PrismOfAngel1.Replace("'", "''") + "', '" + OD_PrismOfAngel2.Replace("'", "''") + "', '" + OS_PrismOfAngel2.Replace("'", "''") + "', '" + OD_VertexDistance.Replace("'", "''") + "', '" + OS_VertexDistance.Replace("'", "''") + "', '" + OD_Comment.Replace("'", "''") + "', '" + OS_Comment.Replace("'", "''") + "', '" + OD_Type.Replace("'", "''") + "', '" + OS_Type.Replace("'", "''") + "', '" + OD_VisionDistance.Replace("'", "''") + "', '" + OS_VisionDistance.Replace("'", "''") + "')";

				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/GONIOSCOPY")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedGONIOSCOPYDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "',  '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/GONIOSCOPY R")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedGonioScopyRDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "',  '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/SMOKING")
			{
				string Time = "";
				string Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "typeValue")
					{
						Value = a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedSmokingDetails(AppointmentId, Time, Value) Values ('" + _appointmentId + "', '" + Time + "', '" + Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/PUPILS SIZE/REACTIVITY")
			{
				string Time = "";
				string OD_LITRoom = "";
				string OD_DarkRoom = "";
				string OD_Value = "";
				string OS_LITRoom = "";
				string OS_DarkRoom = "";
				string OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_LITRoom")
					{
						OD_LITRoom = a.Value;
					}
					else if (a.Key == "OD_DarkRoom")
					{
						OD_DarkRoom = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_LITRoom")
					{
						OS_LITRoom = a.Value;
					}
					else if (a.Key == "OS_DarkRoom")
					{
						OS_DarkRoom = a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedPupilsSizeDetails(AppointmentId, Time, OD_LITRoom, OD_DarkRoom, OD_Value, OS_LITRoom, OS_DarkRoom, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_LITRoom.Replace("'", "''") + "', '" + OD_DarkRoom.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_LITRoom.Replace("'", "''") + "', '" + OS_DarkRoom.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/EOM"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				bool recordexists = false;
				string _insertQry = "Select * From ExtractedEOMDetails Where AppointmentId = '" + _appointmentId + "'";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						SqlDataReader rdr = cmd.ExecuteReader();
						if (rdr.Read())
						{
							recordexists = true;
						}
					}
					con.Close();
				}

				if (Name == "/EOM")
				{
					foreach (KeyValuePair<string, string> a in Content.keyElement)
					{
						if (a.Key == "TIME")
						{
							Time = a.Value;
						}
						else if (a.Key == "OD_Value")
						{
							OD_Value += " " + a.Value;
						}
						else if (a.Key == "OS_Value")
						{
							OS_Value += " " + a.Value;
						}
					}
					if (recordexists)
						_insertQry = "Update ExtractedEOMDetails Set OD_Value = '" + OD_Value.Replace("'", "''") + "', OS_Value = '" + OS_Value.Replace("'", "''") + "' Where AppointmentId = '" + _appointmentId + "'";
					else
						_insertQry = "Insert into ExtractedEOMDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(_insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}
				else if (Name == "/EOM SURG, VERTICAL")
				{
					foreach (KeyValuePair<string, string> a in Content.keyElement)
					{
						if (a.Key == "TIME")
						{
							Time = a.Value;
						}
						else if (a.Key == "OD_Value")
						{
							OD_Value += " " + a.Value;
						}
						else if (a.Key == "OS_Value")
						{
							OS_Value += " " + a.Value;
						}
					}

					if (recordexists)
						_insertQry = "Update ExtractedEOMDetails Set EOM_SURG_Vertical_OD_Value = '" + OD_Value.Replace("'", "''") + "', EOM_SURG_Vertical_OS_Value = '" + OS_Value.Replace("'", "''") + "' Where AppointmentId = '" + _appointmentId + "'";
					else
						_insertQry = "Insert into ExtractedEOMDetails(AppointmentId, Time, EOM_SURG_Vertical_OD_Value, EOM_SURG_Vertical_OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(_insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}
				else if (Name == "/EOM SURG, HORIZONTAL")
				{
					foreach (KeyValuePair<string, string> a in Content.keyElement)
					{
						if (a.Key == "TIME")
						{
							Time = a.Value;
						}
						else if (a.Key == "OD_Value")
						{
							OD_Value += " " + a.Value;
						}
						else if (a.Key == "OS_Value")
						{
							OS_Value += " " + a.Value;
						}
					}

					if (recordexists)
						_insertQry = "Update ExtractedEOMDetails Set EOM_SURG_Horizontal_OD_Value = '" + OD_Value.Replace("'", "''") + "', EOM_SURG_Horizontal_OS_Value = '" + OS_Value.Replace("'", "''") + "' Where AppointmentId = '" + _appointmentId + "'";
					else
						_insertQry = "Insert into ExtractedEOMDetails(AppointmentId, Time, EOM_SURG_Horizontal_OD_Value, EOM_SURG_Horizontal_OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(_insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}
			}
			else if (Name == "/IRIS COLOR")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedIRISColorDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/IRIS ANGIOGRAPHY")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedIRISANGIOGRAPHYDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/IRIS ANGIO TECH")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}

				string _insertQry = "Insert into ExtractedIRISANGIOGRAPHYTechDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name.Contains("/LENSTAR"))
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";

				bool recordexists = false;
				string _insertQry = "Select * From ExtractedLENSTARDetails Where AppointmentId = '" + _appointmentId + "'";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						SqlDataReader rdr = cmd.ExecuteReader();
						if (rdr.Read())
						{
							recordexists = true;
						}
					}
					con.Close();
				}

				if (Name == "/LENSTAR")
				{
					foreach (KeyValuePair<string, string> a in Content.keyElement)
					{
						if (a.Key == "TIME")
						{
							Time = a.Value;
						}
						else if (a.Key == "OD_Value")
						{
							OD_Value += " " + a.Value;
						}
						else if (a.Key == "OS_Value")
						{
							OS_Value += " " + a.Value;
						}
					}
					if (recordexists)
						_insertQry = "Update ExtractedLENSTARDetails Set OD_Value = '" + OD_Value.Replace("'", "''") + "', OS_Value = '" + OS_Value.Replace("'", "''") + "' Where AppointmentId = '" + _appointmentId + "'";
					else
						_insertQry = "Insert into ExtractedLENSTARDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(_insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}
				else if (Name == "/LENSTAR TECH")
				{
					foreach (KeyValuePair<string, string> a in Content.keyElement)
					{
						if (a.Key == "OD_Value")
						{
							OD_Value += " " + a.Value;
						}
						else if (a.Key == "OS_Value")
						{
							OS_Value += " " + a.Value;
						}
					}

					if (recordexists)
						_insertQry = "Update ExtractedLENSTARDetails Set OD_Tech_Value = '" + OD_Value.Replace("'", "''") + "', OS_Tech_Value = '" + OS_Value.Replace("'", "''") + "' Where AppointmentId = '" + _appointmentId + "'";
					else
						_insertQry = "Insert into ExtractedLENSTARDetails(AppointmentId, Time, OD_Tech_Value, OS_Tech_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(_insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}
			}

			#endregion

			#region Third Phase 30 Elements
			// New Elements
			else if (Name == "/AMNIOTIC MEMBRANE TRANSPLANT")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedAmnioticMembraneTransplant(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/ANTERIOR MICROPUNCTURE")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedAnteriorMicroPuncture(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/BANDAGE CONTACT LENS")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedBandageContactLens(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/INCISE AND DRAIN CONJ CYST")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedInciseAndDrainConjCYST(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/BOTOX, MEDICAL")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				string OD_DrugUsed = "", OS_DrugUsed = "";
				string OD_OfUnitsUsed = "", OS_OfUnitsUsed = "";
				string OD_Lot = "", OS_Lot = "";
				string OD_VialExpDate = "", OS_VialExpDate = "";
				string OD_UnitsPer1CC = "", OS_UnitsPer1CC = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "od_drugused")
							{
								OD_DrugUsed = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_ofunitsused")
							{
								OD_OfUnitsUsed = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "os_drugused")
							{
								OD_DrugUsed = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_ofunitsused")
							{
								OD_OfUnitsUsed = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_drugused")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_drugused";
						}
						else
						{
							OD_DrugUsed = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_drugused")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_drugused";
						}
						else
						{
							OS_DrugUsed = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_ofunitsused")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_ofunitsused";
						}
						else
						{
							OD_DrugUsed = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_ofunitsused")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_ofunitsused";
						}
						else
						{
							OS_OfUnitsUsed = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_vialexpdate")
					{
						OD_VialExpDate = a.Value;
					}
					else if (a.Key.ToLower() == "os_vialexpdate")
					{
						OS_VialExpDate = a.Value;
					}
					else if (a.Key.ToLower() == "od_lot")
					{
						OD_Lot = a.Value;
					}
					else if (a.Key.ToLower() == "os_lot")
					{
						OS_Lot = a.Value;
					}
					else if (a.Key.ToLower() == "od_unitsper1cc")
					{
						OD_UnitsPer1CC = a.Value;
					}
					else if (a.Key.ToLower() == "os_unitsper1cc")
					{
						OS_UnitsPer1CC = a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedBotoxAndMedical(AppointmentId, Time, OD_Value, OS_Value, OD_DrugUsed, OS_DrugUsed, OD_OfUnitsUsed, OS_OfUnitsUsed, OD_Lot, OS_Lot, OD_VialExpDate, OS_VialExpDate, OD_UnitsPer1CC, OS_UnitsPer1CC) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "', '" + OD_DrugUsed.Replace("'", "''") + "', '" + OS_DrugUsed.Replace("'", "''") + "', '" + OD_OfUnitsUsed.Replace("'", "''") + "', '" + OS_OfUnitsUsed.Replace("'", "''") + "', '" + OD_Lot.Replace("'", "''") + "', '" + OS_Lot.Replace("'", "''") + "', '" + OD_VialExpDate.Replace("'", "''") + "', '" + OS_VialExpDate.Replace("'", "''") + "', '" + OD_UnitsPer1CC.Replace("'", "''") + "', '" + OS_UnitsPer1CC.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/BOTOX, STRABISMUS")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedBotoxAndStrabismus(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CHALAZION EXCISION")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				string OD_Lower = "", OS_Lower = "";
				string OD_Upper = "", OS_Upper = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "od_lower")
							{
								OD_Lower = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_upper")
							{
								OD_Upper = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "os_lower")
							{
								OS_Lower = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_upper")
							{
								OS_Upper = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_lower")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_lower";
						}
						else
						{
							OD_Lower = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_lower")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_lower";
						}
						else
						{
							OS_Lower = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_upper")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_upper";
						}
						else
						{
							OD_Upper = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_upper")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_upper";
						}
						else
						{
							OS_Upper = a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedChalazionExcision(AppointmentId, Time, OD_Upper, OD_Lower, OD_Value, OS_Upper, OS_Lower, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Upper.Replace("'", "''") + "', '" + OD_Lower.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Upper.Replace("'", "''") + "', '" + OS_Lower.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CLOSURE OF PUNCTUM, PERMANENT")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				string OD_Lower = "", OS_Lower = "";
				string OD_Upper = "", OS_Upper = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "od_lower")
							{
								OD_Lower = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_upper")
							{
								OD_Upper = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "os_lower")
							{
								OS_Lower = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_upper")
							{
								OS_Upper = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_lower")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_lower";
						}
						else
						{
							OD_Lower = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_lower")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_lower";
						}
						else
						{
							OS_Lower = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_upper")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_upper";
						}
						else
						{
							OD_Upper = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_upper")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_upper";
						}
						else
						{
							OS_Upper = a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedClosureOfPunctumAndPermanent(AppointmentId, Time, OD_Upper, OD_Lower, OD_Value, OS_Upper, OS_Lower, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Upper.Replace("'", "''") + "', '" + OD_Lower.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Upper.Replace("'", "''") + "', '" + OS_Lower.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CLOSURE OF PUNCTUM, PLUGS")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				string OD_Lower = "", OS_Lower = "";
				string OD_Upper = "", OS_Upper = "";
				string OD_Lot = "", OS_Lot = "";
				string OD_Manufacturer = "", OS_Manufacturer = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "od_lower")
							{
								OD_Lower = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_upper")
							{
								OD_Upper = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_manufacturer")
							{
								OD_Manufacturer = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_lot")
							{
								OD_Lot = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "os_lower")
							{
								OS_Lower = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_upper")
							{
								OS_Upper = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_manufacturer")
							{
								OS_Manufacturer = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_lot")
							{
								OS_Lot = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_lower")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_lower";
						}
						else
						{
							OD_Lower = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_lower")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_lower";
						}
						else
						{
							OS_Lower = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_upper")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_upper";
						}
						else
						{
							OD_Upper = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_upper")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_upper";
						}
						else
						{
							OS_Upper = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_manufacturer")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_manufacturer";
						}
						else
						{
							OD_Manufacturer = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_manufacturer")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_manufacturer";
						}
						else
						{
							OS_Manufacturer = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_lot")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_lot";
						}
						else
						{
							OD_Lot = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_lot")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_lot";
						}
						else
						{
							OS_Lot = a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedClosureOfPunctumAndPlugs(AppointmentId, Time, OD_Upper, OD_Manufacturer, OD_Lot, OD_Lower, OD_Value, OS_Upper, OS_Manufacturer, OS_Lot, OS_Lower, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Upper.Replace("'", "''") + "', '" + OD_Manufacturer.Replace("'", "''") + "', '" + OD_Lot.Replace("'", "''") + "', '" + OD_Lower.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Upper.Replace("'", "''") + "', '" + OS_Manufacturer.Replace("'", "''") + "', '" + OS_Lot.Replace("'", "''") + "', '" + OS_Lower.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/CULTURE OF CORNEA/CONJUNCTIVA")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedCultureOfCorneaAndConjunctiva(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/ENTROPION REPAIR")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				string OD_UpperLid = "", OS_UpperLid = "";
				string OD_LowerLid = "", OS_LowerLid = "";
				string _speicalColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_speicalColumn != "")
						{
							if (_speicalColumn == "od_lowerlid")
							{
								OS_LowerLid = a.Value;
								_speicalColumn = "";
							}
							else if (_speicalColumn == "od_upperlid")
							{
								OD_UpperLid = a.Value;
								_speicalColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_speicalColumn != "")
						{
							if (_speicalColumn == "os_upperlid")
							{
								OS_UpperLid = a.Value;
								_speicalColumn = "";
							}
							else if (_speicalColumn == "os_lowerlid")
							{
								OS_LowerLid = a.Value;
								_speicalColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_lowerlid")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "os_lowerlid";
						}
						else
						{
							OS_LowerLid = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_lowerlid")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "od_lowerlid";
						}
						else
						{
							OD_LowerLid = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_upperlid")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "os_upperlid";
						}
						else
						{
							OS_UpperLid = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_upperlid")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "od_upperlid";
						}
						else
						{
							OD_UpperLid = a.Value;
						}
					}
					else if (a.Key.ToLower() == "")
					{
						OS_LowerLid = a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedEntropionRepair(AppointmentId, Time, OD_UpperLid, OD_LowerLid, OD_Value, OS_UpperLid, OS_LowerLid, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_UpperLid.Replace("'", "''") + "', '" + OD_LowerLid.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_UpperLid.Replace("'", "''") + "', '" + OS_LowerLid.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/EPILATION")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				string OD_LashesLower = "", OS_LashesLower = "";
				string OD_LashesUpper = "", OS_LashesUpper = "";
				string OD_TypeOfEpilation = "", OS_TypeOfEpilation = "";
				string _speicalColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_speicalColumn != "")
						{
							if (_speicalColumn == "od_lasheslower")
							{
								OD_LashesLower = a.Value;
								_speicalColumn = "";
							}
							else if (_speicalColumn == "od_lashesupper")
							{
								OD_LashesUpper = a.Value;
								_speicalColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_speicalColumn != "")
						{
							if (_speicalColumn == "os_lasheslower")
							{
								OS_LashesLower = a.Value;
								_speicalColumn = "";
							}
							else if (_speicalColumn == "os_lashesupper")
							{
								OS_LashesUpper = a.Value;
								_speicalColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_lasheslower")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "od_lasheslower";
						}
						else
						{
							OD_LashesLower = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_lasheslower")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "os_lasheslower";
						}
						else
						{
							OS_LashesLower = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_lashesupper")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "od_lashesupper";
						}
						else
						{
							OD_LashesUpper = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_lashesupper")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "os_lashesupper";
						}
						else
						{
							OS_LashesUpper = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_typeofepilation")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "od_typeofepilation";
						}
						else
						{
							OD_TypeOfEpilation = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_typeofepilation")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "os_typeofepilation";
						}
						else
						{
							OS_TypeOfEpilation = a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedEpilation(AppointmentId, Time, OD_LashesUpper, OD_LashesLower, OD_TypeOfEpilation, OD_Value, OS_LashesUpper, OS_LashesLower, OS_TypeOfEpilation, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_LashesUpper.Replace("'", "''") + "', '" + OD_LashesLower.Replace("'", "''") + "', '" + OD_TypeOfEpilation.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_LashesUpper.Replace("'", "''") + "', '" + OS_LashesLower.Replace("'", "''") + "', '" + OS_TypeOfEpilation.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/INJECTION, INTRALESIONAL")
			{
				string Time = "";
				string OD_Value = "", OS_Value = "";
				string OD_UpperLid = "", OS_UpperLid = "";
				string OD_LowerLid = "", OS_LowerLid = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "od_upperlid")
							{
								OD_UpperLid = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_lowerlid")
							{
								OD_LowerLid = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "os_upperlid")
							{
								OS_UpperLid = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_lowerlid")
							{
								OS_LowerLid = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_upperlid")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_upperlid";
						}
						else
						{
							OD_UpperLid = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_upperlid")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_upperlid";
						}
						else
						{
							OS_UpperLid = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_lowerlid")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "od_lowerlid";
						}
						else
						{
							OD_LowerLid = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_lowerlid")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "os_lowerlid";
						}
						else
						{
							OS_LowerLid = a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedInjectionAndIntralesional(AppointmentId, Time, OD_UpperLid, OD_LowerLid, OD_Value, OS_UpperLid, OS_LowerLid, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_UpperLid.Replace("'", "''") + "', '" + OD_LowerLid.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_UpperLid.Replace("'", "''") + "', '" + OS_LowerLid.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/INTRAVIT INJEC, AVASTIN")
			{
				string Time = "";
				string OD_CC = "", OD_PreOrMeds = "", OD_IOP1 = "", OD_IOP2 = "", OD_Comments = "", OD_PostOp = "", OD_LotNumber = "", OD_QuickButton = "", OD_Value = "", OS_CC = "", OS_PreOrMeds = "", OS_IOP1 = "", OS_IOP2 = "", OS_Comments = "", OS_PostOp = "", OS_LotNumber = "", OS_QuickButton = "", OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "od_cc")
							{
								OD_CC = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_preormeds")
							{
								OD_PreOrMeds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_comments")
							{
								OD_Comments = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_postop")
							{
								OD_PostOp = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "od_quickbutton")
							{
								OD_QuickButton = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "os_cc")
							{
								OS_CC = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_preormeds")
							{
								OS_PreOrMeds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_comments")
							{
								OS_Comments = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_postop")
							{
								OS_PostOp = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "os_quickbutton")
							{
								OS_QuickButton = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_cc")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "od_cc";
						}
						else
						{
							OD_CC = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_preormeds")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "od_preormeds";
						}
						else
						{
							OD_PreOrMeds = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_iop1")
					{
						OD_IOP1 = a.Value;
					}
					else if (a.Key.ToLower() == "od_iop2")
					{
						OD_IOP2 = a.Value;
					}
					else if (a.Key.ToLower() == "od_comments")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "od_comments";
						}
						else
						{
							OD_Comments = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_postop")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "od_postop";
						}
						else
						{
							OD_PostOp = a.Value;
						}
					}
					else if (a.Key.ToLower() == "od_lotnumber")
					{
						OD_LotNumber = a.Value;
					}
					else if (a.Key.ToLower() == "od_quickbutton")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "od_quickbutton";
						}
						else
						{
							OD_QuickButton = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_cc")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "os_cc";
						}
						else
						{
							OS_CC = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_preormeds")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "os_preormeds";
						}
						else
						{
							OS_PreOrMeds = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_iop1")
					{
						OS_IOP1 = a.Value;
					}
					else if (a.Key.ToLower() == "os_iop2")
					{
						OS_IOP2 = a.Value;
					}
					else if (a.Key.ToLower() == "os_comments")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "os_comments";
						}
						else
						{
							OS_Comments = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_postop")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "os_postop";
						}
						else
						{
							OS_PostOp = a.Value;
						}
					}
					else if (a.Key.ToLower() == "os_lotnumber")
					{
						OS_LotNumber = a.Value;
					}
					else if (a.Key.ToLower() == "os_quickbutton")
					{
						if (a.Value.TrimStart().TrimEnd() == ";")
						{
							_specialColumn = "os_quickbutton";
						}
						else
						{
							OS_QuickButton = a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedIntravitInjectAndAvastin(AppointmentId, Time, OD_CC, OD_PreOrMeds, OD_IOP1, OD_IOP2, OD_Comments, OD_PostOp, OD_LotNumber, OD_QuickButton, OD_Value, OS_CC, OS_PreOrMeds, OS_IOP1, OS_IOP2, OS_Comments, OS_PostOp, OS_LotNumber, OS_QuickButton, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_CC.Replace("'", "''") + "', '" + OD_PreOrMeds.Replace("'", "''") + "', '" + OD_IOP1.Replace("'", "''") + "', '" + OD_IOP2.Replace("'", "''") + "', '" + OD_Comments.Replace("'", "''") + "', '" + OD_PostOp.Replace("'", "''") + "', '" + OD_LotNumber.Replace("'", "''") + "', '" + OD_QuickButton.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_CC.Replace("'", "''") + "', '" + OS_PreOrMeds.Replace("'", "''") + "', '" + OS_IOP1.Replace("'", "''") + "', '" + OS_IOP2.Replace("'", "''") + "', '" + OS_Comments.Replace("'", "''") + "', '" + OS_PostOp.Replace("'", "''") + "', '" + OS_LotNumber.Replace("'", "''") + "', '" + OS_QuickButton.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, IRIDOTOMY (66761)")
			{
				string Time = "";
				string OD_ArgonofApplications = "";
				string OD_ArgonEnergyLevel = "";
				string OD_ArgonSpotSize = "";
				string OD_YagInitialofbursts = "";
				string OD_YagPowerlevel = "";
				string OD_Yagpulsesperburst = "";
				string OD_MedsandLocation = "";
				string OD_Value = "";
				string OS_ArgonofApplications = "";
				string OS_ArgonEnergyLevel = "";
				string OS_ArgonSpotSize = "";
				string OS_YagInitialofbursts = "";
				string OS_YagPowerlevel = "";
				string OS_Yagpulsesperburst = "";
				string OS_MedsandLocation = "";
				string OS_Value = "";
				string _speicalColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_ArgonofApplications")
					{
						OD_ArgonofApplications = a.Value;
					}
					else if (a.Key == "OD_ArgonEnergyLevel")
					{
						OD_ArgonEnergyLevel = a.Value;
					}
					else if (a.Key == "OD_ArgonSpotSize")
					{
						OD_ArgonSpotSize = a.Value;
					}
					else if (a.Key == "OD_YagInitialofbursts")
					{
						OD_YagInitialofbursts = a.Value;
					}
					else if (a.Key == "OD_YagPowerlevel")
					{
						OD_YagPowerlevel = a.Value;
					}
					else if (a.Key == "OD_Yagpulsesperburst")
					{
						OD_Yagpulsesperburst = a.Value;
					}
					else if (a.Key == "OD_MedsandLocation")
					{
						if (a.Key.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "OD_MedsandLocation";
						}
						else
						{
							OD_MedsandLocation = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_speicalColumn != "")
						{
							if (_speicalColumn == "OD_MedsandLocation")
							{
								OD_MedsandLocation = a.Value;
								_speicalColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_ArgonofApplications")
					{
						OS_ArgonofApplications = a.Value;
					}
					else if (a.Key == "OS_ArgonEnergyLevel")
					{
						OS_ArgonEnergyLevel = a.Value;
					}
					else if (a.Key == "OS_ArgonSpotSize")
					{
						OS_ArgonSpotSize = a.Value;
					}
					else if (a.Key == "OS_YagInitialofbursts")
					{
						OS_YagInitialofbursts = a.Value;
					}
					else if (a.Key == "OS_YagPowerlevel")
					{
						OS_YagPowerlevel = a.Value;
					}
					else if (a.Key == "OS_Yagpulsesperburst")
					{
						OS_Yagpulsesperburst = a.Value;
					}
					else if (a.Key == "OS_MedsandLocation")
					{
						if (a.Key.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "OS_MedsandLocation";
						}
						else
						{
							OS_MedsandLocation = a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_speicalColumn != "")
						{
							if (_speicalColumn == "OS_MedsandLocation")
							{
								OS_MedsandLocation = a.Value;
								_speicalColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserAndIridotomy66761(AppointmentId, Time, OD_ArgonofApplications, OD_ArgonEnergyLevel, OD_ArgonSpotSize, OD_YagInitialofbursts, OD_YagPowerlevel, OD_Yagpulsesperburst, OD_MedsandLocation, OD_Value, OS_ArgonofApplications, OS_ArgonEnergyLevel, OS_ArgonSpotSize, OS_YagInitialofbursts, OS_YagPowerlevel, OS_Yagpulsesperburst, OS_MedsandLocation, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_ArgonofApplications.Replace("'", "''") + "', '" + OD_ArgonEnergyLevel.Replace("'", "''") + "', '" + OD_ArgonSpotSize.Replace("'", "''") + "', '" + OD_YagInitialofbursts.Replace("'", "''") + "', '" + OD_YagPowerlevel.Replace("'", "''") + "', '" + OD_Yagpulsesperburst.Replace("'", "''") + "','" + OD_MedsandLocation.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_ArgonofApplications.Replace("'", "''") + "','" + OS_ArgonEnergyLevel.Replace("'", "''") + "', '" + OS_ArgonSpotSize.Replace("'", "''") + "', '" + OS_YagInitialofbursts.Replace("'", "''") + "', '" + OS_YagPowerlevel.Replace("'", "''") + "', '" + OS_Yagpulsesperburst.Replace("'", "''") + "', '" + OS_MedsandLocation.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, IRIDOTOMY, ARGON (66761)")
			{
				string Time = "";
				string OD_NumberofApplications = "";
				string OD_EnergyLevel = "";
				string OD_SpotSize = "";
				string OD_Duration = "";
				string OD_Meds = "";
				string OD_Value = "";
				string OS_NumberofApplications = "";
				string OS_EnergyLevel = "";
				string OS_SpotSize = "";
				string OS_Duration = "";
				string OS_Meds = "";
				string OS_Value = "";
				string _speicalColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_NumberofApplications")
					{
						OD_NumberofApplications = a.Value;
					}
					else if (a.Key == "OD_EnergyLevel")
					{
						OD_EnergyLevel = a.Value;
					}
					else if (a.Key == "OD_SpotSize")
					{
						OD_SpotSize = a.Value;
					}
					else if (a.Key == "OD_Duration")
					{
						OD_Duration = a.Value;
					}
					else if (a.Key == "OD_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "OD_Meds";
						}
						else
						{
							OD_Meds = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_speicalColumn != "")
						{
							if (_speicalColumn == "OD_Meds")
							{
								OD_Meds = a.Value;
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_NumberofApplications")
					{
						OS_NumberofApplications = a.Value;
					}
					else if (a.Key == "OS_EnergyLevel")
					{
						OS_EnergyLevel = a.Value;
					}
					else if (a.Key == "OS_SpotSize")
					{
						OS_SpotSize = a.Value;
					}
					else if (a.Key == "OS_Duration")
					{
						OS_Duration = a.Value;
					}
					else if (a.Key == "OS_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_speicalColumn = "OS_Meds";
						}
						else
						{
							OS_Meds = a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_speicalColumn != "")
						{
							if (_speicalColumn == "OS_Meds")
							{
								OS_Meds = a.Value;
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserIridotomyAndArgon66761(AppointmentId, Time, OD_NumberofApplications, OD_EnergyLevel, OD_SpotSize, OD_Duration, OD_Meds, OD_Value, OS_NumberofApplications, OS_EnergyLevel, OS_SpotSize, OS_Duration, OS_Meds, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_NumberofApplications.Replace("'", "''") + "', '" + OD_EnergyLevel.Replace("'", "''") + "', '" + OD_SpotSize.Replace("'", "''") + "', '" + OD_Duration.Replace("'", "''") + "', '" + OD_Meds.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "','" + OS_NumberofApplications.Replace("'", "''") + "', '" + OS_EnergyLevel.Replace("'", "''") + "', '" + OS_SpotSize.Replace("'", "''") + "','" + OS_Duration.Replace("'", "''") + "', '" + OS_Meds.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, MACULOP, (FOCAL 67210)")
			{
				string Time = "";
				string OD_NumberofApplications = "";
				string OD_EnergyLevelmw = "";
				string OD_SpotSizemicrons = "";
				string OD_Durationsec = "";
				string OD_Meds = "";
				string OD_LasertypeLens = "";
				string OD_Value = "";
				string OS_NumberofApplications = "";
				string OS_EnergyLevelmw = "";
				string OS_SpotSizemicrons = "";
				string OS_Durationsec = "";
				string OS_Meds = "";
				string OS_LasertypeLens = "";
				string OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_NumberofApplications")
					{
						OD_NumberofApplications = a.Value;
					}
					else if (a.Key == "OD_EnergyLevelmw")
					{
						OD_EnergyLevelmw = a.Value;
					}
					else if (a.Key == "OD_SpotSizemicrons")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_SpotSizemicrons";
						}
						else
						{
							OD_SpotSizemicrons = a.Value;
						}
					}
					else if (a.Key == "OD_Durationsec")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Durationsec";
						}
						else
						{
							OD_Durationsec = a.Value;
						}
					}
					else if (a.Key == "OD_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Meds";
						}
						else
						{
							OD_Meds = a.Value;
						}
					}
					else if (a.Key == "OD_LasertypeLens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_LasertypeLens";
						}
						else
						{
							OD_LasertypeLens = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OD_LasertypeLens")
							{
								OD_LasertypeLens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Meds")
							{
								OD_Meds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Durationsec")
							{
								OD_Durationsec = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_SpotSizemicrons")
							{
								OD_SpotSizemicrons = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_NumberofApplications")
					{
						OS_NumberofApplications = a.Value;
					}
					else if (a.Key == "OS_EnergyLevelmw")
					{
						OS_EnergyLevelmw = a.Value;
					}
					else if (a.Key == "OS_SpotSizemicrons")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_SpotSizemicrons";
						}
						else
						{
							OS_SpotSizemicrons = a.Value;
						}
					}
					else if (a.Key == "OS_Durationsec")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Durationsec";
						}
						else
						{
							OS_Durationsec = a.Value;
						}
					}
					else if (a.Key == "OS_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Meds";
						}
						else
						{
							OS_Meds = a.Value;
						}
					}
					else if (a.Key == "OS_LasertypeLens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_LasertypeLens";
						}
						else
						{
							OS_LasertypeLens = a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OS_LasertypeLens")
							{
								OS_LasertypeLens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Meds")
							{
								OS_Meds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Durationsec")
							{
								OS_Durationsec = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_SpotSizemicrons")
							{
								OS_SpotSizemicrons = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserAndMaculopFocal67210(AppointmentId, Time, OD_NumberofApplications, OD_EnergyLevelmw, OD_SpotSizemicrons, OD_Durationsec, OD_Meds, OD_LasertypeLens, OD_Value, OS_NumberofApplications, OS_EnergyLevelmw, OS_SpotSizemicrons, OS_Durationsec, OS_Meds, OS_LasertypeLens, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_NumberofApplications.Replace("'", "''") + "', '" + OD_EnergyLevelmw.Replace("'", "''") + "', '" + OD_SpotSizemicrons.Replace("'", "''") + "', '" + OD_Durationsec.Replace("'", "''") + "', '" + OD_Meds.Replace("'", "''") + "', '" + OD_LasertypeLens.Replace("'", "''") + "','" + OD_Value.Replace("'", "''") + "', '" + OS_NumberofApplications.Replace("'", "''") + "', '" + OS_EnergyLevelmw.Replace("'", "''") + "','" + OS_SpotSizemicrons.Replace("'", "''") + "', '" + OS_Durationsec.Replace("'", "''") + "', '" + OS_Meds.Replace("'", "''") + "', '" + OS_LasertypeLens.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, PROPHY RD (67145)")
			{
				string Time = "";
				string OD_NumberofApplications = "";
				string OD_EnergyLevelmw = "";
				string OD_SpotSizemicrons = "";
				string OD_Durationtime = "";
				string OD_Meds = "";
				string OD_LasertypeLens = "";
				string OD_Location = "";
				string OD_Value = "";
				string OS_NumberofApplications = "";
				string OS_EnergyLevelmw = "";
				string OS_SpotSizemicrons = "";
				string OS_Durationtime = "";
				string OS_Meds = "";
				string OS_LasertypeLens = "";
				string OS_Location = "";
				string OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_NumberofApplications")
					{
						OD_NumberofApplications = a.Value;
					}
					else if (a.Key == "OD_EnergyLevelmw")
					{
						OD_EnergyLevelmw = a.Value;
					}
					else if (a.Key == "OD_SpotSizemicrons")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_SpotSizemicrons";
						}
						else
						{
							OD_SpotSizemicrons = a.Value;
						}
					}
					else if (a.Key == "OD_Durationtime")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Durationtime";
						}
						else
						{
							OD_Durationtime = a.Value;
						}
					}
					else if (a.Key == "OD_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Meds";
						}
						else
						{
							OD_Meds = a.Value;
						}
					}
					else if (a.Key == "OD_LasertypeLens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_LasertypeLens";
						}
						else
						{
							OD_LasertypeLens = a.Value;
						}
					}
					else if (a.Key == "OD_Location")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Location";
						}
						else
						{
							OD_Location = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OD_LasertypeLens")
							{
								OD_LasertypeLens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Location")
							{
								OD_Location = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Meds")
							{
								OD_Meds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Durationtime")
							{
								OD_Durationtime = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_SpotSizemicrons")
							{
								OD_SpotSizemicrons = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_NumberofApplications")
					{
						OS_NumberofApplications = a.Value;
					}
					else if (a.Key == "OS_EnergyLevelmw")
					{
						OS_EnergyLevelmw = a.Value;
					}
					else if (a.Key == "OS_SpotSizemicrons")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_SpotSizemicrons";
						}
						else
						{
							OS_SpotSizemicrons = a.Value;
						}
					}
					else if (a.Key == "OS_Durationtime")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Durationtime";
						}
						else
						{
							OS_Durationtime = a.Value;
						}
					}
					else if (a.Key == "OS_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Meds";
						}
						else
						{
							OS_Meds = a.Value;
						}
					}
					else if (a.Key == "OS_LasertypeLens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_LasertypeLens";
						}
						else
						{
							OS_LasertypeLens = a.Value;
						}
					}
					else if (a.Key == "OS_Location")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Location";
						}
						else
						{
							OS_Location = a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OS_LasertypeLens")
							{
								OS_LasertypeLens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Meds")
							{
								OS_Meds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Durationtime")
							{
								OS_Durationtime = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Location")
							{
								OS_Location = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_SpotSizemicrons")
							{
								OS_SpotSizemicrons = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserProphyRD67145(AppointmentId, Time, OD_NumberofApplications, OD_EnergyLevelmw, OD_SpotSizemicrons, OD_Durationtime, OD_Meds, OD_LasertypeLens, OD_Location, OD_Value, OS_NumberofApplications, OS_EnergyLevelmw, OS_SpotSizemicrons, OS_Durationtime, OS_Meds, OS_LasertypeLens, OS_Location, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_NumberofApplications.Replace("'", "''") + "','" + OD_EnergyLevelmw.Replace("'", "''") + "','" + OD_SpotSizemicrons.Replace("'", "''") + "','" + OD_Durationtime.Replace("'", "''") + "','" + OD_Meds.Replace("'", "''") + "','" + OD_LasertypeLens.Replace("'", "''") + "','" + OD_Location.Replace("'", "''") + "','" + OD_Value.Replace("'", "''") + "','" + OS_NumberofApplications.Replace("'", "''") + "','" + OS_EnergyLevelmw.Replace("'", "''") + "','" + OS_SpotSizemicrons.Replace("'", "''") + "','" + OS_Durationtime.Replace("'", "''") + "','" + OS_Meds.Replace("'", "''") + "','" + OS_LasertypeLens.Replace("'", "''") + "','" + OS_Location.Replace("'", "''") + "','" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, PRP (67228)")
			{
				string Time = "";
				string OD_NumberofApps = "";
				string OD_EnergyLevelmw = "";
				string OD_SpotSizemicrons = "";
				string OD_Durationtime = "";
				string OD_Meds = "";
				string OD_TypeLocPatchLens = "";
				string OD_Quick1 = "";
				string OD_Quick2 = "";
				string OD_Value = "";
				string OS_NumberofApps = "";
				string OS_EnergyLevelmw = "";
				string OS_SpotSizemicrons = "";
				string OS_Durationtime = "";
				string OS_Meds = "";
				string OS_TypeLocPatchLens = "";
				string OS_Quick1 = "";
				string OS_Quick2 = "";
				string OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_NumberofApps")
					{
						OD_NumberofApps = a.Value;
					}
					else if (a.Key == "OD_EnergyLevelmw")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_EnergyLevelmw";
						}
						else
						{
							OD_EnergyLevelmw = a.Value;
						}
					}
					else if (a.Key == "OD_SpotSizemicrons")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_SpotSizemicrons";
						}
						else
						{
							OD_SpotSizemicrons = a.Value;
						}
					}
					else if (a.Key == "OD_Durationtime")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Durationtime";
						}
						else
						{
							OD_Durationtime = a.Value;
						}
					}
					else if (a.Key == "OD_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Meds";
						}
						else
						{
							OD_Meds = a.Value;
						}
					}
					else if (a.Key == "OD_TypeLocPatchLens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_TypeLocPatchLens";
						}
						else
						{
							OD_TypeLocPatchLens = a.Value;
						}
					}
					else if (a.Key == "OD_Quick1")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Quick1";
						}
						else
						{
							OD_Quick1 = a.Value;
						}
					}
					else if (a.Key == "OD_Quick2")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Quick2";
						}
						else
						{
							OD_Quick2 = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OD_Durationtime")
							{
								OD_Durationtime = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_EnergyLevelmw")
							{
								OD_EnergyLevelmw = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Meds")
							{
								OD_Meds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_TypeLocPatchLens")
							{
								OD_TypeLocPatchLens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Quick2")
							{
								OD_Quick2 = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Quick1")
							{
								OD_Quick1 = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_SpotSizemicrons")
							{
								OD_SpotSizemicrons = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_NumberofApps")
					{
						OS_NumberofApps = a.Value;
					}
					else if (a.Key == "OS_EnergyLevelmw")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_EnergyLevelmw";
						}
						else
						{
							OS_EnergyLevelmw = a.Value;
						}
					}
					else if (a.Key == "OS_SpotSizemicrons")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_SpotSizemicrons";
						}
						else
						{
							OS_SpotSizemicrons = a.Value;
						}
					}
					else if (a.Key == "OS_Durationtime")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Durationtime";
						}
						else
						{
							OS_Durationtime = a.Value;
						}
					}
					else if (a.Key == "OS_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Meds";
						}
						else
						{
							OS_Meds = a.Value;
						}
					}
					else if (a.Key == "OS_TypeLocPatchLens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_TypeLocPatchLens";
						}
						else
						{
							OS_TypeLocPatchLens = a.Value;
						}
					}
					else if (a.Key == "OS_Quick1")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Quick1";
						}
						else
						{
							OS_Quick1 = a.Value;
						}
					}
					else if (a.Key == "OS_Quick2")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Quick2";
						}
						else
						{
							OS_Quick2 = a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OS_Durationtime")
							{
								OS_Durationtime = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_EnergyLevelmw")
							{
								OS_EnergyLevelmw = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Meds")
							{
								OS_Meds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_TypeLocPatchLens")
							{
								OS_TypeLocPatchLens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Quick2")
							{
								OS_Quick2 = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Quick1")
							{
								OS_Quick1 = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_SpotSizemicrons")
							{
								OS_SpotSizemicrons = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserAndPRP67228(AppointmentId, Time, OD_NumberofApps, OD_EnergyLevelmw, OD_SpotSizemicrons, OD_Durationtime, OD_Meds, OD_TypeLocPatchLens, OD_Quick1, OD_Quick2, OD_Value, OS_NumberofApps, OS_EnergyLevelmw, OS_SpotSizemicrons, OS_Durationtime, OS_Meds, OS_TypeLocPatchLens, OS_Quick1, OS_Quick2, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_NumberofApps.Replace("'", "''") + "', '" + OD_EnergyLevelmw.Replace("'", "''") + "', '" + OD_SpotSizemicrons.Replace("'", "''") + "', '" + OD_Durationtime.Replace("'", "''") + "', '" + OD_Meds.Replace("'", "''") + "', '" + OD_TypeLocPatchLens.Replace("'", "''") + "', '" + OD_Quick1.Replace("'", "''") + "', '" + OD_Quick2.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_NumberofApps.Replace("'", "''") + "', '" + OS_EnergyLevelmw.Replace("'", "''") + "', '" + OS_SpotSizemicrons.Replace("'", "''") + "', '" + OS_Durationtime.Replace("'", "''") + "', '" + OS_Meds.Replace("'", "''") + "', '" + OS_TypeLocPatchLens.Replace("'", "''") + "', '" + OS_Quick1.Replace("'", "''") + "', '" + OS_Quick2.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, TRABECULOPLASTY (65855)")
			{
				string Time = "";
				string OD_NumberofApplications = "";
				string OD_SpotSize = "";
				string OD_EnergyLevel = "";
				string OD_TotalEnergy = "";
				string OD_Duration = "";
				string OD_Degrees = "";
				string OD_TypeandMeds = "";
				string OD_IOP = "";
				string OD_Value = "";
				string OS_NumberofApplications = "";
				string OS_SpotSize = "";
				string OS_EnergyLevel = "";
				string OS_TotalEnergy = "";
				string OS_Duration = "";
				string OS_Degrees = "";
				string OS_TypeandMeds = "";
				string OS_IOP = "";
				string OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_NumberofApplications")
					{
						OD_NumberofApplications = a.Value;
					}
					else if (a.Key == "OD_SpotSize")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_SpotSize";
						}
						else
						{
							OD_SpotSize = a.Value;
						}
					}
					else if (a.Key == "OD_EnergyLevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_EnergyLevel";
						}
						else
						{
							OD_EnergyLevel = a.Value;
						}
					}
					else if (a.Key == "OD_TotalEnergy")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_TotalEnergy";
						}
						else
						{
							OD_TotalEnergy = a.Value;
						}
					}
					else if (a.Key == "OD_Duration")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Duration";
						}
						else
						{
							OD_Duration = a.Value;
						}
					}
					else if (a.Key == "OD_Degrees")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Degrees";
						}
						else
						{
							OD_Degrees = a.Value;
						}
					}
					else if (a.Key == "OD_TypeandMeds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_TypeandMeds";
						}
						else
						{
							OD_TypeandMeds = a.Value;
						}
					}
					else if (a.Key == "OD_IOP")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_IOP";
						}
						else
						{
							OD_IOP = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OD_Degrees")
							{
								OD_Degrees = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Duration")
							{
								OD_Duration = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_EnergyLevel")
							{
								OD_EnergyLevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_IOP")
							{
								OD_IOP = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_SpotSize")
							{
								OD_SpotSize = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_TotalEnergy")
							{
								OD_TotalEnergy = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_TypeandMeds")
							{
								OD_TypeandMeds = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_NumberofApplications")
					{
						OS_NumberofApplications = a.Value;
					}
					else if (a.Key == "OS_SpotSize")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_SpotSize";
						}
						else
						{
							OS_SpotSize = a.Value;
						}
					}
					else if (a.Key == "OS_EnergyLevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_EnergyLevel";
						}
						else
						{
							OS_EnergyLevel = a.Value;
						}
					}
					else if (a.Key == "OS_TotalEnergy")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_TotalEnergy";
						}
						else
						{
							OS_TotalEnergy = a.Value;
						}
					}
					else if (a.Key == "OS_Duration")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Duration";
						}
						else
						{
							OS_Duration = a.Value;
						}
					}
					else if (a.Key == "OS_Degrees")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Degrees";
						}
						else
						{
							OS_Degrees = a.Value;
						}
					}
					else if (a.Key == "OS_TypeandMeds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_TypeandMeds";
						}
						else
						{
							OS_TypeandMeds = a.Value;
						}
					}
					else if (a.Key == "OS_IOP")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_IOP";
						}
						else
						{
							OS_IOP = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OS_Degrees")
							{
								OS_Degrees = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Duration")
							{
								OS_Duration = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_EnergyLevel")
							{
								OS_EnergyLevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_IOP")
							{
								OS_IOP = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_SpotSize")
							{
								OS_SpotSize = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_TotalEnergy")
							{
								OS_TotalEnergy = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_TypeandMeds")
							{
								OS_TypeandMeds = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserTrabeculoplasty65855(AppointmentId, Time, OD_NumberofApplications, OD_SpotSize, OD_EnergyLevel, OD_TotalEnergy, OD_Duration, OD_Degrees, OD_TypeandMeds, OD_IOP, OD_Value, OS_NumberofApplications, OS_SpotSize, OS_EnergyLevel, OS_TotalEnergy, OS_Duration, OS_Degrees, OS_TypeandMeds, OS_IOP, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_NumberofApplications.Replace("'", "''") + "', '" + OD_SpotSize.Replace("'", "''") + "', '" + OD_EnergyLevel.Replace("'", "''") + "', '" + OD_TotalEnergy.Replace("'", "''") + "', '" + OD_Duration.Replace("'", "''") + "', '" + OD_Degrees.Replace("'", "''") + "', '" + OD_TypeandMeds.Replace("'", "''") + "', '" + OD_IOP.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_NumberofApplications.Replace("'", "''") + "', '" + OS_SpotSize.Replace("'", "''") + "', '" + OS_EnergyLevel.Replace("'", "''") + "', '" + OS_TotalEnergy.Replace("'", "''") + "', '" + OS_Duration.Replace("'", "''") + "', '" + OS_Degrees.Replace("'", "''") + "', '" + OS_TypeandMeds.Replace("'", "''") + "', '" + OS_IOP.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, VIT STRANDS (67031)")
			{
				string Time = "";
				string OD_NumberofApplications = "";
				string OD_TypeandMeds = "";
				string OD_Lens = "";
				string OD_EnergyLevel = "";
				string OD_Value = "";
				string OS_NumberofApplications = "";
				string OS_TypeandMeds = "";
				string OS_Lens = "";
				string OS_EnergyLevel = "";
				string OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_NumberofApplications")
					{
						OD_NumberofApplications = a.Value;
					}
					else if (a.Key == "OD_TypeandMeds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_TypeandMeds";
						}
						else
						{
							OD_TypeandMeds = a.Value;
						}
					}
					else if (a.Key == "OD_Lens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Lens";
						}
						else
						{
							OD_Lens = a.Value;
						}
					}
					else if (a.Key == "OD_EnergyLevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_EnergyLevel";
						}
						else
						{
							OD_EnergyLevel = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OD_EnergyLevel")
							{
								OD_EnergyLevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Lens")
							{
								OD_Lens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_TypeandMeds")
							{
								OD_TypeandMeds = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_NumberofApplications")
					{
						OS_NumberofApplications = a.Value;
					}
					else if (a.Key == "OS_TypeandMeds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_TypeandMeds";
						}
						else
						{
							OS_TypeandMeds = a.Value;
						}
					}
					else if (a.Key == "OS_Lens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Lens";
						}
						else
						{
							OS_Lens = a.Value;
						}
					}
					else if (a.Key == "OS_EnergyLevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_EnergyLevel";
						}
						else
						{
							OS_EnergyLevel = a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OS_EnergyLevel")
							{
								OS_EnergyLevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Lens")
							{
								OS_Lens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_TypeandMeds")
							{
								OS_TypeandMeds = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserVITStrands67031(AppointmentId, Time, OD_NumberofApplications, OD_TypeandMeds, OD_Lens, OD_EnergyLevel, OD_Value, OS_NumberofApplications, OS_TypeandMeds, OS_Lens, OS_EnergyLevel, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_NumberofApplications.Replace("'", "''") + "', '" + OD_TypeandMeds.Replace("'", "''") + "', '" + OD_Lens.Replace("'", "''") + "', '" + OD_EnergyLevel.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_NumberofApplications.Replace("'", "''") + "', '" + OS_TypeandMeds.Replace("'", "''") + "', '" + OS_Lens.Replace("'", "''") + "', '" + OS_EnergyLevel.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, YAG, CAPSULOTOMY (66821)")
			{
				string Time = "";
				string OD_Energylevel = "";
				string OD_Applications = "";
				string OD_OpeningSize = "";
				string OD_Medications = "";
				string OD_Lens = "";
				string OD_Value = "";
				string OS_Energylevel = "";
				string OS_Applications = "";
				string OS_OpeningSize = "";
				string OS_Medications = "";
				string OS_Lens = "";
				string OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Energylevel")
					{
						OD_Energylevel = a.Value;
					}
					else if (a.Key == "OD_Applications")
					{
						OD_Applications = a.Value;
					}
					else if (a.Key == "OD_OpeningSize")
					{
						OD_OpeningSize = a.Value;
					}
					else if (a.Key == "OD_Medications")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Medications";
						}
						else
						{
							OD_Medications = a.Value;
						}
					}
					else if (a.Key == "OD_Lens")
					{
						OD_Lens = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OD_Medications")
							{
								OD_Medications = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Energylevel")
					{
						OS_Energylevel = a.Value;
					}
					else if (a.Key == "OS_Applications")
					{
						OS_Applications = a.Value;
					}
					else if (a.Key == "OS_OpeningSize")
					{
						OS_OpeningSize = a.Value;
					}
					else if (a.Key == "OS_Medications")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Medications";
						}
						else
						{
							OS_Medications = a.Value;
						}
					}
					else if (a.Key == "OS_Lens")
					{
						OS_Lens = a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OS_Medications")
							{
								OS_Medications = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserYagAndCapsulotomy66821(AppointmentId, Time, OD_Energylevel, OD_Applications, OD_OpeningSize, OD_Medications, OD_Lens, OD_Value, OS_Energylevel, OS_Applications, OS_OpeningSize, OS_Medications, OS_Lens, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Energylevel.Replace("'", "''") + "', '" + OD_Applications.Replace("'", "''") + "', '" + OD_OpeningSize.Replace("'", "''") + "', '" + OD_Medications.Replace("'", "''") + "', '" + OD_Lens.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Energylevel.Replace("'", "''") + "', '" + OS_Applications.Replace("'", "''") + "', '" + OS_OpeningSize.Replace("'", "''") + "', '" + OS_Medications.Replace("'", "''") + "', '" + OS_Lens.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, YAG, GONIOPUNCTURE")
			{
				string Time = "";
				string OD_Power = "";
				string OD_SpotSize = "";
				string OD_ofShots = "";
				string OD_PulsesperBrust = "";
				string OD_Result = "";
				string OD_IOPSP = "";
				string OD_Value = "";
				string OS_Power = "";
				string OS_SpotSize = "";
				string OS_ofShots = "";
				string OS_PulsesperBrust = "";
				string OS_Result = "";
				string OS_IOPSP = "";
				string OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Power")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Power";
						}
						else
						{
							OD_Power = a.Value;
						}
					}
					else if (a.Key == "OD_SpotSize")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_SpotSize";
						}
						else
						{
							OD_SpotSize = a.Value;
						}
					}
					else if (a.Key == "OD_ofShots")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_ofShots";
						}
						else
						{
							OD_ofShots = a.Value;
						}
					}
					else if (a.Key == "OD_PulsesperBrust")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_PulsesperBrust";
						}
						else
						{
							OD_PulsesperBrust = a.Value;
						}
					}
					else if (a.Key == "OD_Result")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Result";
						}
						else
						{
							OD_Result = a.Value;
						}
					}
					else if (a.Key == "OD_IOPSP")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_IOPSP";
						}
						else
						{
							OD_IOPSP = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OD_Power")
							{
								OD_Power = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_SpotSize")
							{
								OD_SpotSize = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_ofShots")
							{
								OD_ofShots = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_PulsesperBrust")
							{
								OD_PulsesperBrust = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Result")
							{
								OD_Result = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_IOPSP")
							{
								OD_IOPSP = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Power")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Power";
						}
						else
						{
							OS_Power = a.Value;
						}
					}
					else if (a.Key == "OS_SpotSize")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_SpotSize";
						}
						else
						{
							OS_SpotSize = a.Value;
						}
					}
					else if (a.Key == "OS_ofShots")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_ofShots";
						}
						else
						{
							OS_ofShots = a.Value;
						}
					}
					else if (a.Key == "OS_PulsesperBrust")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_PulsesperBrust";
						}
						else
						{
							OS_PulsesperBrust = a.Value;
						}
					}
					else if (a.Key == "OS_Result")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Result";
						}
						else
						{
							OS_Result = a.Value;
						}
					}
					else if (a.Key == "OS_IOPSP")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_IOPSP";
						}
						else
						{
							OS_IOPSP = a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OS_Power")
							{
								OS_Power = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_SpotSize")
							{
								OS_SpotSize = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_ofShots")
							{
								OS_ofShots = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_PulsesperBrust")
							{
								OS_PulsesperBrust = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Result")
							{
								OS_Result = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_IOPSP")
							{
								OS_IOPSP = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserYagAndGoniopuncture(AppointmentId, Time, OD_Power, OD_SpotSize, OD_ofShots, OD_PulsesperBrust, OD_Result, OD_IOPSP, OD_Value, OS_Power, OS_SpotSize, OS_ofShots, OS_PulsesperBrust, OS_Result, OS_IOPSP, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Power.Replace("'", "''") + "', '" + OD_SpotSize.Replace("'", "''") + "', '" + OD_ofShots.Replace("'", "''") + "', '" + OD_PulsesperBrust.Replace("'", "''") + "', '" + OD_Result.Replace("'", "''") + "', '" + OD_IOPSP.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Power.Replace("'", "''") + "', '" + OS_SpotSize.Replace("'", "''") + "', '" + OS_ofShots.Replace("'", "''") + "', '" + OS_PulsesperBrust.Replace("'", "''") + "', '" + OS_Result.Replace("'", "''") + "', '" + OS_IOPSP.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LASER, YAG, IRIDOTOMY (66761)")
			{
				string Time = "";
				string OD_ofApplications = "";
				string OD_EnergyLevel = "";
				string OD_Initiallevel = "";
				string OD_Singleburstlevel = "";
				string OD_Meds = "";
				string OD_Lens = "";
				string OD_Quick = "";
				string OD_Value = "";
				string OS_ofApplications = "";
				string OS_EnergyLevel = "";
				string OS_Initiallevel = "";
				string OS_Singleburstlevel = "";
				string OS_Meds = "";
				string OS_Lens = "";
				string OS_Quick = "";
				string OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_ofApplications")
					{
						OD_ofApplications = a.Value;
					}
					else if (a.Key == "OD_EnergyLevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_EnergyLevel";
						}
						else
						{
							OD_EnergyLevel = a.Value;
						}
					}
					else if (a.Key == "OD_Singleburstlevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Singleburstlevel";
						}
						else
						{
							OD_Singleburstlevel = a.Value;
						}
					}
					else if (a.Key == "OD_Initiallevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Initiallevel";
						}
						else
						{
							OD_Initiallevel = a.Value;
						}
					}
					else if (a.Key == "OD_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Meds";
						}
						else
						{
							OD_Meds = a.Value;
						}
					}
					else if (a.Key == "OD_Lens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Lens";
						}
						else
						{
							OD_Lens = a.Value;
						}
					}
					else if (a.Key == "OD_Quick")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Quick";
						}
						else
						{
							OD_Quick = a.Value;
						}
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OD_EnergyLevel")
							{
								OD_EnergyLevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Initiallevel")
							{
								OD_Initiallevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Singleburstlevel")
							{
								OD_Singleburstlevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Meds")
							{
								OD_Meds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Lens")
							{
								OD_Lens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OD_Quick")
							{
								OD_Quick = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_ofApplications")
					{
						OS_ofApplications = a.Value;
					}
					else if (a.Key == "OS_EnergyLevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_EnergyLevel";
						}
						else
						{
							OS_EnergyLevel = a.Value;
						}
					}
					else if (a.Key == "OS_Singleburstlevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Singleburstlevel";
						}
						else
						{
							OS_Singleburstlevel = a.Value;
						}
					}
					else if (a.Key == "OS_Initiallevel")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Initiallevel";
						}
						else
						{
							OS_Initiallevel = a.Value;
						}
					}
					else if (a.Key == "OS_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Meds";
						}
						else
						{
							OS_Meds = a.Value;
						}
					}
					else if (a.Key == "OS_Lens")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Lens";
						}
						else
						{
							OS_Lens = a.Value;
						}
					}
					else if (a.Key == "OS_Quick")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Quick";
						}
						else
						{
							OS_Quick = a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OS_EnergyLevel")
							{
								OS_EnergyLevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Initiallevel")
							{
								OS_Initiallevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Singleburstlevel")
							{
								OS_Singleburstlevel = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Meds")
							{
								OS_Meds = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Lens")
							{
								OS_Lens = a.Value;
								_specialColumn = "";
							}
							else if (_specialColumn == "OS_Quick")
							{
								OS_Quick = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLaserYagAndIridotomy66761(AppointmentId, Time, OD_ofApplications, OD_EnergyLevel, OD_Initiallevel, OD_Singleburstlevel, OD_Meds, OD_Lens, OD_Quick, OD_Value, OS_ofApplications, OS_EnergyLevel, OS_Initiallevel, OS_Singleburstlevel, OS_Meds, OS_Lens, OS_Quick, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_ofApplications.Replace("'", "''") + "', '" + OD_EnergyLevel.Replace("'", "''") + "', '" + OD_Initiallevel.Replace("'", "''") + "', '" + OD_Singleburstlevel.Replace("'", "''") + "', '" + OD_Meds.Replace("'", "''") + "', '" + OD_Lens.Replace("'", "''") + "', '" + OD_Quick.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_ofApplications.Replace("'", "''") + "', '" + OS_EnergyLevel.Replace("'", "''") + "', '" + OS_Initiallevel.Replace("'", "''") + "', '" + OS_Singleburstlevel.Replace("'", "''") + "', '" + OS_Meds.Replace("'", "''") + "', '" + OS_Lens.Replace("'", "''") + "', '" + OS_Quick.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/LRI")
			{
				string Time = "";
				string OD_Incisions = "";
				string OD_Arc = "";
				string OD_Meds = "";
				string OD_Axis = "";
				string OD_Lengthmm = "";
				string OD_Value = "";
				string OS_Incisions = "";
				string OS_Arc = "";
				string OS_Meds = "";
				string OS_Axis = "";
				string OS_Lengthmm = "";
				string OS_Value = "";
				string _specialColumn = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Incisions")
					{
						OD_Incisions = a.Value;
					}
					else if (a.Key == "OD_Arc")
					{
						OD_Arc = a.Value;
					}
					else if (a.Key == "OD_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OD_Meds";
						}
						else
						{
							OD_Meds = a.Value;
						}
					}
					else if (a.Key == "OD_Axis")
					{
						OD_Axis = a.Value;
					}
					else if (a.Key == "OD_Lengthmm")
					{
						OD_Lengthmm = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OD_Meds")
							{
								OD_Meds = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OD_Value += " " + a.Value;
						}
					}
					else if (a.Key == "OS_Incisions")
					{
						OS_Incisions = a.Value;
					}
					else if (a.Key == "OS_Arc")
					{
						OS_Arc = a.Value;
					}
					else if (a.Key == "OS_Axis")
					{
						OS_Axis = a.Value;
					}
					else if (a.Key == "OS_Lengthmm")
					{
						OS_Lengthmm = a.Value;
					}
					else if (a.Key == "OS_Meds")
					{
						if (a.Value.TrimEnd().TrimStart() == ";")
						{
							_specialColumn = "OS_Meds";
						}
						else
						{
							OS_Meds = a.Value;
						}
					}
					else if (a.Key == "OS_Value")
					{
						if (_specialColumn != "")
						{
							if (_specialColumn == "OS_Meds")
							{
								OS_Meds = a.Value;
								_specialColumn = "";
							}
						}
						else
						{
							OS_Value += " " + a.Value;
						}
					}
				}
				string _insertQry = "Insert into ExtractedLRI(AppointmentId, Time, OD_Incisions, OD_Arc, OD_Meds, OD_Axis, OD_Lengthmm, OD_Value, OS_Incisions, OS_Arc, OS_Meds, OS_Axis, OS_Lengthmm, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Incisions.Replace("'", "''") + "', '" + OD_Arc.Replace("'", "''") + "', '" + OD_Meds.Replace("'", "''") + "', '" + OD_Axis.Replace("'", "''") + "', '" + OD_Lengthmm.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "','" + OS_Incisions.Replace("'", "''") + "', '" + OS_Arc.Replace("'", "''") + "', '" + OS_Meds.Replace("'", "''") + "', '" + OS_Axis.Replace("'", "''") + "', '" + OS_Lengthmm.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/MASS LESION EXCISION")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedMassLesionExcision(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/PUNCTUM PROBE AND IRRIGATION")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedPunctumProbeAndIrrigation(AppointmentId, Time, OD_Value, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/XEOMIN, MEDICAL")
			{
				string Time = "";
				string OD_Lot = "";
				string OD_Vialexpdate = "";
				string OD_unitsper1cc = "";
				string OD_ofunitsused = "";
				string OD_Value = "";
				string OS_Lot = "";
				string OS_Vialexpdate = "";
				string OS_unitsper1cc = "";
				string OS_ofunitsused = "";
				string OS_Value = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Lot")
					{
						OD_Lot = a.Value;
					}
					else if (a.Key == "OD_Vialexpdate")
					{
						OD_Vialexpdate = a.Value;
					}
					else if (a.Key == "OD_unitsper1cc")
					{
						OD_unitsper1cc = a.Value;
					}
					else if (a.Key == "OD_ofunitsused")
					{
						OD_ofunitsused = a.Value;
					}
					else if (a.Key == "OD_Vialexpdate")
					{
						OD_Vialexpdate = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Lot")
					{
						OS_Lot = a.Value;
					}
					else if (a.Key == "OS_Vialexpdate")
					{
						OS_Vialexpdate = a.Value;
					}
					else if (a.Key == "OS_unitsper1cc")
					{
						OS_unitsper1cc = a.Value;
					}
					else if (a.Key == "OD_ofunitsused")
					{
						OD_ofunitsused = a.Value;
					}
					else if (a.Key == "OS_ofunitsused")
					{
						OS_ofunitsused = a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedXeominAndMedical(AppointmentId, Time, OD_Lot, OD_Vialexpdate, OD_unitsper1cc, OD_ofunitsused, OD_Value, OS_Lot, OS_Vialexpdate, OS_unitsper1cc, OS_ofunitsused, OS_Value) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Lot.Replace("'", "''") + "', '" + OD_Vialexpdate.Replace("'", "''") + "', '" + OD_unitsper1cc.Replace("'", "''") + "', '" + OD_ofunitsused.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OS_Lot.Replace("'", "''") + "', '" + OS_Vialexpdate.Replace("'", "''") + "', '" + OS_unitsper1cc.Replace("'", "''") + "', '" + OS_ofunitsused.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/BOTOX, COSMETIC")
			{
				string Time = "";
				string TYPEUSED_BTXSTRA = "";
				string LOTNUMBER_BTXC = "";
				string VIALEXPDATE_BTXC = "";
				string DILUTION_BTXC = "";
				string NUMBEROFUNITS_BTXC = "";
				string typeValue = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "TYPEUSED_BTXSTRA")
					{
						TYPEUSED_BTXSTRA = a.Value;
					}
					else if (a.Key == "LOTNUMBER_BTXC")
					{
						LOTNUMBER_BTXC = a.Value;
					}
					else if (a.Key == "VIALEXPDATE_BTXC")
					{
						VIALEXPDATE_BTXC = a.Value;
					}
					else if (a.Key == "DILUTION_BTXC")
					{
						DILUTION_BTXC = a.Value;
					}
					else if (a.Key == "NUMBEROFUNITS_BTXC")
					{
						NUMBEROFUNITS_BTXC = a.Value;
					}
					else if (a.Key == "typeValue")
					{
						typeValue += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedBotoxAndCosmetic(AppointmentId, Time, TYPEUSED_BTXSTRA, LOTNUMBER_BTXC, VIALEXPDATE_BTXC, DILUTION_BTXC, NUMBEROFUNITS_BTXC, TypeValue) Values ('" + _appointmentId + "', '" + Time + "', '" + TYPEUSED_BTXSTRA.Replace("'", "''") + "', '" + LOTNUMBER_BTXC.Replace("'", "''") + "', '" + VIALEXPDATE_BTXC.Replace("'", "''") + "', '" + DILUTION_BTXC.Replace("'", "''") + "', '" + NUMBEROFUNITS_BTXC.Replace("'", "''") + "', '" + typeValue.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
			else if (Name == "/AMNIOTIC MEMBRANE, PLACEMENT")
			{
				string Time = "";
				string OD_Type = "";
				string OD_Value = "";
				string OD_LensSize = "";
				string OD_LensBrand = "";
				string OD_ExpirationDate = "";
				string OS_Type = "";
				string OS_Value = "";
				string OS_LensSize = "";
				string OS_LensBrand = "";
				string OS_ExpirationDate = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Type")
					{
						OD_Type = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OD_LensSize")
					{
						OD_LensSize = a.Value;
					}
					else if (a.Key == "OD_LensBrand")
					{
						OD_LensBrand = a.Value;
					}
					else if (a.Key == "OD_ExpirationDate")
					{
						OD_ExpirationDate = a.Value;
					}
					else if (a.Key == "OS_Type")
					{
						OS_Type = a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
					else if (a.Key == "OS_LensSize")
					{
						OS_LensSize = a.Value;
					}
					else if (a.Key == "OS_LensBrand")
					{
						OS_LensBrand = a.Value;
					}
					else if (a.Key == "OS_ExpirationDate")
					{
						OS_ExpirationDate = a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedAmnioticMembraneAndPlacement(AppointmentId, Time, OD_Type, OD_Value, OD_LensSize, OD_LensBrand, OD_ExpirationDate, OS_Type, OS_Value, OS_LensSize, OS_LensBrand, OS_ExpirationDate) Values ('" + _appointmentId + "', '" + Time + "', '" + OD_Type.Replace("'", "''") + "', '" + OD_Value.Replace("'", "''") + "', '" + OD_LensSize.Replace("'", "''") + "', '" + OD_LensBrand.Replace("'", "''") + "', '" + OD_ExpirationDate.Replace("'", "''") + "', '" + OS_Type.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "', '" + OS_LensSize.Replace("'", "''") + "', '" + OS_LensBrand.Replace("'", "''") + "', '" + OS_ExpirationDate.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}

			#endregion

			#region Fourth Phase 

			else if (Name == "/PACHYMETRY")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";
				string OD_ThicknessReading1 = "";
				string OD_ThicknessReading2 = "";
				string OD_ThicknessReading3 = "";
				string OD_IOPAdjustment = "";
				string OD_Implications = "";
				string OD_Impact = "";
				string OS_ThicknessReading1 = "";
				string OS_ThicknessReading2 = "";
				string OS_ThicknessReading3 = "";
				string OS_IOPAdjustment = "";
				string OS_Implications = "";
				string OS_Impact = "";
				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
					else if (a.Key == "OD_ThicknessReading1")
					{
						OD_ThicknessReading1 += " " + a.Value;
					}
					else if (a.Key == "OD_ThicknessReading2")
					{
						OD_ThicknessReading2 += " " + a.Value;
					}
					else if (a.Key == "OD_ThicknessReading3")
					{
						OD_ThicknessReading3 += " " + a.Value;
					}
					else if (a.Key == "OD_IOPAdjustment")
					{
						OD_IOPAdjustment += " " + a.Value;
					}
					else if (a.Key == "OD_Implications")
					{
						OD_Implications += " " + a.Value;
					}
					else if (a.Key == "OD_Impact")
					{
						OD_Impact += " " + a.Value;
					}
					else if (a.Key == "OS_ThicknessReading1")
					{
						OS_ThicknessReading1 += " " + a.Value;
					}
					else if (a.Key == "OS_ThicknessReading2")
					{
						OS_ThicknessReading2 += " " + a.Value;
					}
					else if (a.Key == "OS_ThicknessReading3")
					{
						OS_ThicknessReading3 += " " + a.Value;
					}
					else if (a.Key == "OS_IOPAdjustment")
					{
						OS_IOPAdjustment += " " + a.Value;
					}
					else if (a.Key == "OS_Implications")
					{
						OS_Implications += " " + a.Value;
					}
					else if (a.Key == "OS_Impact")
					{
						OS_Impact += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedPachymetryDetails(AppointmentId, OD_Value, OD_ThicknessReading1, OD_ThicknessReading2, OD_ThicknessReading3, OD_IOPAdjustment, OD_Implications, OD_Impact, OS_Value, OS_ThicknessReading1, OS_ThicknessReading2, OS_ThicknessReading3, OS_IOPAdjustment, OS_Implications, OS_Impact) Values ('" + _appointmentId + "', '" + OD_Value.Replace("'", "''") + "', '" + OD_ThicknessReading1.Replace("'", "''") + "', '" + OD_ThicknessReading2.Replace("'", "''") + "', '" + OD_ThicknessReading3.Replace("'", "''") + "', '" + OD_IOPAdjustment.Replace("'", "''") + "', '" + OD_Implications.Replace("'", "''") + "', '" + OD_Impact.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''").Replace("ThicknessReading1", "").Replace("ThicknessReading2", "").Replace("ThicknessReading3", "") + "', '" + OS_ThicknessReading1.Replace("'", "''") + "', '" + OS_ThicknessReading2.Replace("'", "''") + "', '" + OS_ThicknessReading3.Replace("'", "''") + "', '" + OS_IOPAdjustment.Replace("'", "''") + "', '" + OS_Implications.Replace("'", "''") + "', '" + OS_Impact.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}

			else if (Name == "/PACHYMETRY TECH")
			{
				string Time = "";
				string OD_Value = "";
				string OS_Value = "";
				string OD_ThicknessReading = "";
				string OD_PatientUnderstanding = "";
				string OD_Reliability = "";
				string OD_Method = "";
				string OS_ThicknessReading = "";
				string OS_PatientUnderstanding = "";
				string OS_Reliability = "";
				string OS_Method = "";

				foreach (KeyValuePair<string, string> a in Content.keyElement)
				{
					if (a.Key == "TIME")
					{
						Time = a.Value;
					}
					else if (a.Key == "OD_Value")
					{
						OD_Value += " " + a.Value;
					}
					else if (a.Key == "OS_Value")
					{
						OS_Value += " " + a.Value;
					}
					else if (a.Key == "OD_ThicknessReading")
					{
						OD_ThicknessReading += " " + a.Value;
					}
					else if (a.Key == "OD_PatientUnderstanding")
					{
						OD_PatientUnderstanding += " " + a.Value;
					}
					else if (a.Key == "OD_Reliability")
					{
						OD_Reliability += " " + a.Value;
					}
					else if (a.Key == "OD_Method")
					{
						OD_Method += " " + a.Value;
					}
					else if (a.Key == "OS_ThicknessReading")
					{
						OS_ThicknessReading += " " + a.Value;
					}
					else if (a.Key == "OS_PatientUnderstanding")
					{
						OS_PatientUnderstanding += " " + a.Value;
					}
					else if (a.Key == "OS_Reliability")
					{
						OS_Reliability += " " + a.Value;
					}
					else if (a.Key == "OS_Method")
					{
						OS_Method += " " + a.Value;
					}
				}
				string _insertQry = "Insert into ExtractedPachymetryTechDetails(AppointmentId, OD_Value, OD_ThicknessReading, OD_PatientUnderstanding, OD_Reliability, OD_Method, OS_Value, OS_ThicknessReading, OS_PatientUnderstanding, OS_Reliability, OS_Method) Values ('" + _appointmentId + "', '" + OD_Value.Replace("'", "''") + "', '" + OD_ThicknessReading.Replace("'", "''") + "', '" + OD_PatientUnderstanding.Replace("'", "''") + "', '" + OD_Reliability.Replace("'", "''") + "', '" + OD_Method.Replace("'", "''") + "', '" + OS_Value.Replace("'", "''") + "', '" + OS_ThicknessReading.Replace("'", "''") + "', '" + OS_PatientUnderstanding.Replace("'", "''") + "', '" + OS_Reliability.Replace("'", "''") + "', '" + OS_Method.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}

			#endregion
		}
	}

	public class EyeContext
	{
		public List<KeyValuePair<string, string>> keyElement { get; set; }
	}
}
