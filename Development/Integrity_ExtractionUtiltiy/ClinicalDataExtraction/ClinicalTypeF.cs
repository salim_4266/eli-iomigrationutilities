﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ClinicalDataExtraction
{
	public class ClinicalTypeF
	{
		private static List<MasterElementModel> ElementList = null;

		static ClinicalTypeF()
		{
			ElementList = new MasterElements().Get();
		}

		public static void Extract(string appointmentId)
		{
			var dsSymptoms = new DataTable();
			AppointmentExamElement objAEE = new AppointmentExamElement();
			objAEE.AppointmentId = appointmentId;
			string _selectQry = "Select Distinct Symptom From dbo.PatientClinical with (nolock) Where AppointmentId = '" + appointmentId + "' and ClinicalType = 'F' and Symptom not in ('/BRIGHTNESS ACUITY TEST', '/COLOR PLATES', '/COLOR SCREEN', '/GLARE', '/PHYSICAL EXAM', '/HEIGHT AND WEIGHT', '/BLOOD PRESSURE')";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(_selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(dsSymptoms);
					}
				}
			}
			if (dsSymptoms.Rows.Count > 0)
			{
				objAEE.ElementDetail = getDetail(dsSymptoms, appointmentId);
			}
		}

		private static List<ExamElement> getDetail(DataTable dtSymptom, string _appointmentId)
		{
			List<ExamElement> objListExamElement = new List<ExamElement>();
			foreach (DataRow dt in dtSymptom.Rows)
			{
				ExamElement objElement = new ExamElement()
				{
					Name = dt["Symptom"].ToString(),
					Content = getExamDetail(dt["Symptom"].ToString(), _appointmentId)
				};
				objElement.Save(_appointmentId);
				objListExamElement.Add(objElement);
			}
			return objListExamElement;
		}

		private static EyeContext getExamDetail(string _symptomType, string _appointmentId)
		{
			bool phDistanceValue = false;
			string _eyeType = "";
			EyeContext objEyeContext = new EyeContext();
			List<KeyValuePair<string, string>> objkeyPairs = new List<KeyValuePair<string, string>>();
			DataTable dtElement = new DataTable();
			string _selectQry = "Select FindingDetail From dbo.PatientClinical with (nolock) Where Symptom = '" + _symptomType + "' and AppointmentId = '" + _appointmentId + "' and Status = 'A' and ClinicalType = 'F' Order by ClinicalId";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(_selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(dtElement);
					}
				}
			}
			foreach (DataRow dr in dtElement.Rows)
			{
				int startIndex = 0;
				int endIndex = 0;
				string tempControlId = "";
				if (dr["FindingDetail"].ToString().Contains("TIME"))
				{
					phDistanceValue = false;
					_eyeType = "";
					startIndex = dr["FindingDetail"].ToString().IndexOf('=');
					endIndex = dr["FindingDetail"].ToString().IndexOf('(');
					if (endIndex != -1)
						objkeyPairs.Add(new KeyValuePair<string, string>("TIME", dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex - 1))));
					else
						objkeyPairs.Add(new KeyValuePair<string, string>("TIME", dr["FindingDetail"].ToString().Substring(startIndex + 1, 5)));
				}
				else if (dr["FindingDetail"].ToString().Contains("PH-DISTANCE"))
				{
					phDistanceValue = true;
				}
				else if ((dr["FindingDetail"].ToString().Contains("*OD-") || _eyeType == "OD") && !dr["FindingDetail"].ToString().Contains("*OS-"))
				{
					if (_eyeType == "")
					{
						_eyeType = "OD";
					}
					else
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('=');
						endIndex = dr["FindingDetail"].ToString().IndexOf('<');
						tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
						FormControls objForm1 = new FormControls(tempControlId);
						if (objForm1.ControlType == "N")
						{
							startIndex = dr["FindingDetail"].ToString().IndexOf('<');
							endIndex = dr["FindingDetail"].ToString().IndexOf('>');
							if (ElementList.FirstOrDefault(x => x.Name == _symptomType.Replace("/", "")) != null)
							{
								objkeyPairs.Add(new KeyValuePair<string, string>("OD_" + objForm1.ControlText.Replace("'", "''"), dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
							}
							else
							{
								if (phDistanceValue && _symptomType.Contains("CORRECTED"))
								{
									objkeyPairs.Add(new KeyValuePair<string, string>("PH_OD_Value", objForm1.ControlText.Replace("'", "''") + " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
								}
								else
								{
									objkeyPairs.Add(new KeyValuePair<string, string>("OD_Value", objForm1.ControlText.Replace("'", "''") + " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
								}
							}
						}
						else
						{
							if (phDistanceValue && _symptomType.Contains("CORRECTED"))
							{
								objkeyPairs.Add(new KeyValuePair<string, string>("PH_OD_Value", objForm1.ControlText.Replace("'", "''")));
							}
							else
							{
								objkeyPairs.Add(new KeyValuePair<string, string>("OD_Value", objForm1.ControlText.Replace("'", "''")));
							}
						}
					}
				}
				else if (dr["FindingDetail"].ToString().Contains("*OS-") || _eyeType == "OS")
				{
					if (_eyeType != "OS")
					{
						_eyeType = "OS";
					}
					else
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('=');
						endIndex = dr["FindingDetail"].ToString().IndexOf('<');
						tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
						FormControls objForm1 = new FormControls(tempControlId);
						if (objForm1.ControlType == "N")
						{
							startIndex = dr["FindingDetail"].ToString().IndexOf('<');
							endIndex = dr["FindingDetail"].ToString().IndexOf('>');
							if (ElementList.FirstOrDefault(x => x.Name == _symptomType.Replace("/", "")) != null)
							{
								objkeyPairs.Add(new KeyValuePair<string, string>("OS_Value", objForm1.ControlText.Replace("'", "''") + " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
							}
							else
							{
								if (phDistanceValue && _symptomType.Contains("CORRECTED"))
								{
									objkeyPairs.Add(new KeyValuePair<string, string>("PH_OS_Value", objForm1.ControlText.Replace("'", "''") + " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
								}
								else
								{
									objkeyPairs.Add(new KeyValuePair<string, string>("OS_Value", objForm1.ControlText.Replace("'", "''") + " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
								}
							}
						}
						else
						{
							if (phDistanceValue && _symptomType.Contains("CORRECTED"))
							{
								objkeyPairs.Add(new KeyValuePair<string, string>("PH_OS_Value", objForm1.ControlText.Replace("'", "''")));
							}
							else
							{
								objkeyPairs.Add(new KeyValuePair<string, string>("OS_Value", objForm1.ControlText));
							}
						}
					}
				}
				else
				{
					if (_symptomType == "/SMOKING" || _symptomType == "/GENERAL MEDICAL OBSERVATION")
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('=');
						endIndex = dr["FindingDetail"].ToString().IndexOf('<');
						tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
						FormControls objForm1 = new FormControls(tempControlId);
						if (objForm1.ControlType == "N")
						{
							startIndex = dr["FindingDetail"].ToString().IndexOf('<');
							endIndex = dr["FindingDetail"].ToString().IndexOf('>');
							objkeyPairs.Add(new KeyValuePair<string, string>("typeValue", (objForm1.LetterTranslation.Trim() != "" ? objForm1.LetterTranslation.Replace("'", "''") : objForm1.ControlText.Replace("'", "''"))));
						}
						else
						{
							objkeyPairs.Add(new KeyValuePair<string, string>("typeValue", (objForm1.LetterTranslation.Trim() != "" ? objForm1.LetterTranslation.Replace("'", "''") : objForm1.ControlText.Replace("'", "''"))));
						}
					}
					else if (_symptomType == "/VISION CORRECTED" || _symptomType == "/VISION UNCORRECTED")
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('=');
						endIndex = dr["FindingDetail"].ToString().IndexOf('<');
						tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
						FormControls objForm1 = new FormControls(tempControlId);
						if (objForm1.ControlType == "R")
						{
							objkeyPairs.Add(new KeyValuePair<string, string>("typeValue", (objForm1.LetterTranslation.Trim() != "" ? objForm1.LetterTranslation.Replace("'", "''") : objForm1.ControlText.Replace("'", "''"))));
						}
					}
					else if (_symptomType == "/BOTOX, COSMETIC")
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('=');
						endIndex = dr["FindingDetail"].ToString().IndexOf('<');
						tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
						FormControls objForm1 = new FormControls(tempControlId);
						if (objForm1.ControlType == "N")
						{
							startIndex = dr["FindingDetail"].ToString().IndexOf('*');
							endIndex = dr["FindingDetail"].ToString().IndexOf('=');
							string _columnName = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''").Replace("-", "_");
							startIndex = dr["FindingDetail"].ToString().IndexOf('<');
							endIndex = dr["FindingDetail"].ToString().IndexOf('>');
							objkeyPairs.Add(new KeyValuePair<string, string>(_columnName, dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
						}
						if (objForm1.ControlType == "B")
						{
							string _columnName = "typeValue";
							objkeyPairs.Add(new KeyValuePair<string, string>(_columnName, objForm1.ControlText.Replace("'", "''")));
						}
					}
				}
			}
			objEyeContext.keyElement = objkeyPairs;
			return objEyeContext;
		}
	}
}
