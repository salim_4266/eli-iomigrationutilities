﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
    public class DBTables
    {
        public string Name { get; set; }
        public List<Column> Columns { get; set; }
        public string tableId { get; set; }

        public DBTables()
        {

        }

        public DBTables(string tableName)
        {
            this.Name = tableName;
            string _selectQry = "Select a.name as Col1, a.object_Id as Col2 From Sys.Columns a with (nolock) inner join Sys.Tables b on a.Object_Id = b.Object_Id Where b.name = '" + tableName + "'";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Columns.Add(new Column { _name = reader["Col1"].ToString(), _objectId = reader["Col2"].ToString() });
                    }
                }
                con.Close();
            }
        }
    }

    public class Column
    {
        public string _name { get; set; }
        public string _objectId { get; set; }
    }
}
