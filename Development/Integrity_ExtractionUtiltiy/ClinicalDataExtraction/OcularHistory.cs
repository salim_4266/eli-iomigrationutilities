﻿using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
    public class OcularHistory
    {
		public string Symptom { get; set; } = "";
        public string Finding { get; set; } = "";
		public string _HAVE_CONJUNCTIVITIS_DID_WEAR { get; set; } = "";
		public string _HAVE_ULCERS_DID_WEAR { get; set; } = "";
		public string _HOW_LONG { get; set; } = "";
		public string _HOW_OFTEN_WEAR_DID_WEAR { get; set; } = "";
		public string _MEDICAL_LENSES_DID_WEAR { get; set; } = "";
		public string _WHAT_TYPE_DID_WEAR { get; set; } = "";
		public string _WHAT_TYPE_DO_WEAR { get; set; } = "";
		public string _WHEN { get; set; } = "";
		public string _WHEN_STOP { get; set; } = "";
		public string _WHICH_EYE_TYPE { get; set; } = "";
		public string _WHICH_KIND { get; set; } = "";
		public string _WHICH_SIDE { get; set; } = "";
		public string _WHY_STOP { get; set; } = "";
		public string _WHEN_SURGERY_LEFT_BOTH { get; set; } = "";
		public string _WHEN_SURGERY_LEFT_ONLY { get; set; } = "";
		public string _WHEN_SURGERY_RIGHT_BOTH { get; set; } = "";
		public string _WHEN_SURGERY_RIGHT_ONLY { get; set; } = "";
		public string _COMPLICATIONS_SURGERY { get; set; } = "";
		public string _HAVE_SURGERY_LEFT_BOTH { get; set; } = "";
		public string _HAVE_SURGERY_LEFT_ONLY { get; set; } = "";
		public string _HAVE_SURGERY_RIGHT_BOTH { get; set; } = "";
		public string _HAVE_SURGERY_RIGHT_ONLY { get; set; } = "";
		public string _HOW_MANY_TIMES_SURGERY { get; set; } = "";
		public string _HOW_OFTEN_BOTH { get; set; } = "";
		public string _HOW_OFTEN_LEFT_BOTH { get; set; } = "";
		public string _HOW_OFTEN_RIGHT_BOTH { get; set; } = "";
		public string _HOW_OFTEN_RIGHT_ONLY { get; set; } = "";
		public string _PATCH_RIGHT { get; set; } = "";
		public string _TYPE_EYE_CANCER_RIGHT { get; set; } = "";
		public string _WHEN_DIAGNOSED_LEFT_BOTH { get; set; } = "";
		public string _WHEN_DIAGNOSED_LEFT_ONLY { get; set; } = "";
		public string _WHEN_DIAGNOSED_RIGHT_BOTH { get; set; } = "";
		public string _WHEN_DIAGNOSED_RIGHT_ONLY { get; set; } = "";
		public string _WHEN_LAST__SURGERY { get; set; } = "";
		public string _WHEN_SURGERY { get; set; } = "";
		public string _WHICH_EYE { get; set; } = "";

		public void Add(string _appointmentId)
        {
			string _selectQry = "Select 1 From ExtractedOcularHistoryDetails Where AppointmentId  = '" + _appointmentId + "'" + " and Symptom = '" + Symptom.Replace("'", "''") + "' and ButtonText = '" + Finding.Replace("'", "''")+"'";
			DataTable dsSymptoms = new DataTable();
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(_selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(dsSymptoms);
					}
				}
			}

			if (dsSymptoms.Rows.Count == 0)
			{
				string _insertQry = "Insert into ExtractedOcularHistoryDetails(AppointmentId, Symptom, ButtonText, HAVE_CONJUNCTIVITIS_DID_WEAR, HAVE_ULCERS_DID_WEAR, HOW_LONG, HOW_OFTEN_WEAR_DID_WEAR, MEDICAL_LENSES_DID_WEAR, WHAT_TYPE_DO_WEAR, [WHEN], WHEN_STOP, WHICH_EYE_TYPE, WHICH_KIND, WHICH_SIDE, WHY_STOP, WHEN_SURGERY_LEFT_BOTH, WHEN_SURGERY_LEFT_ONLY, WHEN_SURGERY_RIGHT_BOTH, WHEN_SURGERY_RIGHT_ONLY, COMPLICATIONS_SURGERY, HAVE_SURGERY_LEFT_BOTH, HAVE_SURGERY_LEFT_ONLY, HAVE_SURGERY_RIGHT_BOTH, HAVE_SURGERY_RIGHT_ONLY, HOW_MANY_TIMES_SURGERY, HOW_OFTEN_BOTH, HOW_OFTEN_LEFT_BOTH, HOW_OFTEN_RIGHT_BOTH, HOW_OFTEN_RIGHT_ONLY, PATCH_RIGHT, TYPE_EYE_CANCER_RIGHT, WHEN_DIAGNOSED_LEFT_BOTH, WHEN_DIAGNOSED_LEFT_ONLY, WHEN_DIAGNOSED_RIGHT_BOTH, WHEN_DIAGNOSED_RIGHT_ONLY, WHEN_LAST__SURGERY, WHEN_SURGERY, WHICH_EYE) Values ('" + _appointmentId + "', '" + Symptom.Replace("'", "''") + "', '" + Finding.Replace("'", "''") + "', '" + _HAVE_CONJUNCTIVITIS_DID_WEAR.Replace("'", "''") + "', '" + _HAVE_ULCERS_DID_WEAR.Replace("'", "''") + "', '" + _HOW_LONG.Replace("'", "''") + "', '" + _HOW_OFTEN_WEAR_DID_WEAR.Replace("'", "''") + "', '" + _MEDICAL_LENSES_DID_WEAR.Replace("'", "''") + "', '" + _WHAT_TYPE_DID_WEAR.Replace("'", "''") + "','" + _WHAT_TYPE_DO_WEAR.Replace("'", "''") + "', '" + _WHEN.Replace("'", "''") + "', '" + _WHEN_STOP.Replace("'", "''") + "', '" + _WHICH_EYE_TYPE.Replace("'", "''") + "', '" + _WHICH_KIND.Replace("'", "''") + "', '" + _WHICH_SIDE.Replace("'", "''") + "', '" + _WHY_STOP.Replace("'", "''") + "', '" + _WHEN_SURGERY_LEFT_BOTH.Replace("'", "''") + "', '" + _WHEN_SURGERY_LEFT_ONLY.Replace("'", "''") + "', '" + _WHEN_SURGERY_RIGHT_BOTH.Replace("'", "''") + "', '" + _WHEN_SURGERY_RIGHT_ONLY.Replace("'", "''") + "', '" + _COMPLICATIONS_SURGERY.Replace("'", "''") + "', '" + _HAVE_SURGERY_LEFT_BOTH.Replace("'", "''") + "', '" + _HAVE_SURGERY_LEFT_ONLY.Replace("'", "''") + "', '" + _HAVE_SURGERY_RIGHT_ONLY.Replace("'", "''") + "', '" + _HOW_MANY_TIMES_SURGERY.Replace("'", "''") + "', '" + _HOW_OFTEN_BOTH.Replace("'", "''") + "', '" + _HOW_OFTEN_LEFT_BOTH.Replace("'", "''") + "', '" + _HOW_OFTEN_RIGHT_BOTH.Replace("'", "''") + "', '" + _HOW_OFTEN_RIGHT_ONLY.Replace("'", "''") + "', '" + _PATCH_RIGHT.Replace("'", "''") + "', '" + _TYPE_EYE_CANCER_RIGHT.Replace("'", "''") + "', '" + _WHEN_DIAGNOSED_LEFT_BOTH.Replace("'", "''") + "', '" + _WHEN_DIAGNOSED_LEFT_ONLY.Replace("'", "''") + "', '" + _WHEN_DIAGNOSED_RIGHT_BOTH.Replace("'", "''") + "', '" + _WHEN_DIAGNOSED_RIGHT_ONLY.Replace("'", "''") + "', '" + _WHEN_LAST__SURGERY.Replace("'", "''") + "', '" + _WHEN_SURGERY.Replace("'", "''") + "', '" + _WHICH_EYE.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
        }
    }
}
