﻿using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
    public class CLInventory
    {
        public string ModelName { get; set; }

        public CLInventory()
        {

        }
     
        public CLInventory(string _CLInventoryId)
        {
            string _selectQry = "Select * From dbo.CLInventory Where InventoryId = '" + _CLInventoryId + "'";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                {
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        this.ModelName = rdr["Series"].ToString() + " " + rdr["Type_"].ToString();
                    }
                }
                con.Close();
            }
        }

    }
}
