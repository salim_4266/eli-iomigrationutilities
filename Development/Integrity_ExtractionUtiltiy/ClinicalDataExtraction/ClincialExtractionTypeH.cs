﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace ClinicalDataExtraction
{
	public sealed class ClincialExtractionTypeH
	{
		private static List<SecondaryFormControlModel> secondFormControlModelList { get; set; }

		static ClincialExtractionTypeH()
		{
			secondFormControlModelList = new SecondaryFormControls().Get();
		}

		public static void Extarct_PMH_HPI_OCX(string _appointmentId)
		{
			DataTable dsSymptoms = new DataTable();
			string _selectQry = "Select ClinicalId, Symptom, FindingDetail, ImageDescriptor From dbo.PatientClinical with(nolock) Where AppointmentId = '" + _appointmentId + "' and ClinicalType in ('H', 'C') and Symptom not like '%Family%' and Symptom not like '%CHIEF%' Order by ClinicalId";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(_selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(dsSymptoms);
					}
				}
			}
			var pastMedicalHistoryList = new List<PastMedicalHistory>();
			var appointmentHPIList = new List<AppointmentHPI>();
			var ocularHistoryList = new List<OcularHistory>();
			if (dsSymptoms.Rows.Count > 0)
			{
				string columnContent = "";
				int index = 0;
				int searchingIndex = 0;
				
				dynamic pastmedicalHistory = null;
				dynamic appointmentHPI = null;
				dynamic ocularHistory = null;

				foreach (DataRow dr in dsSymptoms.Rows)
				{
					// Past Medical History
					if (dr["Symptom"].Equals("/DM, HTN, HEART PROBS, HI CHOL, CART/ART DISEASE, TEMP ARTERITIS, PREMATURITY, BIRTH PROBS, GENETIC PROBS, BRAIN PROBS")
					|| dr["Symptom"].Equals("/STROKE,  CANCER,  HIV/AIDS,  HEPATITIS,  ALCOHOLISM,  ASTHMA,  ARTHRITIS, LYME, GALL BLADDER")
					|| dr["Symptom"].Equals("/SICKLE CELL,  ULCERATED COLITIS,  EMOTIONAL PROBS,  SKIN CONDITIONS,  LUPUS,  THYROID,  ARTIFICIAL ORGAN,  PARKINSONS,  MS")
					|| dr["Symptom"].Equals("/DIALYSIS, NUTRITIONAL DEFICIENCY, HEADACHES, HEARING PROBS, VOCAL PROBS, SMELL PROBS, PROBS W/TASTING, OTHER SURGERIES"))
					{
						if (pastmedicalHistory != null && !string.IsNullOrEmpty(pastmedicalHistory.Symptom) && !string.IsNullOrEmpty(pastmedicalHistory.Finding))
						{
							pastMedicalHistoryList.Add(pastmedicalHistory);
							pastmedicalHistory = null;
							searchingIndex = 0;
						}

						if (appointmentHPI != null && !string.IsNullOrEmpty(appointmentHPI.Symptom) && !string.IsNullOrEmpty(appointmentHPI.Finding))
						{
							appointmentHPIList.Add(appointmentHPI);
							appointmentHPI = null;
							searchingIndex = 0;
						}

						if (ocularHistory != null && !string.IsNullOrEmpty(ocularHistory.Symptom) && !string.IsNullOrEmpty(ocularHistory.Finding))
						{
							ocularHistoryList.Add(ocularHistory);
							ocularHistory = null;
							searchingIndex = 0;
						}

						searchingIndex = 1;
						pastmedicalHistory = new PastMedicalHistory();
						if (pastmedicalHistory != null && dr["FindingDetail"].ToString().Contains("="))
						{	
							index = dr["FindingDetail"].ToString().IndexOf("=");
							columnContent = dr["FindingDetail"].ToString().Substring(0, index).Replace("*", "");
							string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
							if (tempControlId != "")
							{
								var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
								if (obj != null)
								{
									columnContent = obj.ReplacedText.ToUpper();
								}
							}
						}
						else
						{
							columnContent = dr["FindingDetail"].ToString();
						}
						pastmedicalHistory.Symptom = dr["Symptom"].ToString();
						pastmedicalHistory.Finding = columnContent;
					}

					// History of Present Illness
					else if (dr["Symptom"].Equals("/LOSS OR CHANGE OF VISION")
						  || dr["Symptom"].Equals("/PAIN OR IRRITATION")
						  || dr["Symptom"].Equals("/DISCOLORATION OF EITHER EYE")
						  || dr["Symptom"].Equals("/INJURY TO AFFECTED EYE")
						  || dr["Symptom"].Equals("/PROBLEMS AROUND EYE OR EYELID")
						  || dr["Symptom"].Equals("/CHEST PAIN,  COLD SORES,  DRY MOUTH, SHORTNESS OF BREATH,  WEIGHT CHANGE"))
					{
						if (pastmedicalHistory != null && !string.IsNullOrEmpty(pastmedicalHistory.Symptom) && !string.IsNullOrEmpty(pastmedicalHistory.Finding))
						{
							pastMedicalHistoryList.Add(pastmedicalHistory);
							pastmedicalHistory = null;
							searchingIndex = 0;
						}

						if (appointmentHPI != null && !string.IsNullOrEmpty(appointmentHPI.Symptom) && !string.IsNullOrEmpty(appointmentHPI.Finding))
						{
							appointmentHPIList.Add(appointmentHPI);
							appointmentHPI = null;
							searchingIndex = 0;
						}

						if (ocularHistory != null && !string.IsNullOrEmpty(ocularHistory.Symptom) && !string.IsNullOrEmpty(ocularHistory.Finding))
						{
							ocularHistoryList.Add(ocularHistory);
							ocularHistory = null;
							searchingIndex = 0;
						}

						searchingIndex = 2;
						appointmentHPI = new AppointmentHPI();
						if (appointmentHPI != null && dr["FindingDetail"].ToString().Contains("="))
						{	
							index = dr["FindingDetail"].ToString().IndexOf("=");
							columnContent = dr["FindingDetail"].ToString().Substring(0, index).Replace("*", "");
							string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
							if (tempControlId != "")
							{
								var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
								if (obj != null)
								{
									columnContent = obj.ReplacedText.ToUpper();
								}
							}
						}
						else
						{
							columnContent = dr["FindingDetail"].ToString();
						}
						appointmentHPI.Symptom = dr["Symptom"].ToString();
						appointmentHPI.Finding = columnContent;
					}

					// Ocular History
					else if (dr["Symptom"].Equals("/DO YOU WEAR GLASSES")
						  || dr["Symptom"].Equals("/DO YOU WEAR CONTACTS")
						  || dr["Symptom"].Equals("/CATARACT,  GLAUCOMA,  DIABETIC EYE DISEASE,  ARMD,  AMBLYOPIA,  CLOT,  EYE INJURY")
						  || dr["Symptom"].Equals("/UVEITIS,  MUSCLE IMBALANCE,  FACIAL PALSY,  MALIG-GROWTH,  NON-MALIG-GROWTH,  PTOSIS,  EYE CANCER,  FACIAL PAIN,  CONJUN")
						  || dr["Symptom"].Equals("/NIGHT BLINDNESS,  BLINDNESS,  COLOR BLINDNESS,  EYE SOCKET PROBLEMS,  INFLAMMATION ,  OPTIC NERVE BLOOD VESSEL DISEASE")
						  || dr["Symptom"].Equals("/LASIK OR PRK SURGERY,  FOLLOW-UP SURGERY AFTER LASIK,  COSMETIC FACIAL SURGERY,  CORNEAL TRANSPLANT SURGERY"))
					{
						if (pastmedicalHistory != null && !string.IsNullOrEmpty(pastmedicalHistory.Symptom) && !string.IsNullOrEmpty(pastmedicalHistory.Finding))
						{
							pastMedicalHistoryList.Add(pastmedicalHistory);
							pastmedicalHistory = null;
							searchingIndex = 0;
						}

						if (appointmentHPI != null && !string.IsNullOrEmpty(appointmentHPI.Symptom) && !string.IsNullOrEmpty(appointmentHPI.Finding))
						{
							appointmentHPIList.Add(appointmentHPI);
							appointmentHPI = null;
							searchingIndex = 0;
						}

						if (ocularHistory != null && !string.IsNullOrEmpty(ocularHistory.Symptom) && !string.IsNullOrEmpty(ocularHistory.Finding))
						{
							ocularHistoryList.Add(ocularHistory);
							ocularHistory = null;
							searchingIndex = 0;
						}

						searchingIndex = 3;
						ocularHistory = new OcularHistory();
						if (ocularHistory != null && dr["FindingDetail"].ToString().Contains("="))
						{
							index = dr["FindingDetail"].ToString().IndexOf("=");
							columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
							if (tempControlId != "")
							{
								var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
								if (obj != null)
								{
									columnContent = obj.ReplacedText.ToUpper();
								}
							}
						}
						else
						{
							columnContent = dr["FindingDetail"].ToString();
						}
						ocularHistory.Symptom = dr["Symptom"].ToString();
						ocularHistory.Finding = columnContent;
					}

					else if (searchingIndex == 1)
					{
						#region PastMedicalHistory
						if (dr["Symptom"].Equals("?AFFECT SIGHT"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								pastmedicalHistory._AffectSight = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								pastmedicalHistory._AffectSight = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								pastmedicalHistory._WhatType = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								pastmedicalHistory._WhatType = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE PAGE ONE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								pastmedicalHistory._WhatTypePageOne = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								pastmedicalHistory._WhatTypePageOne = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?TYPE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								pastmedicalHistory._Type = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								pastmedicalHistory._Type = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHEN DIAGNOSED"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								pastmedicalHistory._WhenDiagnosed = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								pastmedicalHistory._WhenDiagnosed = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?HAVE SURGERY") || dr["Symptom"].Equals("]HAVE SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								pastmedicalHistory._HaveSurgery = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								pastmedicalHistory._HaveSurgery = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("]WHEN SURGERY") || dr["Symptom"].Equals("?WHEN SURGERY") || dr["Symptom"].Equals("]WHEN TRANSPLANT SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								pastmedicalHistory._WhenSurgery = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								pastmedicalHistory._WhenSurgery = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("]COMPLICATIONS SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								pastmedicalHistory._ComplicationSurgery = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								pastmedicalHistory._ComplicationSurgery = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?COMPLICATIONS-DISEASE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								pastmedicalHistory._ComplicationDisease = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								pastmedicalHistory._ComplicationDisease = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?CONTROLLED"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								pastmedicalHistory._Controlled = dr["ImageDescriptor"].ToString().Trim();
							else
								pastmedicalHistory._Controlled = columnContent;
						}
						else if (dr["Symptom"].Equals("?WHEN BEGUN"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								pastmedicalHistory._WhenBegun = dr["ImageDescriptor"].ToString().Trim();
							else
								pastmedicalHistory._WhenBegun = columnContent;
						}
						else if (dr["Symptom"].Equals("?"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									var obj = secondFormControlModelList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										columnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								pastmedicalHistory._QuestionMark = dr["ImageDescriptor"].ToString().Trim();
							else
								pastmedicalHistory._QuestionMark = columnContent;
						}

						#endregion
					}
					else if (searchingIndex == 2)
					{
						#region History Of Present Illness
						if (dr["Symptom"].Equals("?CONDITION"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index).Replace("*", "");
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString().Substring(0, index).Replace("*", "");
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								appointmentHPI._Condition = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								appointmentHPI._Condition = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?HOW LONG"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								appointmentHPI._HowLong = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								appointmentHPI._HowLong = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?PAIN TYPE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								appointmentHPI._PainType = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								appointmentHPI._PainType = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								appointmentHPI._WhatType = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								appointmentHPI._WhatType = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHICH EYE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								appointmentHPI._WhichEye = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								appointmentHPI._WhichEye = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("]WHICH EYE  FLASHES"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								appointmentHPI._WhichEyeFlashes = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								appointmentHPI._WhichEyeFlashes = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("]WHICH EYE BLURRING"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								appointmentHPI._WhichEyeBurning = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								appointmentHPI._WhichEyeBurning = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHICH SIDE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								appointmentHPI._WhichSide = dr["ImageDescriptor"].ToString().Trim();
							else
								appointmentHPI._WhichSide = columnContent;
						}
						else if (dr["Symptom"].Equals("?WEIGHT LOSS GAIN"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								appointmentHPI._WeightLossGain = dr["ImageDescriptor"].ToString().Trim();
							else
								appointmentHPI._WeightLossGain = columnContent;
						}

						#endregion
					}
					else if (searchingIndex == 3)
					{
						#region Ocular History 
						if (dr["Symptom"].Equals("?HAVE CONJUNCTIVITIS-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HAVE_CONJUNCTIVITIS_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HAVE_CONJUNCTIVITIS_DID_WEAR = columnContent;
						}
						else if (dr["Symptom"].Equals("?HAVE ULCERS-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HAVE_ULCERS_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HAVE_ULCERS_DID_WEAR = columnContent;
						}
						else if (dr["Symptom"].Equals("?HOW LONG"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HOW_LONG = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HOW_LONG = columnContent;
						}
						else if (dr["Symptom"].Equals("?HOW OFTEN WEAR-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HOW_OFTEN_WEAR_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HOW_OFTEN_WEAR_DID_WEAR = columnContent;
						}
						else if (dr["Symptom"].Equals("?MEDICAL LENSES-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._MEDICAL_LENSES_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._MEDICAL_LENSES_DID_WEAR = columnContent;
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHAT_TYPE_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHAT_TYPE_DID_WEAR = columnContent;
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE-DO-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								ocularHistory._WHAT_TYPE_DO_WEAR = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								ocularHistory._WHAT_TYPE_DO_WEAR = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHEN"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								ocularHistory._WHEN = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								ocularHistory._WHEN = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHEN STOP"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								ocularHistory._WHEN_STOP = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								ocularHistory._WHEN_STOP = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHICH EYE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
							{
								ocularHistory._WHICH_EYE_TYPE = dr["ImageDescriptor"].ToString().Trim();
							}
							else
							{
								ocularHistory._WHICH_EYE_TYPE = columnContent;
							}
						}
						else if (dr["Symptom"].Equals("?WHICH KIND"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHICH_KIND = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHICH_KIND = columnContent;
						}
						else if (dr["Symptom"].Equals("?WHICH SIDE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHICH_SIDE = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHICH_SIDE = columnContent;
						}
						else if (dr["Symptom"].Equals("?WHY STOP"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHY_STOP = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHY_STOP = columnContent;
						}
						else if (dr["Symptom"].Equals("[WHEN SURGERY LEFT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_SURGERY_LEFT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_SURGERY_LEFT_BOTH = columnContent;
						}
						else if (dr["Symptom"].Equals("[WHEN SURGERY LEFT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_SURGERY_LEFT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_SURGERY_LEFT_ONLY = columnContent;
						}
						else if (dr["Symptom"].Equals("[WHEN SURGERY RIGHT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_SURGERY_RIGHT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_SURGERY_RIGHT_BOTH = columnContent;
						}
						else if (dr["Symptom"].Equals("[WHEN SURGERY RIGHT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_SURGERY_RIGHT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_SURGERY_RIGHT_ONLY = columnContent;
						}
						else if (dr["Symptom"].Equals("]COMPLICATIONS SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._COMPLICATIONS_SURGERY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._COMPLICATIONS_SURGERY = columnContent;
						}
						else if (dr["Symptom"].Equals("[HAVE SURGERY LEFT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HAVE_SURGERY_LEFT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HAVE_SURGERY_LEFT_BOTH = columnContent;
						}
						else if (dr["Symptom"].Equals("[HAVE SURGERY LEFT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HAVE_SURGERY_LEFT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HAVE_SURGERY_LEFT_ONLY = columnContent;
						}
						else if (dr["Symptom"].Equals("[HAVE SURGERY RIGHT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HAVE_SURGERY_RIGHT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HAVE_SURGERY_RIGHT_BOTH = columnContent;
						}
						else if (dr["Symptom"].Equals("[HAVE SURGERY RIGHT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HAVE_SURGERY_RIGHT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HAVE_SURGERY_RIGHT_ONLY = columnContent;
						}
						else if (dr["Symptom"].Equals("]HOW MANY TIMES SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HOW_MANY_TIMES_SURGERY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HOW_MANY_TIMES_SURGERY = columnContent;
						}
						else if (dr["Symptom"].Equals("]HOW OFTEN BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HOW_OFTEN_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HOW_OFTEN_BOTH = columnContent;
						}
						else if (dr["Symptom"].Equals("]HOW OFTEN LEFT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HOW_OFTEN_LEFT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HOW_OFTEN_LEFT_BOTH = columnContent;
						}
						else if (dr["Symptom"].Equals("]HOW OFTEN RIGHT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HOW_OFTEN_RIGHT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HOW_OFTEN_RIGHT_BOTH = columnContent;
						}
						else if (dr["Symptom"].Equals("]HOW OFTEN RIGHT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._HOW_OFTEN_RIGHT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._HOW_OFTEN_RIGHT_ONLY = columnContent;
						}
						else if (dr["Symptom"].Equals("]PATCH RIGHT"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._PATCH_RIGHT = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._PATCH_RIGHT = columnContent;
						}
						else if (dr["Symptom"].Equals("]TYPE EYE CANCER RIGHT"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._TYPE_EYE_CANCER_RIGHT = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._TYPE_EYE_CANCER_RIGHT = columnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN DIAGNOSED LEFT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_DIAGNOSED_LEFT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_DIAGNOSED_LEFT_BOTH = columnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN DIAGNOSED LEFT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_DIAGNOSED_LEFT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_DIAGNOSED_LEFT_ONLY = columnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN DIAGNOSED RIGHT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_DIAGNOSED_RIGHT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_DIAGNOSED_RIGHT_BOTH = columnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN DIAGNOSED RIGHT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_DIAGNOSED_RIGHT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_DIAGNOSED_RIGHT_ONLY = columnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN LAST  SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_LAST__SURGERY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_LAST__SURGERY = columnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHEN_SURGERY = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHEN_SURGERY = columnContent;
						}
						else if (dr["Symptom"].Equals("]WHICH EYE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								index = dr["FindingDetail"].ToString().IndexOf("=");
								columnContent = dr["FindingDetail"].ToString().Substring(0, index);
							}
							else
							{
								columnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								ocularHistory._WHICH_EYE = dr["ImageDescriptor"].ToString().Trim();
							else
								ocularHistory._WHICH_EYE = columnContent;
						}

						#endregion
					}
				}

				if (pastmedicalHistory != null && !string.IsNullOrEmpty(pastmedicalHistory.Symptom) && !string.IsNullOrEmpty(pastmedicalHistory.Finding))
				{
					pastMedicalHistoryList.Add(pastmedicalHistory);
					pastmedicalHistory = null;
				}

				if (appointmentHPI != null && !string.IsNullOrEmpty(appointmentHPI.Symptom) && !string.IsNullOrEmpty(appointmentHPI.Finding))
				{
					appointmentHPIList.Add(appointmentHPI);
					appointmentHPI = null;
				}

				if (ocularHistory != null && !string.IsNullOrEmpty(ocularHistory.Symptom) && !string.IsNullOrEmpty(ocularHistory.Finding))
				{
					ocularHistoryList.Add(ocularHistory);
					ocularHistory = null;
				}
			}

			pastMedicalHistoryList.ForEach(item => item.Add(_appointmentId));
			ocularHistoryList.ForEach(item => item.Add(_appointmentId));
			appointmentHPIList.ForEach(item => item.Add(_appointmentId));
		}

		public static void ExtractCheifComplaint(string _appointmentId)
		{
			try
			{
				string _insertQry = string.Empty;
				string _name = string.Empty;
				string _value = string.Empty;

				DataTable dtFindingDetail = new DataTable();
				string _selectQry = "Select * From dbo.PatientClinical Where ClinicalType = 'H' and Symptom like '%CHIEFCOMPLAINTS%' AND AppointmentId = '" + _appointmentId + "'";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					using (SqlCommand cmd = new SqlCommand(_selectQry, con))
					{
						cmd.CommandType = CommandType.Text;
						using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
						{
							ds.Fill(dtFindingDetail);
						}
					}
				}

				foreach (DataRow dr in dtFindingDetail.Rows)
				{
					if (dr["Symptom"].ToString().Contains("/CHIEFCOMPLAINTS"))
					{
						_name = dr["Symptom"].ToString();
						_value = dr["FindingDetail"].ToString();
						_insertQry = "Insert into ExtractedCheifComplaintDetails(AppointmentId, Symptom, FindingDetail) Values ('" + _appointmentId + "', '" + _name.Replace("'", "''") + "', '" + _value.Replace("'", "''") + "')";
						using (SqlConnection con = new SqlConnection(Util._connectionString))
						{
							con.Open();
							using (SqlCommand cmd = new SqlCommand(_insertQry, con))
							{
								cmd.ExecuteNonQuery();
							}
							con.Close();
						}
					}
				}
			}
			catch
			{

			}
		}

		public static void ExtractFamilyHistory(string _appointmentId)
		{
			DataTable dtFindingDetail = new DataTable();
			string _selectQry = "Select * From dbo.PatientClinical Where ClinicalType = 'H' and Symptom like '%Family%' AND AppointmentId = '" + _appointmentId + "'";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(_selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(dtFindingDetail);
					}
				}
			}

			string Relationship2Patient = "";
			string DiseaseCaused = "";
			string _insertQry = "";

			foreach (DataRow dr in dtFindingDetail.Rows)
			{
				if (dr["Symptom"].ToString().Contains("?WHICH FAMILY"))
				{
					DiseaseCaused = dr["FindingDetail"].ToString();
					if (DiseaseCaused.Contains("-"))
					{
						Relationship2Patient = DiseaseCaused.Split('-')[0];
						DiseaseCaused = DiseaseCaused.Split('-')[1].Split('=')[0];
					}
					_insertQry = "Insert into dbo.ExtractedFamilyHistoryDetails(AppointmentId, Relationship2Patient, DiseaseCaused) Values ('" + _appointmentId + "', '" + Relationship2Patient + "', '" + DiseaseCaused + "')";
					using (SqlConnection con = new SqlConnection(Util._connectionString))
					{
						con.Open();
						using (SqlCommand cmd = new SqlCommand(_insertQry, con))
						{
							cmd.ExecuteNonQuery();
						}
						con.Close();
					}
				}
			}
		}
	}
}
