If Not Exists (Select * from sys.tables Where name  = 'AppointmentExtractionStatus')
Begin
CREATE TABLE [dbo].[AppointmentExtractionStatus](
	[AppointmentId] [numeric](18, 0) NOT NULL,
	[Status] [bit] NOT NULL,
	[ExceptionMessage] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AppointmentExtractionStatus] PRIMARY KEY CLUSTERED 
(
	[AppointmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
End



