﻿using System;

namespace ClinicalDataExtraction.Procedures
{
	public class ProcedureModel
	{
		public string ProcedureName { get; set; }

		public string ProcedureType { get; set; }

		public string EyeType { get; set; }

		public string OD_Comments { get; set; }

		public string OS_Comments { get; set; }
	}
}
