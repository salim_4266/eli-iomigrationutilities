﻿using ClinicalDataExtraction.Procedures;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
	public class Procedure
	{
		private static string AppointmentId = string.Empty;
		
		public static void Extract(string appointmentId)
		{
			AppointmentId = appointmentId;
			var elements = new List<string>
			{
				"/AC PARACENT W DX ASP"
				, "/AC PARACENT W INJECTION"
				, "/AC PARACENT W REM OF BLOOD"
				, "/AC PARACENT W THERAP"
				, "/AC RESTORATION"
				, "/ADJACENT TISSUE TRANSFER"
				, "/ALCOHOL INJECTION, RETROBULBAR"
				, "/AMNIOTIC MEMBRANE TRANSPLANT"
				, "/AMNIOTIC MEMBRANE, PLACEMENT"
				, "/AMNIOTIC MEMBRANE, SUTURED"
				, "/ANTERIOR MICROPUNCTURE"
				, "/ASTIGMATIC KERATOTOMY (LRI)"
				, "/BANDAGE CONTACT LENS"
				, "/BIOPSY, EXCISION, CONJUNCTIVA"
				, "/BLEPHAROPLASTY"
				, "/BOTOX, COSMETIC"
				, "/BOTOX, MEDICAL"
				, "/BOTOX, STRABISMUS"
				, "/CANALOPLASTY"
				, "/CANTHOPLASTY"
				, "/CANTHOTOMY"
				, "/CAPSULOTOMY"
				, "/CATARACT SURGERY"
				, "/CATARACT SURGERY, COMPLEX"
				, "/CB DESTRUC, CYCLOPHOTO"
				, "/CB DESTRUC, CYCLOPHOTO, ENDO"
				, "/CHALAZION EXCISION"
				, "/CLOSURE OF PUNCTUM, PERMANENT"
				, "/CLOSURE OF PUNCTUM, PLUGS"
				, "/CLOSURE OF WOUND, ADNEXA"
				, "/COMPLEX REPAIR, LIDS"
				, "/CONDUCTIVE KERATOPLASTY"
				, "/CONJUNCTIVAL FB REMOVAL"
				, "/CONJUNCTIVAL LITHIASIS REMOVAL"
				, "/CONJUNCTIVOPLASTY"
				, "/CORNEAL ABRASION"
				, "/CORNEAL EPITHELIAL REMOVAL"
				, "/CORNEAL FB REMOVAL"
				, "/CORNEAL TRANSPLANT"
				, "/CORNEAL WEDGE RESECTION"
				, "/CRYO, DESTR OF RET LESION"
				, "/CRYOPEXY PROPHYLAXIS"
				, "/CRYOPEXY RETINOPATHY"
				, "/CRYOTHERAPY LOC RD"
				, "/DACRYOCYSTORHINOSTOMY"
				, "/DEEP SCLERECTOMY"
				, "/DERMAL FILLERS, COSMETIC"
				, "/DERMAL FILLERS, JUVEDERM"
				, "/DERMAL FILLERS, RESTYLANE"
				, "/DERMAL FILLERS, SCULPTRA"
				, "/DESTRUC LOC LES RADIATION"
				, "/DESTRUCTION OF LESION, CONJ"
				, "/DESTRUCTION OF LESION, CORNEA"
				, "/DESTRUCTION, PROLIFERATIVE LESIONS"
				, "/DIALYSIS SERVICES"
				, "/DISCISSION OF PCO (66820)"
				, "/DIVISION OF SYMBLEPHARON"
				, "/ECTROPION REPAIR"
				, "/ENTROPION REPAIR"
				, "/ENUCLEATION WITH IMPLANT"
				, "/EOM SURG, HORIZONTAL"
				, "/EOM SURG, VERTICAL"
				, "/EPILATION"
				, "/EPILATION, OTHER THAN FORCEPS"
				, "/EVISCERATION OF OCULAR CONTENTS"
				, "/EX PRESS SHUNT"
				, "/EXCHANGE IOL"
				, "/EXCISION AND REPAIR OF LID"
				, "/EXCISION OF LESION, ADNEXA"
				, "/EXCISION OF LESION, BENIGN"
				, "/EXCISION OF LESION, MALIGNANT"
				, "/EXCISION, BENIGN"
				, "/EXCISION, MALIGNANT"
				, "/FLUID GAS EXCHANGE"
				, "/FRONTALIS SUSPENSION"
				, "/GOLD WEIGHT IMPLANT"
				, "/HARVEST OF SKIN GRAFT"
				, "/ICL SURGERY"
				, "/IMPLANT GANCICLOVIR"
				, "/IMPLANT RETISERT"
				, "/IMPLANTABLE MINI TELESCOPE"
				, "/INC/EXC BIOPSY OF SKIN"
				, "/INC/EXC BIOPSY OF SKIN, MULTI"
				, "/INCISE AND DRAIN ABCESS"
				, "/INCISE AND DRAIN CONJ CYST"
				, "/INCISE AND DRAIN, LID"
				, "/INJECTION OF FILTERING BLEB"
				, "/INJECTION, INTRALESIONAL"
				, "/INJECTION, PERIBULBAR"
				, "/INJECTION, RETROBULBAR"
				, "/INJECTION, SUB TENONS"
				, "/INJECTION, SUBCONJ"
				, "/INJECTION, SUBCUTANEOUS"
				, "/INTERMEDIATE REPAIR, FACE"
				, "/INTERMEDIATE REPAIR, NECK"
				, "/INTERMEDIATE REPAIR, SCALP"
				, "/INTRALASE"
				, "/INTRAVIT INJEC, AMIKACIN"
				, "/INTRAVIT INJEC, AVASTIN"
				, "/INTRAVIT INJEC, CEFTAZIDIME"
				, "/INTRAVIT INJEC, DEXAMETHASONE"
				, "/INTRAVIT INJEC, EYLEA"
				, "/INTRAVIT INJEC, FOSCARNET"
				, "/INTRAVIT INJEC, JETREA"
				, "/INTRAVIT INJEC, LUCENTIS 0.3MG"
				, "/INTRAVIT INJEC, LUCENTIS"
				, "/INTRAVIT INJEC, MACUGEN"
				, "/INTRAVIT INJEC, MITOMYCIN"
				, "/INTRAVIT INJEC, OZURDEX"
				, "/INTRAVIT INJEC, TRIAMCINOLONE"
				, "/INTRAVIT INJEC, TRIAMCINOLONE, PF"
				, "/INTRAVIT INJEC, VANCOMYCIN"
				, "/INTRAVIT INJECT, TRIESENCE"
				, "/IRIDECTOMY, PERIPHERAL"
				, "/ISTENT INSERTION"
				, "/JUVEDERM PLAIN"
				, "/JUVEDERM ULTRA PLUS"
				, "/JUVEDERM"
				, "/LACRIMAL PROBE AND IRRIGATION"
				, "/LACRIMAL PROBE, ANESTHESIA"
				, "/LACRIMAL PROBE, CANALICULA"
				, "/LAMELLAR KERATECTOMY"
				, "/LASER, EYELID LESION (67850)"
				, "/LASER, FOCAL, CNVM (67220)"
				, "/LASER, IRIDOPLASTY (66762)"
				, "/LASER, IRIDOTOMY (66761)"
				, "/LASER, IRIDOTOMY, ARGON (66761)"
				, "/LASER, MACULOP, (FOCAL 67210)"
				, "/LASER, PDT (67221)"
				, "/LASER, PDT, 2ND EYE (67225)"
				, "/LASER, PROPHY RD (67145)"
				, "/LASER, PRP (67228)"
				, "/LASER, REPAIR LOCAL RD (67105)"
				, "/LASER, ROP (67229)"
				, "/LASER, SEVER ADHESIONS (65860)"
				, "/LASER, SKIN LESION (17000)"
				, "/LASER, SUTURE LYSIS (66250)"
				, "/LASER, TRABECULOPLASTY (65855)"
				, "/LASER, VIT STRANDS (67031)"
				, "/LASER, YAG, CAPSULOTOMY (66821)"
				, "/LASER, YAG, GONIOPUNCTURE"
				, "/LASER, YAG, IRIDOTOMY (66761)"
				, "/LASIK ENHANCEMENT"
				, "/LASIK SURGERY"
				, "/LENSECTOMY"
				, "/LID BIOPSY"
				, "/LID LESION EXCISION"
				, "/LIPIFLOW"
				, "/LRI"
				, "/LYSIS PAS"
				, "/LYSIS POST SYN"
				, "/MASS LESION EXCISION"
				, "/MASS LESION EXCISION, FACE"
				, "/MCCANNEL SUTURE"
				, "/NASAL ENDOSCOPY"
				, "/OPTIC NERVE DECOMPRESSION"
				, "/ORBITAL FLOOR BLOWOUT TREATMENT"
				, "/ORBITAL FLOOR BLOWOUT TREATMENT, IMPLANT"
				, "/ORBITAL IMPLANT REVISION"
				, "/ORBITOTOMY"
				, "/PHACOEMULSIFICATION"
				, "/PLASTIC REPAIR OF CANANLICULI"
				, "/PNEUMATIC RETINOPEXY"
				, "/PRK ENHANCEMENT"
				, "/PRK"
				, "/PROLONGED OV (99354)"
				, "/PTERYGIUM EXCISION W GRAFT"
				, "/PTERYGIUM EXCISION"
				, "/PTK"
				, "/PTOSIS REPAIR"
				, "/PTOSIS REPAIR, FASANELLA-SERVAT"
				, "/PUNCTOPLASTY, BIOPSY, EXC, LAC"
				, "/PUNCTUM PROBE AND IRRIGATION"
				, "/RADIAL KERATOTOMY"
				, "/RECONSTRUCTION, LID"
				, "/REFRACTIVE LENS EXCHANGE"
				, "/RELEASE VIT OR CHOROIDAL FLUID"
				, "/REMOVAL AC IOL"
				, "/REMOVAL FB, LACRIMAL"
				, "/REMOVAL FB, LIDS"
				, "/REMOVAL IOFB, MAG"
				, "/REMOVAL IOFB, NON MAG"
				, "/REMOVE IMPL MATERIAL, IO"
				, "/REMOVE LENS MATERIAL, ASPIRATION"
				, "/REMOVE LENS MATERIAL, INTRACAP"
				, "/REMOVE SKIN TAG"
				, "/REPAIR CONJ LACERATION"
				, "/REPAIR CORN, SCLER LAC W PLAPSE"
				, "/REPAIR CORN, SCLERAL LAC"
				, "/REPAIR OF IRIS, CB"
				, "/REPAIR, SIMPLE WOUND"
				, "/REPOSITION IOL"
				, "/RESTYLANE .4 ML"
				, "/RESTYLANE 1 ML"
				, "/RETROBULB INJEC MEDICATION"
				, "/REVIS OP WOUND"
				, "/REVISION OF AQUEOUS SHUNT"
				, "/REVISION OF OCULAR IMPLANT"
				, "/SCLERAL BUCKLE"
				, "/SCLERAL BUCKLE, RELEASE (67115)"
				, "/SCLERAL BUCKLE, REMOVAL"
				, "/SCLERAL BUCKLE, REVISION"
				, "/SCLERAL PATCH GRAFT"
				, "/SECONDARY IOL"
				, "/SEVERING TARSORRAPHY"
				, "/SHAVE SMALL LESION, ADNEXA"
				, "/SKIN GRAFT TO EYELID OR FACE"
				, "/STUDY, ALIMERA, INTRAVIT FLUO"
				, "/STUDY, ALLERGAN, INTRAVIT  DEX"
				, "/STUDY, BRAVO, INJ"
				, "/STUDY, BRIMO, INTRAVIT INSERT"
				, "/STUDY, CRUISE, INJ"
				, "/STUDY, DIAMOND, INTRAVIT INJ"
				, "/STUDY, EMERALD, INTRAVIT INJ"
				, "/STUDY, HARBOR, INTRAVIT INJECT"
				, "/STUDY, HOFFMAN ASTIG"
				, "/STUDY, HORIZON, INTRAVIT INJ"
				, "/STUDY, MIVI TRUST, INTRAVIT INJ"
				, "/STUDY, OASIS, INTRAVIT INJ"
				, "/STUDY, PDR IST, INTRAVIT INJ"
				, "/STUDY, RISE, INJ"
				, "/STUDY, SIRIUS"
				, "/SUPERFICIAL KERATECTOMY"
				, "/SUTURE OF RECENT WOUND, EYELID"
				, "/SUTURE REMOVAL, CONJ"
				, "/SUTURE REMOVAL, CORNEA"
				, "/SUTURE REMOVAL, LATINA"
				, "/TARSORRHAPHY"
				, "/TARSORRHAPHY, TEMP, SUTURE"
				, "/TEMPORAL ARTERIAL BIOPSY"
				, "/TRABECULECTOMY"
				, "/TRICHIASIS REPAIR, SPLIT LID"
				, "/VALVE, MOLTENO, BAERVELDT"
				, "/VISUDYNE WORKUP"
				, "/VITRECT, (67036)"
				, "/VITRECT, ANTERIOR (67010)"
				, "/VITRECT, ANTERIOR PARTIAL"
				, "/VITRECT, ANTERIOR SUBTOTAL"
				, "/VITRECT, COMPLEX RD (67113)"
				, "/VITRECT, ILM PEEL (67042)"
				, "/VITRECT, PRERET MEM PEEL (67041)"
				, "/VITRECT, REPAIR RD (67108)"
				, "/VITRECT, SRM PEEL (67043)"
				, "/VITRECT, W FOCAL LASER (67039)"
				, "/VITRECT, W PRP (67040)"
				, "/VITREOUS LAVAGE"
				, "/XEOMIN, COSMETIC"
				, "/XEOMIN, MEDICAL"
				, "/ZIEGLER CAUTERY"
			};
			elements.ForEach(symptom => ExtractViaSymptom(symptom));
		}

		private static void ExtractViaSymptom(string symptomType)
		{
			var model = new ProcedureModel();
			bool leftEye = false;
			bool rightEye = false;
			int startIndex = 0;
			int endIndex = 0;
			string tempControlId = "";
			var dtElement = new DataTable();
			model.ProcedureName = symptomType.Replace("/", "");
			string _selectQry = "Select FindingDetail From dbo.PatientClinical with (nolock) Where Symptom = '" + symptomType + "' and AppointmentId = '" + AppointmentId + "' and Status = 'A' and ClinicalType = 'F' Order by ClinicalId";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(_selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(dtElement);
					}
				}
			}
			if (dtElement.Rows.Count > 0)
			{
				foreach (DataRow dr in dtElement.Rows)
				{
					if ((dr["FindingDetail"].ToString().Contains("*OD-") || leftEye && !rightEye) && !dr["FindingDetail"].ToString().Contains("*OS-"))
					{
						if (!leftEye)
						{
							leftEye = true;
						}
						else
						{
							startIndex = dr["FindingDetail"].ToString().IndexOf('=');
							endIndex = dr["FindingDetail"].ToString().IndexOf('<');
							tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
							FormControls objForm1 = new FormControls(tempControlId);
							if (objForm1.ControlType == "N")
							{
								startIndex = dr["FindingDetail"].ToString().IndexOf('<');
								endIndex = dr["FindingDetail"].ToString().IndexOf('>');
								model.OD_Comments += " " + objForm1.ControlText.Replace("'", "''") + " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''");
							}
						}
					}
					else if (dr["FindingDetail"].ToString().Contains("*OS-") || rightEye)
					{
						if (!rightEye)
						{
							rightEye = true;
						}
						else
						{
							startIndex = dr["FindingDetail"].ToString().IndexOf('=');
							endIndex = dr["FindingDetail"].ToString().IndexOf('<');
							tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
							FormControls objForm1 = new FormControls(tempControlId);
							if (objForm1.ControlType == "N")
							{
								startIndex = dr["FindingDetail"].ToString().IndexOf('<');
								endIndex = dr["FindingDetail"].ToString().IndexOf('>');
								model.OS_Comments += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''");
							}
						}
					}
				}

				model.EyeType = leftEye && rightEye ? "OU" : leftEye && !rightEye ? "OD" : !leftEye && rightEye ? "OS" : "";
				model.ProcedureType = leftEye || rightEye ? "Ocular" : "Non Ocular";

				Save(model);
			}
		}

		private static void Save(ProcedureModel model)
		{
			string insertQry = "Insert into dbo.ExtractedProcedures(AppointmentId, ProcedureName, ProcedureType, EyeSite, OD_Comments, OS_Comments) Values ('" + AppointmentId + "', '" + model.ProcedureName + "', '" + model.ProcedureType + "', '" + model.EyeType + "', '" + model.OD_Comments + "', '" + model.OS_Comments + "')";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				con.Open();
				using (SqlCommand cmd = new SqlCommand(insertQry, con))
				{
					cmd.CommandType = CommandType.Text;
					cmd.ExecuteNonQuery();
				}
			}
		}
	}
}
