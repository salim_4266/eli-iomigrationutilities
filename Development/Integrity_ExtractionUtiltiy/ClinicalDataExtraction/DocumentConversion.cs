﻿using System;
using System.Data.SqlClient;
using System.IO;

namespace ClinicalDataExtraction
{
	public sealed class DocumentConversion
	{
		private DocumentConversion()
		{

		}

		public static void Extract()
		{
			var serverDataPath = System.Configuration.ConfigurationManager.AppSettings["ServerDathPath"].ToString();
			DirectoryInfo myScanDirectory = new DirectoryInfo(serverDataPath);
			if (myScanDirectory.Exists)
			{
                TruncateTable();
				foreach (var subdirectory in myScanDirectory.GetDirectories())
				{
					var patientId = subdirectory.Name;
					foreach (var file in subdirectory.GetFiles())
					{
						var testType = string.Empty;
						var dateOfService = file.Name.Substring(file.Name.LastIndexOf('-') + 1, 8);
						var filePath = file.FullName;
						if (dateOfService.Length == 8)
						{
							dateOfService = dateOfService.Substring(4, 2) + "-" + dateOfService.Substring(6, 2) + "-" + dateOfService.Substring(0, 4);
						}
						if (file.Name.Contains("AScan"))
						{
							testType = "A/Scan";
						}
						else if (file.Name.Contains("Fundus"))
						{
							testType = "Fundus";
						}
						else if (file.Name.Contains("Topography"))
						{
							testType = "Topography";
						}
						else if (file.Name.Contains("IOLMASTER"))
						{
							testType = "IOLMaster";
						}
						else if (file.Name.Contains("OCT"))
						{
							testType = "OCT";
						}
						else if (file.Name.Contains("PACHMETRY"))
						{
							testType = "Pachy";
						}
						else
						{
							testType = "Other";
						}
						Insert(patientId, dateOfService, testType, filePath);
					}
				}
			}
		}

		private static void Insert(string patientId, string dateOfService, string testName, string testPath)
		{
			string insertQry = "Insert into ExtractedScanAndTestDetails(PatientId, DateOfService, TestName, TestPath) Values ('" + patientId + "', '" + dateOfService + "', '" + testName + "', '" + testPath + "')";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand(insertQry, con);
				cmd.ExecuteNonQuery();
				con.Close();
			}
		}

        private static void TruncateTable()
        {
            string qryString = "Truncate tbale ExtractedScanAndTestDetails";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(qryString, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }


    }
}
