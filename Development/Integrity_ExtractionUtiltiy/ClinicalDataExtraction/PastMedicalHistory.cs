﻿using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
    public class PastMedicalHistory
    {	
		public string Symptom { get; set; } = "";
		public string Finding { get; set; } = "";
		public string _AffectSight { get; set; } = "";
		public string _WhenDiagnosed { get; set; } = "";
		public string _HaveSurgery { get; set; } = "";
		public string _ComplicationSurgery { get; set; } = "";
		public string _Controlled { get; set; } = "";
		public string _ComplicationDisease { get; set; } = "";
		public string _WhatType { get; set; } = "";
		public string _WhatTypePageOne { get; set; } = "";
		public string _WhenSurgery { get; set; } = "";
		public string _Type { get; set; } = "";
		public string _WhenBegun { get; set; } = "";
		public string _QuestionMark { get; set; } = "";

		public void Add(string _appointmentId)
        {
            string _insertQry = "Insert into ExtractedPastMedicalHistoryDetails(AppointmentId, Symptom, ButtonText, Type, AffectSight, WhenDiagnosed, WhenSurgery, ComplicationSurgery, Controlled, ComplicationDisease, HaveSurgery, WhatTypePageOne, WhatType, WhenBegun, QuestionMark) Values ('" + _appointmentId + "', '"+ Symptom + "', '" + Finding.Replace("'", "''") + "', '" + _Type.Replace("'", "''") + "', '" + _AffectSight.Replace("'", "''") + "', '" + _WhenDiagnosed.Replace("'", "''") + "', '" + _WhenSurgery.Replace("'", "''") + "', '" + _ComplicationSurgery.Replace("'", "''") + "', '" + _Controlled.Replace("'", "''") + "','" + _ComplicationDisease.Replace("'", "''") + "', '" + _HaveSurgery.Replace("'", "''") + "', '" + _WhatTypePageOne.Replace("'", "''") + "', '" + _WhatType.Replace("'", "''") + "', '" + _WhenBegun.Replace("'", "''") + "','" + _QuestionMark.Replace("'", "''") + "')";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(_insertQry, con))
                {
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
        }
    }
}
