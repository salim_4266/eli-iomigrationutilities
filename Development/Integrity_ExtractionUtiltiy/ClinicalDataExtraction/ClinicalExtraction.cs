﻿using System;

namespace ClinicalDataExtraction
{
	public sealed class ClinicalExtraction
	{
		public static void GenerateElement(string appointmentId)
		{
			bool isExceptionRaised = false;

			#region All Remaining clinical type F Items
			try
			{
				ClinicalTypeF.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region ExtractedProcedures 
			try
			{
				Procedure.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Past medical history, History of present illness and Ocular history (Clinical Extraction - H)
			try
			{
				ClincialExtractionTypeH.Extarct_PMH_HPI_OCX(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Family History Details
			try
			{
				ClincialExtractionTypeH.ExtractFamilyHistory(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Social History - Work and School, Drink and Living Situation
			try
			{
				WorkandSchool.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			try
			{
				Drink.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			try
			{
				LivingSituation.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Cheif Complaints Details
			try
			{
				ClincialExtractionTypeH.ExtractCheifComplaint(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Contact Lens Information
			try
			{
				ContactLens.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region BrightnessAcuityTest
			try
			{
				BrightnessAcuityTest.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Glare
			try
			{
				Glare.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Color Plates details
			try
			{
				ColorPlates.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Color Screen details
			try
			{
				ColorScreen.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Height, Wieght and BMI
			try
			{
				HeightAndWeight.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Physical Exam
			try
			{
				PhysicalExam.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			#region Blood Pressure
			try
			{
				BloodPressure.Extract(appointmentId);
			}
			catch (Exception ee)
			{
				isExceptionRaised = true;
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
					Status = "0"
				}.Save();
			}
			#endregion

			if(!isExceptionRaised)
			{
				new AppointmentExtractionStatus()
				{
					AppointmentId = appointmentId,
					ExceptionMessage = "",
					Status = "1"
				}.Save();

			}
		}
	}
}
