﻿using System.Data;
using System.Data.SqlClient;


namespace ClinicalDataExtraction
{
	internal sealed class PhysicalExam
	{
		private PhysicalExam()
		{

		}

		private static string Value = string.Empty;

		public static void Extract(string appointmentId)
		{
			DataTable ElementList = new DataTable();
			string selectQry = "Select FindingDetail From dbo.PatientClinical Where AppointmentId = '" + appointmentId + "' and ClinicalType = 'F' and Symptom  = '/PHYSICAL EXAM'";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(ElementList);
					}
				}
			}

			if (ElementList.Rows.Count > 0)
			{
				foreach (DataRow dr in ElementList.Rows)
				{
					int startIndex = 0;
					int endIndex = 0;
					if (dr["FindingDetail"].ToString().Contains("TIME"))
					{

					}
					else
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						Value += dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1) + " ";
					}
				}
				Add(appointmentId);
			}
		}

		private static void Add(string appointmentId)
		{
			string insertQry = "Insert into ExtractedPhysicalExamDetails(AppointmentId, Value) Values ('" + appointmentId + "', '" + Value + "')";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				con.Open();
				using (SqlCommand cmd = new SqlCommand(insertQry, con))
				{
					cmd.CommandType = CommandType.Text;
					cmd.ExecuteNonQuery();
				}
				con.Close();
			}
		}
	}
}
