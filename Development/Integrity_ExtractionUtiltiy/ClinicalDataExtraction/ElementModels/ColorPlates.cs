﻿using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
    internal class ColorPlates
    {
        public static void Extract(string appointmentId)
        {
            try
            {
                string eyeType = string.Empty;
                string ODMethod = string.Empty;
                string OSMethod = string.Empty;
                string ODValue = string.Empty;
                string OSValue = string.Empty;
                string selectQry = "Select FindingDetail From dbo.PatientClinical Where AppointmentId = '" + appointmentId + "' and Symptom = '/COLOR PLATES'";
                var ElementList = new DataTable();
                using (SqlConnection con = new SqlConnection(Util._connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(selectQry, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                        {
                            ds.Fill(ElementList);
                        }
                    }
                }
                if (ElementList.Rows.Count > 0)
                {
                    int startIndex = 0;
                    int endIndex = 0;
                    foreach (DataRow dr in ElementList.Rows)
                    {
                        if (dr["FindingDetail"].ToString().Contains("OD-ICV"))
                        {
                            eyeType = "OD";
                        }
                        else if (dr["FindingDetail"].ToString().Contains("OS-ICV"))
                        {
                            eyeType = "OS";
                        }
                        else if (eyeType != "")
                        {
                            startIndex = dr["FindingDetail"].ToString().IndexOf('=');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('<');
                            string tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
                            FormControls objForm = new FormControls(tempControlId);
                            if (objForm.ControlType == "N")
                            {
                                startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                if (eyeType == "OD")
                                {
                                    ODValue += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                                else if (eyeType == "OS")
                                {
                                    OSValue += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                            }
                            else if (objForm.ControlType == "B")
                            {
                                startIndex = dr["FindingDetail"].ToString().IndexOf('*');
                                endIndex = dr["FindingDetail"].ToString().IndexOf('=');
                                if (eyeType == "OD")
                                {
                                    ODMethod += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                                else if (eyeType == "OS")
                                {
                                    OSMethod += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                            }
                        }
                    }
                    Add(appointmentId, ODValue, OSValue, ODMethod, OSMethod);
                }
            }
            catch { }
        }

        private static void Add(string appointmentId, string ODValue, string OSValue, string OD_Method, string OS_Method)
        {
            string insertQry = "Insert into ExtractedColorPlatesDetails(AppointmentId, Time, OD_Value, OS_Value, OD_Method, OS_Method) Values ('" + appointmentId + "', '', '" + ODValue.Replace("'", "''") + "', '" + OSValue.Replace("'", "''") + "', '" + OD_Method.Replace("'", "''") + "', '" + OS_Method.Replace("'", "''") + "')";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(insertQry, con))
                {
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
        }
    }
}

