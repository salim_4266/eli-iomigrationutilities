﻿using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
	public sealed class Drink
	{
		public static void Extract(string appointmentId)
		{
			string type = string.Empty;
			string value = string.Empty;
			string selectQry = "Select Symptom, FindingDetail From dbo.PatientClinical Where AppointmentId = '" + appointmentId + "' and ClinicalType = 'H' and Symptom in ('/DRINK', '?DRINK EVERY DAY') Order by ClinicalId";
			var ElementList = new DataTable();
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(ElementList);
					}
				}
			}

			if (ElementList.Rows.Count > 0)
			{
				int startIndex = 0;
				int endIndex = 0;
				string symptomType = string.Empty;
				foreach (DataRow dr in ElementList.Rows)
				{
					symptomType = dr["Symptom"].ToString();
					if (!string.IsNullOrEmpty(symptomType))
					{
						type = "DRINK";
						startIndex = 0;
						endIndex = dr["FindingDetail"].ToString().IndexOf('=');
						value = dr["FindingDetail"].ToString().Substring(startIndex, (endIndex - startIndex)).Replace("-", " ").Replace("*", " ");
					}
				}
			}
			Add(appointmentId, type, value);
			type = string.Empty;
			value = string.Empty;
		}

		private static void Add(string appointmentId, string type, string value)
		{
			if (!string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(value))
			{
				string insertQry = "Insert into dbo.ExtractedDrinkingStatusDetails(AppointmentId, Type, Value) Values ('" + appointmentId + "', '" + type.Replace("'", "''") + "', '" + value.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(insertQry, con))
					{
						cmd.CommandType = CommandType.Text;
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
		}
	}
}
