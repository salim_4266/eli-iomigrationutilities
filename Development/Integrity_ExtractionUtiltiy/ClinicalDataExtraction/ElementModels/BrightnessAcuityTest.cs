﻿using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
    public class BrightnessAcuityTest
    {
        public static void Extract(string appointmentId)
        {
            try
            {
                string eyeType = string.Empty;
                string batType = string.Empty;

                string OD_LowReading = string.Empty;
                string OD_HighReading = string.Empty;
                string OD_MediumReading = string.Empty;
                string OS_LowReading = string.Empty;
                string OS_HighReading = string.Empty;
                string OS_MediumReading = string.Empty;

                string OS_LowAcuityMethod = string.Empty;
                string OS_LowAcuityReading = string.Empty;
                string OS_MediumAcuityMethod = string.Empty;
                string OS_MediumAcuityReading = string.Empty;
                string OS_HighAcuityMethod = string.Empty;
                string OS_HighAcuityReading = string.Empty;
                string OD_LowAcuityMethod = string.Empty;
                string OD_LowAcuityReading = string.Empty;
                string OD_MediumAcuityMethod = string.Empty;
                string OD_MediumAcuityReading = string.Empty;
                string OD_HighAcuityMethod = string.Empty;
                string OD_HighAcuityReading = string.Empty;
                string tempControlId = string.Empty;
                int startIndex = 0;
                int endIndex = 0;
                string selectQry = "Select FindingDetail From dbo.PatientClinical Where AppointmentId = '" + appointmentId + "' and Symptom = '/BRIGHTNESS ACUITY TEST'";
                var ElementList = new DataTable();
                using (SqlConnection con = new SqlConnection(Util._connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(selectQry, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                        {
                            ds.Fill(ElementList);
                        }
                    }
                }
                if (ElementList.Rows.Count > 0)
                {
                    foreach (DataRow dr in ElementList.Rows)
                    {
                        if (dr["FindingDetail"].ToString().Contains("OD-BAT"))
                        {
                            eyeType = "OD";
                            batType = string.Empty;
                        }
                        else if (dr["FindingDetail"].ToString().Contains("OS-BAT"))
                        {
                            eyeType = "OS";
                            batType = string.Empty;
                        }
                        else if (dr["FindingDetail"].ToString().Contains("LOWBAT"))
                        {
                            batType = "LOW";
                            if(eyeType == "OD")
                            {
                                startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                OD_LowReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                            }
                            else if (eyeType == "OS")
                            {
                                startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                OS_LowReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                            }
                        }
                        else if (dr["FindingDetail"].ToString().Contains("MEDIUMBAT"))
                        {
                            batType = "MEDIUM";
                            if (eyeType == "OD")
                            {
                                startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                OD_MediumReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                            }
                            else if (eyeType == "OS")
                            {
                                startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                OS_MediumReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                            }
                        }
                        else if (dr["FindingDetail"].ToString().Contains("HIGHBAT"))
                        {
                            batType = "HIGH";
                            if (eyeType == "OD")
                            {
                                startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                OD_HighReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                            }
                            else if (eyeType == "OS")
                            {
                                startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                OS_HighReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                            }
                        }
                        else if (batType != "" && eyeType == "OD")
                        {
                            startIndex = dr["FindingDetail"].ToString().IndexOf('=');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('<');
                            tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
                            FormControls frmControl = new FormControls(tempControlId);
                            startIndex = dr["FindingDetail"].ToString().IndexOf('*');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('=');
                            if (batType == "LOW")
                            {
                                if (frmControl.ControlId == "15708" || frmControl.ControlId == "15709" || frmControl.ControlId == "15710" || frmControl.ControlId == "15711" || frmControl.ControlId == "15712" || frmControl.ControlId == "15713" || frmControl.ControlId == "9748" || frmControl.ControlId == "9747" || frmControl.ControlId == "9725" || frmControl.ControlId == "9726" || frmControl.ControlId == "1123" || frmControl.ControlId == "1124")
                                {
                                    OD_LowAcuityMethod = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                else
                                {
                                    OD_LowAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                if (frmControl.ControlType == "N")
                                {
                                    startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                    endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                    OD_LowAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                            }
                            else if (batType == "HIGH")
                            {
                                if (frmControl.ControlId == "15708" || frmControl.ControlId == "15709" || frmControl.ControlId == "15710" || frmControl.ControlId == "15711" || frmControl.ControlId == "15712" || frmControl.ControlId == "15713" || frmControl.ControlId == "9748" || frmControl.ControlId == "9747" || frmControl.ControlId == "9725" || frmControl.ControlId == "9726" || frmControl.ControlId == "1123" || frmControl.ControlId == "1124")
                                {
                                    OD_HighAcuityMethod = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                else
                                {
                                    OD_HighAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                if (frmControl.ControlType == "N")
                                {
                                    startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                    endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                    OD_HighAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                            }
                            else if (batType == "MEDIUM")
                            {
                                if (frmControl.ControlId == "15708" || frmControl.ControlId == "15709" || frmControl.ControlId == "15710" || frmControl.ControlId == "15711" || frmControl.ControlId == "15712" || frmControl.ControlId == "15713" || frmControl.ControlId == "9748" || frmControl.ControlId == "9747" || frmControl.ControlId == "9725" || frmControl.ControlId == "9726" || frmControl.ControlId == "1123" || frmControl.ControlId == "1124")
                                {
                                    OD_MediumAcuityMethod = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                else
                                {
                                    OD_MediumAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                if (frmControl.ControlType == "N")
                                {
                                    startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                    endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                    OD_MediumAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                            }
                        }
                        else if (batType != "" && eyeType == "OS")
                        {
                            startIndex = dr["FindingDetail"].ToString().IndexOf('=');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('<');
                            tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
                            FormControls frmControl = new FormControls(tempControlId);
                            startIndex = dr["FindingDetail"].ToString().IndexOf('*');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('=');
                            if (batType == "LOW")
                            {
                                if (frmControl.ControlId == "15708" || frmControl.ControlId == "15709" || frmControl.ControlId == "15710" || frmControl.ControlId == "15711" || frmControl.ControlId == "15712" || frmControl.ControlId == "15713" || frmControl.ControlId == "9748" || frmControl.ControlId == "9747" || frmControl.ControlId == "9725" || frmControl.ControlId == "9726" || frmControl.ControlId == "1123" || frmControl.ControlId == "1124")
                                {
                                    OS_LowAcuityMethod = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                else
                                {
                                    OS_LowAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                if (frmControl.ControlType == "N")
                                {
                                    startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                    endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                    OS_LowAcuityMethod += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                            }
                            else if (batType == "HIGH")
                            {
                                if (frmControl.ControlId == "15708" || frmControl.ControlId == "15709" || frmControl.ControlId == "15710" || frmControl.ControlId == "15711" || frmControl.ControlId == "15712" || frmControl.ControlId == "15713" || frmControl.ControlId == "9748" || frmControl.ControlId == "9747" || frmControl.ControlId == "9725" || frmControl.ControlId == "9726" || frmControl.ControlId == "1123" || frmControl.ControlId == "1124")
                                {
                                    OS_HighAcuityMethod = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                else
                                {
                                    OS_HighAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                if (frmControl.ControlType == "N")
                                {
                                    startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                    endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                    OS_HighAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                            }
                            else if (batType == "MEDIUM")
                            {
                                if (frmControl.ControlId == "15708" || frmControl.ControlId == "15709" || frmControl.ControlId == "15710" || frmControl.ControlId == "15711" || frmControl.ControlId == "15712" || frmControl.ControlId == "15713" || frmControl.ControlId == "9748" || frmControl.ControlId == "9747" || frmControl.ControlId == "9725" || frmControl.ControlId == "9726" || frmControl.ControlId == "1123" || frmControl.ControlId == "1124")
                                {
                                    OS_MediumAcuityMethod = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                else
                                {
                                    OS_MediumAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("*", "").Replace("-BAT", "");
                                }
                                if (frmControl.ControlType == "N")
                                {
                                    startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                                    endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                                    OS_MediumAcuityReading += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                                }
                            }
                        }
                    }

                    Add(appointmentId, OS_LowAcuityMethod, OS_LowReading + " " + OS_LowAcuityReading, OS_MediumAcuityMethod, OS_MediumReading + " " + OS_MediumAcuityReading, OS_HighAcuityMethod, OS_HighReading + " " + OS_HighAcuityReading, OD_LowAcuityMethod, OD_LowReading + " " + OD_LowAcuityReading, OD_MediumAcuityMethod, OD_MediumReading + " " + OD_MediumAcuityReading, OD_HighAcuityMethod, OD_HighReading + " " + OD_HighAcuityReading);
                }
            }
            catch
            {

            }
        }


        private static void Add(string appointmentId, string OS_LowAcuityMethod, string OS_LowAcuityReading, string OS_MediumAcuityMethod, string OS_MediumAcuityReading, string OS_HighAcuityMethod, string OS_HighAcuityReading, string OD_LowAcuityMethod, string OD_LowAcuityReading, string OD_MediumAcuityMethod, string OD_MediumAcuityReading, string OD_HighAcuityMethod, string OD_HighAcuityReading)
        {
            string insertQry = "Insert into ExtractedBrightnessAcuityTestDetails(AppointmentId, Time, OS_LowAcuityMethod, OS_LowAcuityReading, OS_MediumAcuityMethod, OS_MediumAcuityReading, OS_HighAcuityMethod, OS_HighAcuityReading ,OS_Comment, OD_LowAcuityMethod, OD_LowAcuityReading, OD_MediumAcuityMethod, OD_MediumAcuityReading, OD_HighAcuityMethod, OD_HighAcuityReading, OD_Comment) Values ('" + appointmentId + "', '', '" + OS_LowAcuityMethod.Replace("'", "''") + "', '" + OS_LowAcuityReading.Replace("'", "''") + "', '" + OS_MediumAcuityMethod.Replace("'", "''") + "', '" + OS_MediumAcuityReading.Replace("'", "''") + "', '" + OS_HighAcuityMethod.Replace("'", "''") + "', '" + OS_HighAcuityReading.Replace("'", "''") + "', '', '" + OD_LowAcuityMethod.Replace("'", "''") + "', '" + OD_LowAcuityReading.Replace("'", "''") + "', '" + OD_MediumAcuityMethod.Replace("'", "''") + "', '" + OD_MediumAcuityReading.Replace("'", "''") + "', '" + OD_HighAcuityMethod.Replace("'", "''") + "', '" + OD_HighAcuityReading.Replace("'", "''") + "', '' )";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(insertQry, con))
                {
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
        }
    }
}
