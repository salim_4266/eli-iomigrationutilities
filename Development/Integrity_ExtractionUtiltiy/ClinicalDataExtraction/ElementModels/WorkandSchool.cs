﻿using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
    public class WorkandSchool
    {
        public static void Extract(string appointmentId)
        {
            try
            {
                string value = string.Empty;
                string selectQry = "Select FindingDetail From dbo.PatientClinical Where AppointmentId = '" + appointmentId + "' and ClinicalType = 'H' and Symptom in ('/WORK AND SCHOOL') Order by ClinicalId";
                var ElementList = new DataTable();
                using (SqlConnection con = new SqlConnection(Util._connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(selectQry, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                        {
                            ds.Fill(ElementList);
                        }
                    }
                }

                if (ElementList.Rows.Count > 0)
                {
                    foreach (DataRow dr in ElementList.Rows)
                    {
                        int startIndex = 0;
                        int endIndex = dr["FindingDetail"].ToString().IndexOf('=');
                        value = dr["FindingDetail"].ToString().Substring(startIndex, (endIndex - startIndex)).Replace("-", " ");
                        Add(appointmentId, value);
                    }
                }
            }
            catch { }
        }

        private static void Add(string appointmentId, string value)
        {
            string insertQry = "Insert into dbo.ExtractedWorkandSchoolDetails(AppointmentId, Value) Values ('" + appointmentId + "', '" + value.Replace("'", "''") + "')";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(insertQry, con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
        }
    }
}
