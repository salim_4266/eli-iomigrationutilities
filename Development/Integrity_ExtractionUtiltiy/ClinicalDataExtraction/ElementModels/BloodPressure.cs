﻿
using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
	internal sealed class BloodPressure
	{
		private BloodPressure()
		{

		}

		private static string Value = string.Empty;

		private static string BLOODPRESSURE = string.Empty;

		private static string BLOODPRESSURELEFT	 = string.Empty;

		private static string BLOODPRESSURERTWRIST = string.Empty;

		private static string BLOODPRESSURELEFTWRIST = string.Empty;

		private static string PULSERATE = string.Empty;

		private static string RESPIRATION = string.Empty;

		private static string TEMPERATURE = string.Empty;

		public static void Extract(string appointmentId)
		{
			DataTable ElementList = new DataTable();
			string selectQry = "Select FindingDetail From dbo.PatientClinical Where AppointmentId = '" + appointmentId + "' and ClinicalType = 'F' and Symptom  = '/BLOOD PRESSURE'";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(ElementList);
					}
				}
			}

			if (ElementList.Rows.Count > 0)
			{
				foreach (DataRow dr in ElementList.Rows)
				{
					int startIndex = 0;
					int endIndex = 0;
					if (dr["FindingDetail"].ToString().Contains("TIME"))
					{

					}
					else if (dr["FindingDetail"].ToString().Contains("*BLOODPRESSURE="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						BLOODPRESSURE = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*BLOODPRESSURELEFT="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						BLOODPRESSURELEFT = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*BLOODPRESSURELEFTWRIST="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						BLOODPRESSURELEFTWRIST = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*BLOODPRESSURERTWRIST="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						BLOODPRESSURERTWRIST = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*PULSERATE="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						PULSERATE = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*TEMPERATURE=")) // *WEIGHT-POUNDS=T14741 <8>
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						TEMPERATURE = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*RESPIRATION=")) // *WEIGHT-OUNCES=T14746 <6>
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						RESPIRATION = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						Value += dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1) + " ";
					}
				}

				Add(appointmentId);
			}
		}

		private static void Add(string appointmentId)
		{
			string insertQry = "Insert into ExtractedBloodPressureDetails(AppointmentId, BLOODPRESSURE, BLOODPRESSURELEFT, BLOODPRESSURERTWRIST, BLOODPRESSURELEFTWRIST, TEMPERATURE, RESPIRATION, PULSERATE, Value) Values ('" + appointmentId + "', '" + BLOODPRESSURE.Replace("'", "''") + "', '" + BLOODPRESSURELEFT.Replace("'", "''") + "', '"+ BLOODPRESSURERTWRIST.Replace("'", "''") + "', '" + BLOODPRESSURELEFTWRIST.Replace("'", "''") + "', '" + TEMPERATURE.Replace("'", "''") + "', '" + RESPIRATION.Replace("'", "''") + "', '" + PULSERATE.Replace("'", "''") + "', '" + Value.Replace("'", "''") + "')";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				con.Open();
				using (SqlCommand cmd = new SqlCommand(insertQry, con))
				{
					cmd.CommandType = CommandType.Text;
					cmd.ExecuteNonQuery();
				}
				con.Close();
			}
		}
	}
}
