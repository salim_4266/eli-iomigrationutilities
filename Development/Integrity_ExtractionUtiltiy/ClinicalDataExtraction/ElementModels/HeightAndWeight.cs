﻿using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
	internal sealed class HeightAndWeight
	{
		private HeightAndWeight()
		{

		}

		private static string BMI = string.Empty;
		private static string HEIGHTFEET = string.Empty;
		private static string HEIGHTINCHES = string.Empty;
		private static string HEIGHTMETERS = string.Empty;
		private static string HEIGHTCM = string.Empty;
		private static string WEIGHTPOUNDS = string.Empty;
		private static string WEIGHTOUNCES = string.Empty;
		private static string WEIGHTKGS = string.Empty;
		private static string WEIGHTGRAMS = string.Empty;

		public static void Extract(string appointmentId)
		{
			DataTable ElementList = new DataTable();
			string selectQry = "Select FindingDetail From dbo.PatientClinical Where AppointmentId = '" + appointmentId + "' and ClinicalType = 'F' and Symptom  = '/HEIGHT AND WEIGHT'";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(ElementList);
					}
				}
			}

			if (ElementList.Rows.Count > 0)
			{
				foreach (DataRow dr in ElementList.Rows)
				{
					int startIndex = 0;
					int endIndex = 0;
					if (dr["FindingDetail"].ToString().Contains("TIME"))
					{

					}
					else if (dr["FindingDetail"].ToString().Contains("*BMI="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						BMI = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*HEIGHT-FEET="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						HEIGHTFEET = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*HEIGHT-INCHES="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						HEIGHTINCHES = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*HEIGHT-METERS="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						HEIGHTMETERS = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*HEIGHT-CM="))
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						HEIGHTCM = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*WEIGHT-POUNDS=")) // *WEIGHT-POUNDS=T14741 <8>
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						WEIGHTPOUNDS = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*WEIGHT-OUNCES=")) // *WEIGHT-OUNCES=T14746 <6>
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						WEIGHTOUNCES = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*WEIGHT-KGS=")) // *WEIGHT-KGS=T14743 <4>
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						WEIGHTKGS = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
					else if (dr["FindingDetail"].ToString().Contains("*WEIGHT-GRAMS=")) // *WEIGHT-GRAMS=T14751 <78>
					{
						startIndex = dr["FindingDetail"].ToString().IndexOf('<');
						endIndex = dr["FindingDetail"].ToString().IndexOf('>');
						WEIGHTGRAMS = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
					}
				}

				Add(appointmentId);
			}
		}

		private static void Add(string appointmentId)
		{
			string insertQry = "Insert into ExtractedHeightAndWeightDetails(AppointmentId, BMI, HEIGHTFEET, HEIGHTINCHES, HEIGHTMETERS, HEIGHTCM, WEIGHTPOUNDS, WEIGHTOUNCES, WEIGHTKGS, WEIGHTGRAMS) Values ('" + appointmentId + "', '" + BMI.Replace("'", "''") + "', '" + HEIGHTFEET.Replace("'", "''") + "', '" + HEIGHTINCHES.Replace("'", "''") + "', '" + HEIGHTMETERS.Replace("'", "''") + "', '" + HEIGHTCM.Replace("'", "''") + "', '" + WEIGHTPOUNDS.Replace("'", "''") + "', '" + WEIGHTOUNCES.Replace("'", "''") + "', '" + WEIGHTKGS.Replace("'", "''") + "', '" + WEIGHTGRAMS.Replace("'", "''") + "')";
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				con.Open();
				using (SqlCommand cmd = new SqlCommand(insertQry, con))
				{
					cmd.CommandType = CommandType.Text;
					cmd.ExecuteNonQuery();
				}
				con.Close();
			}
		}
	}
}
