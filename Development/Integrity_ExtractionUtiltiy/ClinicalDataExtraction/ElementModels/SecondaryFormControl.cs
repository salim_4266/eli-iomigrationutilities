﻿
using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
    public class SecondaryFormControl
    {
        public string ControlId { get; set; }

        public string DoctorLingo { get; set; }

        public SecondaryFormControl(string controlId)
        {
            this.ControlId = "0";
            this.DoctorLingo = string.Empty;

            string selectQry = "Select SecondaryFormId as ControlId, ControlText FROM df.secondaryformscontrols WHERE SecondaryControlId = '" + controlId + "'";
            var ElementList = new DataTable();
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(selectQry, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                    {
                        ds.Fill(ElementList);
                    }
                }
            }
            if (ElementList.Rows.Count > 0)
            {
                this.ControlId = ElementList.Rows[0]["ControlId"].ToString();
                this.DoctorLingo = ElementList.Rows[0]["ControlText"].ToString();
            }
        }
    }
}
