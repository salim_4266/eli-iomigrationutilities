﻿using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
    internal class ColorScreen
    {
        public static void Extract(string appointmentId)
        {
            try
            {
                string eyeType = string.Empty;
                string value = string.Empty;
                string selectQry = "Select FindingDetail From dbo.PatientClinical Where AppointmentId = '" + appointmentId + "' and Symptom = '/COLOR SCREEN'";
                var ElementList = new DataTable();
                using (SqlConnection con = new SqlConnection(Util._connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(selectQry, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                        {
                            ds.Fill(ElementList);
                        }
                    }
                }
                if (ElementList.Rows.Count > 0)
                {
                    foreach (DataRow dr in ElementList.Rows)
                    {
                        if (!dr["FindingDetail"].ToString().Contains("TIME"))
                        {
                            int startIndex = dr["FindingDetail"].ToString().IndexOf('*');
                            int endIndex = dr["FindingDetail"].ToString().IndexOf('=');
                            value += " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1);
                        }
                    }
                    Add(appointmentId, value, "");
                }
            }
            catch { }
        }

        private static void Add(string appointmentId, string ODValue, string OSValue)
        {
            string insertQry = "Insert into ExtractedColorScreenDetails(AppointmentId, Time, OD_Value, OS_Value) Values ('" + appointmentId + "', '', '" + ODValue.Replace("'", "''") + "', '" + OSValue.Replace("'", "''") + "')";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(insertQry, con))
                {
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
        }
    }
}
