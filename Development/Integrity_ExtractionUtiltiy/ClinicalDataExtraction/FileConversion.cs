﻿using System;
using System.Linq;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace ClinicalDataExtraction
{
    public class FileConversion
    {
        public string path2Current { get; set; }

        public string path2NewDirectory { get; set; }

        public bool Convert(string _type)
        {
            bool retStatus = false;
            string appointmentId = string.Empty;
            string patientId = string.Empty;
            string path2NewImage = string.Empty;
            if (_type == "image")
            {
                FileInfo _objectFile = new FileInfo(path2Current);
                if (_objectFile.Exists)
                {
                    if (path2Current.IndexOf("-") > 0)
                    {
                        string[] ImageSturcture = path2Current.Split('-');
                        if (ImageSturcture.Count() > 2)
                        {
                            appointmentId = ImageSturcture[1].Replace("A", "");
                            patientId = getPatientId(appointmentId);
                            if (patientId != "")
                            {
                                string newPath = patientId + @"_" + appointmentId + @"_" +  Guid.NewGuid().ToString().Substring(1,5) + _objectFile.Extension;
                                File.Move(path2Current, path2NewDirectory + @"/" + newPath);
                                InsertRecord(appointmentId, patientId, _objectFile.FullName, newPath);
                            }
                        }
                    }
                }
            }
            return retStatus;
        }

        private string getPatientId(string _appointmentId)
        {
            string _patientId = string.Empty;
            DataTable dsTemp = new DataTable();
            string _selectQry = "Select PatientId from dbo.Appointments with (nolock) Where AppointmentId = '" + _appointmentId + "'";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                    {
                        ds.Fill(dsTemp);
                    }
                }
            }

            foreach (DataRow dr in dsTemp.Rows)
            {
                _patientId = dr["PatientId"].ToString();
            }

            return _patientId;
        }

        private void InsertRecord(string appointmentId, string patientId, string oldImagePath, string newImagePath)
        {
            string _selectQry = "Insert into dbo.ExtractedImageConvertedDetails Values ('" + patientId + "', '" + appointmentId + "', '" + oldImagePath + "', '" + newImagePath + "')";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(_selectQry, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
    }
}

