﻿using System.Data;
using System.Data.SqlClient;

namespace ClinicalDataExtraction
{
	public class AppointmentHPI
	{
		public string Symptom { get; set; } = "";
		public string Finding { get; set; } = "";
		public string _Condition { get; set; } = "";
		public string _HowLong { get; set; } = "";
		public string _PainType { get; set; } = "";
		public string _WhatType { get; set; } = "";
		public string _WhichEye { get; set; } = "";
		public string _WhichSide { get; set; } = "";
		public string _WhichEyeFlashes { get; set; } = "";
		public string _WhichEyeBurning { get; set; } = "";
		public string _WhenDiagnosed { get; set; } = "";
		public string _WeightLossGain { get; set; } = "";

		public void Add(string _appointmentId)
		{
			string _selectQry = "Select 1 From ExtractedHPIDetails Where AppointmentId  = '" + _appointmentId + "'" + " and Symptom = '" + Symptom.Replace("'", "''") + "' and ButtonText = '" + Finding.Replace("'", "''") + "'";
			DataTable dsSymptoms = new DataTable();
			using (SqlConnection con = new SqlConnection(Util._connectionString))
			{
				using (SqlCommand cmd = new SqlCommand(_selectQry, con))
				{
					cmd.CommandType = CommandType.Text;
					using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
					{
						ds.Fill(dsSymptoms);
					}
				}
			}

			if (dsSymptoms.Rows.Count == 0)
			{
				string _insertQry = "Insert into ExtractedHPIDetails(AppointmentId, Symptom, ButtonText, Condition, HowLong, PainType, WhatType, WhichEye, WhichEyeFlashes, WhichEyeBurning, WhichSide, WeightLossGain) Values ('" + _appointmentId + "', '" + Symptom.Replace("'", "''") + "', '" + Finding.Replace("'", "''") + "', '" + _Condition.Replace("'", "''") + "', '" + _HowLong.Replace("'", "''") + "', '" + _PainType.Replace("'", "''") + "', '" + _WhatType.Replace("'", "''") + "', '" + _WhichEye.Replace("'", "''") + "','" + _WhichEyeFlashes.Replace("'", "''") + "','" + _WhichEyeBurning.Replace("'", "''") + "', '" + _WhichSide.Replace("'", "''") + "','" + _WeightLossGain.Replace("'", "''") + "')";
				using (SqlConnection con = new SqlConnection(Util._connectionString))
				{
					con.Open();
					using (SqlCommand cmd = new SqlCommand(_insertQry, con))
					{
						cmd.ExecuteNonQuery();
					}
					con.Close();
				}
			}
		}
	}
}
