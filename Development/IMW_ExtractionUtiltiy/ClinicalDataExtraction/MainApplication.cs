﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicalDataExtraction
{
    public partial class MainApplication : Form
    {
        public MainApplication()
        {
            InitializeComponent();
            PatientIds = new List<string>();
        }
        List<string> PatientIds = null;
        private async void button1_Click(object sender, EventArgs e)
        {
            string AppId = "";
            string PatientId = "";
            button1.Enabled = false;
            int recCnt = 0;
            int patientProccessed = 0;

            if (txtFromPatientId.Text.Trim() != "" && txtToPatientId.Text.Trim() != "")
            {
                string _fromPatientId = txtFromPatientId.Text.ToString();
                string _toPatientId = txtToPatientId.Text.ToString();
                DataTable dtElement = new DataTable();
                string _selectQry = "Select A.AppointmentId as AppId,p.Id as PatientId From model.Patients p inner join dbo.Appointments a on p.Id = A.PatientId Where A.AppointmentId not in (Select AppointmentId from AppointmentExtractionStatus) and P.Id Between '" + _fromPatientId + "' and '" + _toPatientId + "'";
                using (SqlConnection con = new SqlConnection(Util._connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dtElement);
                        if (dtElement.Rows.Count > 0)
                        {
                            pnlExtraction.Visible = true;
                            lblTotalNoOfPatients.Text = dtElement.DefaultView.ToTable(true, "PatientId").Rows.Count.ToString();
                            lblTotAppointment.Text = dtElement.Rows.Count.ToString();
                        }
                    }
                }
                var progressHandler = new Progress<string>(value =>
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        lblPatientProcessed.Text = value.Split('~')[3];
                        lblAppointmentProcessed.Text = value.Split('~')[0];
                        lblCurrAppt.Text = value.Split('~')[1];
                        lblCurrPatient.Text = value.Split('~')[2];
                    }
                });
                var progress = progressHandler as IProgress<string>;
                await Task.Run(() =>
                {
                    foreach (DataRow dr in dtElement.Rows)
                    {
                        recCnt++;
                        AppId = dr["AppId"].ToString();
                        PatientId = dr["PatientId"].ToString();
                        if (!PatientIds.Contains(PatientId))
                        {
                            PatientIds.Add(PatientId);
                            patientProccessed = int.Parse(lblTotalNoOfPatients.Text) - 1;
                        }
                        new ClinicalExtraction().GenerateExamElement(AppId);
                        if (progress != null)
                        {
                            if (recCnt == dtElement.Rows.Count)
                            {
                                patientProccessed = PatientIds.Count;
                                progress.Report(recCnt.ToString() + "~" + AppId + "~" + PatientId + "~" + patientProccessed);
                            }
                            else
                                progress.Report(recCnt.ToString() + "~" + AppId + "~" + PatientId + "~" + patientProccessed);
                        }
                    }
                });
                lblExtrctionPrgs.Text = "Extraction Process Completed.";
                string HTMLContent = "<p>Hi Team, <br/> <br/>Extraction is completed, Please find status of extraction.</p><table><tr><td>Patient Range</td><td>xxxToPatientxxx - xxxFromPatientxxx</td></tr><tr><td>Patient Extracted</td><td>xxxPatientExtractedxxx</td></tr><tr><td>Appointment Extracted&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>xxxAppointmentExtractedxxx</td></tr></table><br><br>Thanks";
                HTMLContent = HTMLContent.Replace("xxxToPatientxxx", _fromPatientId);
                HTMLContent = HTMLContent.Replace("xxxFromPatientxxx", _toPatientId);
                HTMLContent = HTMLContent.Replace("xxxPatientExtractedxxx", lblTotalNoOfPatients.Text);
                HTMLContent = HTMLContent.Replace("xxxAppointmentExtractedxxx", lblTotAppointment.Text);
                new EmailUtility()
                {
                    _EmailBody = HTMLContent,
                    _Subject = ConfigurationManager.AppSettings["Subject"].ToString(),
                    _To = ConfigurationManager.AppSettings["ToAddresses"].ToString()
                }.SendMail();
            }
            else
            {
                MessageBox.Show("Please insert From PatientId and To PatientId");
            }
            //new ClinicalExtraction().GenerateExamElement(txtFromPatientId.Text.Trim());
            button1.Enabled = true;
        }

        private void ResetValues()
        {
            lblTotalNoOfPatients.Text =
            lblTotAppointment.Text =
            lblAppointmentProcessed.Text =
            lblCurrAppt.Text = lblCurrPatient.Text =
            lblAppointmentProcessed.Text = string.Empty;
        }

        private void MainApplication_Load(object sender, EventArgs e)
        {
            Util._connectionString = ConfigurationManager.AppSettings["PracticeRepository"].ToString();
            DBInteraction _objDBInteraction = new DBInteraction();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ImageConversion frm = new ImageConversion();
            frm.Show();
            this.Hide();
        }
    }
}
