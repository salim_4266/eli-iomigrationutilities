﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ClinicalDataExtraction
{
    public class EmailUtility
    {
        public string _To = "";
        public string _Subject = "";
        public string _EmailBody = "";

        public void SendMail()
        {
            try
            {
                using (SmtpClient SmtpMail = new SmtpClient("smtp.elasticemail.com", 587))
                {
                    MailMessage message = new MailMessage("help@iopracticeware.com", this._To, this._Subject, this._EmailBody);
                    message.From = new MailAddress("help@iopracticeware.com", "Extraction Status");
                    message.Priority = MailPriority.Normal;
                    message.IsBodyHtml = true;
                    SmtpMail.EnableSsl = true;
                    SmtpMail.Send(message);
                }
            }
            catch
            {
            }
        }
    }
}
