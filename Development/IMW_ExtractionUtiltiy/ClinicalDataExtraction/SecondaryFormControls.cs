﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicalDataExtraction
{
    public class SecondaryFormControlModel
    {
        public string SecondaryFormId { get; set; }

        public string CurrentText { get; set; }

        public string ReplacedText { get; set; }

        public bool isActive { get; set; }
    }

    public class SecondaryFormControls
    {
        public List<SecondaryFormControlModel> Get()
        {
            List<SecondaryFormControlModel> FormList = new List<SecondaryFormControlModel>();
            DataTable dsSymptoms = new DataTable();
            string _selectQry = "Select * from dbo.ExtractionMasterSecondaryForms with (nolock)";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                    {
                        ds.Fill(dsSymptoms);
                    }
                }
            }

            foreach (DataRow dr in dsSymptoms.Rows)
            {
                FormList.Add(new SecondaryFormControlModel()
                {
                    CurrentText = Convert.ToString(dr["CurrentText"]),
                    isActive = Convert.ToBoolean(dr["isActive"]),
                    ReplacedText = Convert.ToString(dr["ReplacedText"]),
                    SecondaryFormId = Convert.ToString(dr["SecondaryFormId"])
                });
            }

            return FormList;
        }
    }
}
