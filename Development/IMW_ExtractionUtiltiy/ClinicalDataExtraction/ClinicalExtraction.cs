﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicalDataExtraction
{
    public class ClinicalExtraction
    {
        private static List<MasterElementModel> ElementList = null;  
        public ClinicalExtraction()
        {
            ElementList = new MasterElements().Get();
        }

        public bool GenerateExamElement(string _appointmentId)
        {
            bool retStatus = false;
            try
            {
                // To Extract Past medical history, History of present illness and Ocular history (Clinical Extraction - H)
                ClincialExtractionTypeH.Extarct_PMH_HPI_OCX(_appointmentId);

                //To Extract Family History Details
                ClincialExtractionTypeH.ExtractFamilyHistory(_appointmentId);

                // To Extract Cheif Complaints Details
                ClincialExtractionTypeH.ExtractCheifComplaint(_appointmentId);

                // To Extract exam elements - Contact Lens Information
                ExtractContactLens(_appointmentId);

                // All Remaining Items 
                DataTable dsSymptoms = new DataTable();
                AppointmentExamElement objAEE = new AppointmentExamElement();
                objAEE.AppointmentId = _appointmentId;
                string _selectQry = "Select Distinct Symptom From dbo.PatientClinical with (nolock) Where AppointmentId = '" + _appointmentId + "' and ClinicalType = 'F'";
                using (SqlConnection con = new SqlConnection(Util._connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                        {
                            ds.Fill(dsSymptoms);
                        }
                    }
                }
                if (dsSymptoms.Rows.Count > 0)
                {
                    objAEE.ElementDetail = getDetail(dsSymptoms, _appointmentId);
                }

                new AppointmentExtractionStatus()
                {
                    AppointmentId = _appointmentId,
                    ExceptionMessage = "",
                    Status = "1"
                }.Save();

            }
            catch (Exception ee)
            {
                new AppointmentExtractionStatus()
                {
                    AppointmentId = _appointmentId,
                    ExceptionMessage = ee.Message + "/r/n" + ee.InnerException + "/r/n" + ee.StackTrace,
                    Status = "0"
                }.Save();
            }
            return retStatus;
        }

        public void ExtractContactLens(string _appointmentId)
        {
            try
            {
                string FindingDetail = "";
                string Comment = "";
                string _selectQry = "Select FindingDetail, ImageDescriptor from dbo.PatientClinical with (nolock) Where AppointmentId = '" + _appointmentId + "' and ClinicalType = 'A' and FindingDetail Like 'DISPENSE CL%' and Status != 'D'";
                using (SqlConnection con = new SqlConnection(Util._connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                    {
                        SqlDataReader rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            FindingDetail = rdr["FindingDetail"].ToString();
                            Comment = rdr["ImageDescriptor"].ToString();
                        }
                    }
                    con.Close();
                }

                if (FindingDetail != "")
                {
                    string OD_Sphere = "";
                    string OS_Sphere = "";
                    string OD_Cylinder = "";
                    string OS_Cylinder = "";
                    string OD_Axis = "";
                    string OS_Axis = "";
                    string OD_AddReading = "";
                    string OS_AddReading = "";
                    string OD_BaseCurve = "";
                    string OS_BaseCurve = "";
                    string OD_Diameter = "";
                    string OS_Diameter = "";
                    string OD_PeriphCurve = "";
                    string OS_PeriphCurve = "";
                    string OD_DORN = "";
                    string OS_DORN = "";

                    FindingDetail = FindingDetail.Replace("DISPENSE CL RX", "").Replace("BASE CURVE", "BASE_CURVE").Replace("PERIPH CURVE", "PERIPH_CURVE").Replace("D OR N", "D_OR_N");
                    int startIndex = FindingDetail.IndexOf("-2/");
                    int endIndex = FindingDetail.IndexOf("-3/");
                    if (startIndex != 0 && endIndex != 0)
                    {
                        string OD_Findings = FindingDetail.Substring(startIndex + 3, (endIndex - startIndex) - 3);
                        OD_Sphere = OD_Findings.Split(' ')[0];
                        OD_Cylinder = OD_Findings.Split(' ')[1];
                        OD_Axis = OD_Findings.Split(' ')[2];
                        if (OD_Findings.Contains("ADD-READING"))
                            OD_AddReading = OD_Findings.Substring(OD_Findings.IndexOf("ADD-READING:") + 12, 5);
                        if (OD_Findings.Contains("BASE_CURVE"))
                            OD_BaseCurve = OD_Findings.Substring(OD_Findings.IndexOf("BASE_CURVE:") + 11, 3);
                        if (OD_Findings.Contains("DIAMETER:"))
                            OD_Diameter = OD_Findings.Substring(OD_Findings.IndexOf("DIAMETER:") + 9, 2);
                        if (OD_Findings.Contains("PERIPH_CURVE"))
                            OD_PeriphCurve = OD_Findings.Substring(OD_Findings.IndexOf("PERIPH_CURVE:") + 13, 5);
                        if (OD_Findings.Contains("D_OR_N"))
                            OD_DORN = OD_Findings.Substring(OD_Findings.IndexOf("D_OR_N") + 7, 1);
                    }
                    startIndex = FindingDetail.IndexOf("-3/");
                    endIndex = FindingDetail.IndexOf("-4/");
                    if (startIndex != 0 && endIndex != 0)
                    {
                        string OS_Findings = FindingDetail.Substring(startIndex + 3, (endIndex - startIndex) - 3);
                        OS_Sphere = OS_Findings.Split(' ')[0];
                        OS_Cylinder = OS_Findings.Split(' ')[1];
                        OS_Axis = OS_Findings.Split(' ')[2];
                        if (OS_Findings.Contains("ADD-READING"))
                            OS_AddReading = OS_Findings.Substring(OS_Findings.IndexOf("ADD-READING:") + 12, 5);
                        if (OS_Findings.Contains("BASE_CURVE"))
                            OS_BaseCurve = OS_Findings.Substring(OS_Findings.IndexOf("BASE_CURVE:") + 11, 3);
                        if (OS_Findings.Contains("DIAMETER:"))
                            OS_Diameter = OS_Findings.Substring(OS_Findings.IndexOf("DIAMETER:") + 9, 2);
                        if (OS_Findings.Contains("PERIPH_CURVE"))
                            OS_PeriphCurve = OS_Findings.Substring(OS_Findings.IndexOf("PERIPH_CURVE:") + 13, 5);
                        if (OS_Findings.Contains("D_OR_N"))
                            OS_DORN = OS_Findings.Substring(OS_Findings.IndexOf("D_OR_N") + 7, 1);
                    }

                    startIndex = FindingDetail.IndexOf("-5/");
                    endIndex = FindingDetail.IndexOf("-6/");
                    string OD_ModelId = FindingDetail.Substring(startIndex + 3, (endIndex - startIndex - 3));

                    startIndex = FindingDetail.IndexOf("-6/");
                    endIndex = FindingDetail.IndexOf("-7/");
                    string OS_ModelId = FindingDetail.Substring(startIndex + 3, (endIndex - startIndex - 3));

                    string OD_Model = "";
                    string OS_Model = "";

                    if (OD_ModelId.Trim() != "")
                    {
                        OD_Model = new CLInventory(OD_ModelId).ModelName;
                    }

                    if (OS_ModelId.Trim() != "")
                    {
                        OS_Model = new CLInventory(OS_ModelId).ModelName;
                    }

                    string insertQry = "Insert into ExtractedContactLensDetails(AppointmentId, OD_Sphere, OS_Sphere, OD_Cylinder, OS_Cylinder, OD_Axis, OS_Axis, OD_AddReading, OS_AddReading, OD_BaseCurve, OS_BaseCurve, OD_Diameter, OS_Diameter, OD_PeriphCurve, OS_PeriphCurve, OD_DORN, OS_DORN, OD_Comment, OD_Model, OS_Model) Values ('" + _appointmentId + "', '" + OD_Sphere.Replace("'", "''") + "', '" + OS_Sphere.Replace("'", "''") + "', '" + OD_Cylinder.Replace("'", "''") + "', '" + OS_Cylinder.Replace("'", "''") + "', '" + OD_Axis.Replace("'", "''") + "', '" + OS_Axis.Replace("'", "''") + "', '" + OD_AddReading.Replace("'", "''") + "', '" + OS_AddReading.Replace("'", "''") + "', '" + OD_BaseCurve.Replace("'", "''") + "', '" + OS_BaseCurve.Replace("'", "''") + "', '" + OD_Diameter.Replace("'", "''") + "', '" + OS_Diameter.Replace("'", "''") + "', '" + OD_PeriphCurve.Replace("'", "''") + "', '" + OS_PeriphCurve.Replace("'", "''") + "', '" + OD_DORN.Replace("'", "''") + "', '" + OS_DORN.Replace("'", "''") + "', '" + Comment.Replace("'", "''") + "', '" + OD_Model.Replace("'", "''") + "','" + OS_Model.Replace("'", "''") + "')";
                    using (SqlConnection con = new SqlConnection(Util._connectionString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand(insertQry, con))
                        {
                            cmd.ExecuteNonQuery();
                        }
                        con.Close();
                    }
                }

            }
            catch
            {

            }
        }

        private List<ExamElement> getDetail(DataTable dtSymptom, string _appointmentId)
        {
            List<ExamElement> objListExamElement = new List<ExamElement>();
            foreach (DataRow dt in dtSymptom.Rows)
            {
                ExamElement objElement = new ExamElement()
                {
                    Name = dt["Symptom"].ToString(),
                    Content = getExamDetail(dt["Symptom"].ToString(), _appointmentId)
                };
                objElement.Save(_appointmentId);
                objListExamElement.Add(objElement);
            }
            return objListExamElement;
        }

        private EyeContext getExamDetail(string _symptomType, string _appointmentId)
        {
            EyeContext objEyeContext = new EyeContext();
            List<KeyValuePair<string, string>> objkeyPairs = new List<KeyValuePair<string, string>>();
            DataTable dtElement = new DataTable();
            string _selectQry = "Select FindingDetail From dbo.PatientClinical with (nolock) Where Symptom = '" + _symptomType + "' and AppointmentId = '" + _appointmentId + "' and Status = 'A' and ClinicalType = 'F' Order by ClinicalId";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                    {
                        ds.Fill(dtElement);
                    }
                }
            }
            string _eyeType = "";
            foreach (DataRow dr in dtElement.Rows)
            {
                int startIndex = 0;
                int endIndex = 0;
                string tempControlId = "";
                if (dr["FindingDetail"].ToString().Contains("TIME"))
                {
                    _eyeType = "";
                    startIndex = dr["FindingDetail"].ToString().IndexOf('=');
                    endIndex = dr["FindingDetail"].ToString().IndexOf('(');
                    if (endIndex != -1)
                        objkeyPairs.Add(new KeyValuePair<string, string>("TIME", dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex - 1))));
                    else
                        objkeyPairs.Add(new KeyValuePair<string, string>("TIME", dr["FindingDetail"].ToString().Substring(startIndex + 1, 5)));
                }
                else if ((dr["FindingDetail"].ToString().Contains("*OD-") || _eyeType == "OD") && !dr["FindingDetail"].ToString().Contains("*OS-"))
                {
                    if (_eyeType == "")
                    {
                        _eyeType = "OD";
                    }
                    else
                    {
                        startIndex = dr["FindingDetail"].ToString().IndexOf('=');
                        endIndex = dr["FindingDetail"].ToString().IndexOf('<');
                        tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
                        FormControls objForm1 = new FormControls(tempControlId);
                        if (objForm1.ControlType == "N")
                        {
                            startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                            if(ElementList.FirstOrDefault(x => x.Name == _symptomType.Replace("/", "")) != null)
                            //if (_symptomType.Replace("/", "") == "GLASSES" || _symptomType.Replace("/", "") == "AUTOREFRACTION" || _symptomType.Replace("/", "") == "MANIFEST REFRACTION" || _symptomType.Replace("/", "") == "IOP" || _symptomType.Replace("/", "") == "AUTOKERATOMETRY" || _symptomType.Replace("/", "") == "BOTOX, MEDICAL")
                            {
                                objkeyPairs.Add(new KeyValuePair<string, string>("OD_" + objForm1.ControlText.Replace("'", "''"), dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
                            }
                            else
                            {
                                objkeyPairs.Add(new KeyValuePair<string, string>("OD_Value", objForm1.ControlText.Replace("'", "''") + " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
                            }
                        }
                        else
                        {
                            objkeyPairs.Add(new KeyValuePair<string, string>("OD_Value", objForm1.ControlText.Replace("'", "''")));
                        }
                    }
                }
                else if (dr["FindingDetail"].ToString().Contains("*OS-") || _eyeType == "OS")
                {
                    if (_eyeType != "OS")
                    {
                        _eyeType = "OS";
                    }
                    else
                    {
                        startIndex = dr["FindingDetail"].ToString().IndexOf('=');
                        endIndex = dr["FindingDetail"].ToString().IndexOf('<');
                        tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
                        FormControls objForm1 = new FormControls(tempControlId);
                        if (objForm1.ControlType == "N")
                        {
                            startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                            if (ElementList.FirstOrDefault(x => x.Name == _symptomType.Replace("/", "")) != null)
                            //if (_symptomType.Replace("/", "") == "GLASSES" || _symptomType.Replace("/", "") == "AUTOREFRACTION" || _symptomType.Replace("/", "") == "MANIFEST REFRACTION" || _symptomType.Replace("/", "") == " " || _symptomType.Replace("/", "") == "AUTOKERATOMETRY" || _symptomType.Replace("/", "") == "BOTOX, MEDICAL")
                            {
                                objkeyPairs.Add(new KeyValuePair<string, string>("OS_" + objForm1.ControlText.Replace("'", "''"), dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
                            }
                            else
                            {
                                objkeyPairs.Add(new KeyValuePair<string, string>("OS_Value", objForm1.ControlText.Replace("'", "''") + " " + dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
                            }
                        }
                        else
                        {
                            objkeyPairs.Add(new KeyValuePair<string, string>("OS_Value", objForm1.ControlText));
                        }
                    }
                }
                else
                {
                    if (_symptomType == "/SMOKING" || _symptomType == "/GENERAL MEDICAL OBSERVATION")
                    {
                        startIndex = dr["FindingDetail"].ToString().IndexOf('=');
                        endIndex = dr["FindingDetail"].ToString().IndexOf('<');
                        tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
                        FormControls objForm1 = new FormControls(tempControlId);
                        if (objForm1.ControlType == "N")
                        {
                            startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                            objkeyPairs.Add(new KeyValuePair<string, string>("typeValue", (objForm1.LetterTranslation.Trim() != "" ? objForm1.LetterTranslation.Replace("'", "''") : objForm1.ControlText.Replace("'", "''"))));
                        }
                        else
                        {
                            objkeyPairs.Add(new KeyValuePair<string, string>("typeValue", (objForm1.LetterTranslation.Trim() != "" ? objForm1.LetterTranslation.Replace("'", "''") : objForm1.ControlText.Replace("'", "''"))));
                        }
                    }
                    else if (_symptomType == "/VISION CORRECTED" || _symptomType == "/VISION UNCORRECTED")
                    {
                        startIndex = dr["FindingDetail"].ToString().IndexOf('=');
                        endIndex = dr["FindingDetail"].ToString().IndexOf('<');
                        tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
                        FormControls objForm1 = new FormControls(tempControlId);
                        if (objForm1.ControlType == "R")
                        {
                            objkeyPairs.Add(new KeyValuePair<string, string>("typeValue", (objForm1.LetterTranslation.Trim() != "" ? objForm1.LetterTranslation.Replace("'", "''") : objForm1.ControlText.Replace("'", "''"))));
                        }
                    }
                    else if (_symptomType == "/BOTOX, COSMETIC")
                    {
                        startIndex = dr["FindingDetail"].ToString().IndexOf('=');
                        endIndex = dr["FindingDetail"].ToString().IndexOf('<');
                        tempControlId = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("T", "");
                        FormControls objForm1 = new FormControls(tempControlId);
                        if (objForm1.ControlType == "N")
                        {
                            startIndex = dr["FindingDetail"].ToString().IndexOf('*');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('=');
                            string _columnName = dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''").Replace("-", "_");
                            startIndex = dr["FindingDetail"].ToString().IndexOf('<');
                            endIndex = dr["FindingDetail"].ToString().IndexOf('>');
                            objkeyPairs.Add(new KeyValuePair<string, string>(_columnName, dr["FindingDetail"].ToString().Substring(startIndex + 1, (endIndex - startIndex) - 1).Replace("'", "''")));
                        }
                        if (objForm1.ControlType == "B")
                        {
                            string _columnName = "typeValue";
                            objkeyPairs.Add(new KeyValuePair<string, string>(_columnName, objForm1.ControlText.Replace("'", "''")));
                        }
                    }
                }
            }
            objEyeContext.keyElement = objkeyPairs;
            return objEyeContext;
        }
    }
}
