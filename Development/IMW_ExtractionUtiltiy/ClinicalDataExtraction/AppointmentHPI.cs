﻿using System.Data.SqlClient;
using System.Reflection;

namespace ClinicalDataExtraction
{
    public class AppointmentHPI
    {
		public AppointmentHPI()
		{
			PropertyInfo[] properties = this.GetType().GetProperties();

			foreach (var propertyInfo in properties)
			{
				propertyInfo.SetValue(this, string.Empty);
			}
		}

        public string Symptom { get; set; }
        public string Finding { get; set; }
        public string _Condition { get; set; }
        public string _HowLong { get; set; }
        public string _PainType { get; set; }
        public string _WhatType { get; set; }
        public string _WhichEye { get; set; }
        public string _WhichSide { get; set; }
        public string _WhichEyeFlashes { get; set; }
        public string _WhichEyeBurning { get; set; }
        public string _WhenDiagnosed { get; set; }
        public string _WeightLossGain { get; set; }

        public void Add(string _appointmentId)
        {
            string _insertQry = "Insert into ExtractedHPIDetails(AppointmentId, Symptom, ButtonText, Condition, HowLong, PainType, WhatType, WhichEye, WhichEyeFlashes, WhichEyeBurning, WhichSide, WeightLossGain) Values ('" + _appointmentId + "', '"+ Symptom.Replace("'", "''") + "', '" + Finding.Replace("'", "''") + "', '" + _Condition.Replace("'", "''") + "', '" + _HowLong.Replace("'", "''") + "', '" + _PainType.Replace("'", "''") + "', '" + _WhatType.Replace("'", "''") + "', '" + _WhichEye.Replace("'", "''") + "','" + _WhichEyeFlashes.Replace("'", "''") + "','" + _WhichEyeBurning.Replace("'", "''") + "', '" + _WhichSide.Replace("'", "''") + "','" + _WeightLossGain.Replace("'", "''") + "')";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(_insertQry, con))
                {
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
        }
    }
}
