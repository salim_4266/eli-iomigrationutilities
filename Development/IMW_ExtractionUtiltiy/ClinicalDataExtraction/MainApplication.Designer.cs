﻿namespace ClinicalDataExtraction
{
    partial class MainApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.txtFromPatientId = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtToPatientId = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.lblTotalNoOfPatients = new System.Windows.Forms.Label();
			this.lblPatientProcessed = new System.Windows.Forms.Label();
			this.lblTotAppointment = new System.Windows.Forms.Label();
			this.lblAppointmentProcessed = new System.Windows.Forms.Label();
			this.lblCurrAppt = new System.Windows.Forms.Label();
			this.lblCurrPatient = new System.Windows.Forms.Label();
			this.lblExtrctionPrgs = new System.Windows.Forms.Label();
			this.pnlExtraction = new System.Windows.Forms.Panel();
			this.button2 = new System.Windows.Forms.Button();
			this.pnlExtraction.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Teal;
			this.label1.Location = new System.Drawing.Point(27, 33);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(306, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Welcome to IOPW Clinical Data Extraction";
			// 
			// txtFromPatientId
			// 
			this.txtFromPatientId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtFromPatientId.Location = new System.Drawing.Point(31, 92);
			this.txtFromPatientId.Name = "txtFromPatientId";
			this.txtFromPatientId.Size = new System.Drawing.Size(248, 20);
			this.txtFromPatientId.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Location = new System.Drawing.Point(27, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(94, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "From PatientId";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Location = new System.Drawing.Point(318, 72);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(80, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "To PatientId";
			// 
			// txtToPatientId
			// 
			this.txtToPatientId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtToPatientId.Location = new System.Drawing.Point(322, 92);
			this.txtToPatientId.Name = "txtToPatientId";
			this.txtToPatientId.Size = new System.Drawing.Size(248, 20);
			this.txtToPatientId.TabIndex = 3;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Teal;
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.Location = new System.Drawing.Point(601, 84);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(153, 32);
			this.button1.TabIndex = 5;
			this.button1.Text = "Extract Data";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(22, -2);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(0, 13);
			this.label4.TabIndex = 6;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(14, 48);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(104, 13);
			this.label5.TabIndex = 7;
			this.label5.Text = "Total No. of Patients";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(14, 77);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(92, 13);
			this.label6.TabIndex = 8;
			this.label6.Text = "Patient processed";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 135);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(125, 13);
			this.label7.TabIndex = 9;
			this.label7.Text = "Total No. of Appointment";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(13, 162);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(119, 13);
			this.label8.TabIndex = 10;
			this.label8.Text = "Appointment Processed";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(14, 191);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(115, 13);
			this.label9.TabIndex = 11;
			this.label9.Text = "Current Appointment Id";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(14, 103);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(89, 13);
			this.label10.TabIndex = 12;
			this.label10.Text = "Current Patient Id";
			// 
			// lblTotalNoOfPatients
			// 
			this.lblTotalNoOfPatients.AutoSize = true;
			this.lblTotalNoOfPatients.Location = new System.Drawing.Point(202, 48);
			this.lblTotalNoOfPatients.Name = "lblTotalNoOfPatients";
			this.lblTotalNoOfPatients.Size = new System.Drawing.Size(0, 13);
			this.lblTotalNoOfPatients.TabIndex = 13;
			// 
			// lblPatientProcessed
			// 
			this.lblPatientProcessed.AutoSize = true;
			this.lblPatientProcessed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPatientProcessed.ForeColor = System.Drawing.Color.Green;
			this.lblPatientProcessed.Location = new System.Drawing.Point(202, 77);
			this.lblPatientProcessed.Name = "lblPatientProcessed";
			this.lblPatientProcessed.Size = new System.Drawing.Size(0, 13);
			this.lblPatientProcessed.TabIndex = 14;
			// 
			// lblTotAppointment
			// 
			this.lblTotAppointment.AutoSize = true;
			this.lblTotAppointment.Location = new System.Drawing.Point(202, 134);
			this.lblTotAppointment.Name = "lblTotAppointment";
			this.lblTotAppointment.Size = new System.Drawing.Size(0, 13);
			this.lblTotAppointment.TabIndex = 15;
			// 
			// lblAppointmentProcessed
			// 
			this.lblAppointmentProcessed.AutoSize = true;
			this.lblAppointmentProcessed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAppointmentProcessed.ForeColor = System.Drawing.Color.Green;
			this.lblAppointmentProcessed.Location = new System.Drawing.Point(202, 163);
			this.lblAppointmentProcessed.Name = "lblAppointmentProcessed";
			this.lblAppointmentProcessed.Size = new System.Drawing.Size(0, 13);
			this.lblAppointmentProcessed.TabIndex = 16;
			// 
			// lblCurrAppt
			// 
			this.lblCurrAppt.AutoSize = true;
			this.lblCurrAppt.Location = new System.Drawing.Point(202, 190);
			this.lblCurrAppt.Name = "lblCurrAppt";
			this.lblCurrAppt.Size = new System.Drawing.Size(0, 13);
			this.lblCurrAppt.TabIndex = 17;
			// 
			// lblCurrPatient
			// 
			this.lblCurrPatient.AutoSize = true;
			this.lblCurrPatient.Location = new System.Drawing.Point(202, 103);
			this.lblCurrPatient.Name = "lblCurrPatient";
			this.lblCurrPatient.Size = new System.Drawing.Size(0, 13);
			this.lblCurrPatient.TabIndex = 18;
			// 
			// lblExtrctionPrgs
			// 
			this.lblExtrctionPrgs.AutoSize = true;
			this.lblExtrctionPrgs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblExtrctionPrgs.Location = new System.Drawing.Point(15, 16);
			this.lblExtrctionPrgs.Name = "lblExtrctionPrgs";
			this.lblExtrctionPrgs.Size = new System.Drawing.Size(156, 13);
			this.lblExtrctionPrgs.TabIndex = 19;
			this.lblExtrctionPrgs.Text = "Extraction is in Progress...";
			// 
			// pnlExtraction
			// 
			this.pnlExtraction.BackColor = System.Drawing.Color.LightGray;
			this.pnlExtraction.Controls.Add(this.lblExtrctionPrgs);
			this.pnlExtraction.Controls.Add(this.label4);
			this.pnlExtraction.Controls.Add(this.lblCurrPatient);
			this.pnlExtraction.Controls.Add(this.label5);
			this.pnlExtraction.Controls.Add(this.lblCurrAppt);
			this.pnlExtraction.Controls.Add(this.label6);
			this.pnlExtraction.Controls.Add(this.lblAppointmentProcessed);
			this.pnlExtraction.Controls.Add(this.label7);
			this.pnlExtraction.Controls.Add(this.lblTotAppointment);
			this.pnlExtraction.Controls.Add(this.label8);
			this.pnlExtraction.Controls.Add(this.lblPatientProcessed);
			this.pnlExtraction.Controls.Add(this.label9);
			this.pnlExtraction.Controls.Add(this.lblTotalNoOfPatients);
			this.pnlExtraction.Controls.Add(this.label10);
			this.pnlExtraction.Location = new System.Drawing.Point(30, 149);
			this.pnlExtraction.Name = "pnlExtraction";
			this.pnlExtraction.Size = new System.Drawing.Size(540, 228);
			this.pnlExtraction.TabIndex = 20;
			this.pnlExtraction.Visible = false;
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Teal;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.ForeColor = System.Drawing.Color.White;
			this.button2.Location = new System.Drawing.Point(601, 28);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(153, 32);
			this.button2.TabIndex = 21;
			this.button2.Text = "Image Section";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// MainApplication
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(869, 515);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.pnlExtraction);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtToPatientId);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtFromPatientId);
			this.Controls.Add(this.label1);
			this.Name = "MainApplication";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.MainApplication_Load);
			this.pnlExtraction.ResumeLayout(false);
			this.pnlExtraction.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFromPatientId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtToPatientId;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTotalNoOfPatients;
        private System.Windows.Forms.Label lblPatientProcessed;
        private System.Windows.Forms.Label lblTotAppointment;
        private System.Windows.Forms.Label lblAppointmentProcessed;
        private System.Windows.Forms.Label lblCurrAppt;
        private System.Windows.Forms.Label lblCurrPatient;
        private System.Windows.Forms.Label lblExtrctionPrgs;
        private System.Windows.Forms.Panel pnlExtraction;
        private System.Windows.Forms.Button button2;
    }
}

