﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicalDataExtraction
{
    public class MasterElementModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }
    }

    public class MasterElements
    {
        public List<MasterElementModel> Get()
        {
            List<MasterElementModel> _ObjectList = new List<MasterElementModel>();
            string _selectQry = "Select Id, Name, IsActive from dbo.ExtractedMasterElements with (nolock) Where IsActive = 1";
            DataTable dsSymptoms = new DataTable();
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                    {
                        ds.Fill(dsSymptoms);
                    }
                }
            }

            foreach (DataRow dr in dsSymptoms.Rows)
            {
                _ObjectList.Add(new MasterElementModel()
                {
                    Id = Convert.ToString(dr["Id"]),
                    Name = Convert.ToString(dr["Name"]),
                    IsActive = Convert.ToBoolean(dr["isActive"])
                });
            }

            return _ObjectList;
        }
    }
}
