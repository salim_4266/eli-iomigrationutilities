﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ClinicalDataExtraction
{
    public class FormControls
    {
        public string ControlId { get; set; }
        public string ControlText { get; set; }
        public string ControlType { get; set; }
        public string LetterTranslation { get; set; }

        public FormControls()
        {

        }

        public FormControls(string _fromControlId)
        {
            string _selectQry = "Select ControlText, ControlType, ControlId, LetterTranslation From df.FormsControls with (nolock) Where ControlId = '" + _fromControlId + "'";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        this.ControlId = reader["ControlId"].ToString();
                        this.ControlType = reader["ControlType"].ToString();
                        if (this.ControlType == "N")
                        {
                            this.ControlText = Regex.Replace(reader["ControlText"].ToString(), @"[^0-9a-zA-Z]+", "");
                            this.LetterTranslation = Regex.Replace(reader["LetterTranslation"].ToString(), @"[^0-9a-zA-Z]+", "");
                        }
                        else
                        {
                            this.ControlText = reader["ControlText"].ToString();
                            this.LetterTranslation = reader["LetterTranslation"].ToString();
                        }
                    }
                }
                con.Close();
            }
        }
    }
}
