﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicalDataExtraction
{
    public class DBInteraction
    {
        public DBInteraction()
        {
            string _path2Migrations = Application.StartupPath.Replace("\\bin\\Debug", "").Replace("\\bin\\Release", "") + "\\Migrations\\";
            DirectoryInfo _directory = new DirectoryInfo(_path2Migrations);
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                string _qryStatement = "";
                con.Open();
                foreach (FileInfo f in _directory.GetFiles())
                {
                    System.Threading.Thread.Sleep(500);
                    _qryStatement = File.ReadAllText(f.FullName);
                    using (SqlCommand cmd = new SqlCommand(_qryStatement, con))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
                con.Close();
            }
        }
    }
}
