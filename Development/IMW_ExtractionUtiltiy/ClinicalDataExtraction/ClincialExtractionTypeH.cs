﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace ClinicalDataExtraction
{
    public class ClincialExtractionTypeH
    {
        public static void Extarct_PMH_HPI_OCX(string _appointmentId)
        {
            List<SecondaryFormControlModel> SecondFormControlList = new SecondaryFormControls().Get();
            DataTable dsSymptoms = new DataTable();
            string _selectQry = "Select ClinicalId, Symptom, FindingDetail, ImageDescriptor From dbo.PatientClinical with(nolock) Where AppointmentId = '" + _appointmentId + "' and ClinicalType = 'H' and Symptom not like '%Family%'";
            using (SqlConnection con = new SqlConnection(Util._connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                    {
                        ds.Fill(dsSymptoms);
                    }
                }
            }

			if (dsSymptoms.Rows.Count > 0)
			{
				string _ColumnContent = "";
				int _index = 0;
				int _searchingIndex = 0;

				PastMedicalHistory objPMHTemp = new PastMedicalHistory();
				AppointmentHPI objHPITemp1 = new AppointmentHPI();
				OcularHistory objOHTemp2 = new OcularHistory();
				foreach (DataRow dr in dsSymptoms.Rows)
				{
					// Past Medical History
					if (dr["Symptom"].Equals("/DM, HTN, HEART PROBS, HI CHOL, CART/ART DISEASE, TEMP ARTERITIS, PREMATURITY, BIRTH PROBS, GENETIC PROBS, BRAIN PROBS") || dr["Symptom"].Equals("/STROKE,  CANCER,  HIV/AIDS,  HEPATITIS,  ALCOHOLISM,  ASTHMA,  ARTHRITIS, LYME, GALL BLADDER") || dr["Symptom"].Equals("/SICKLE CELL,  ULCERATED COLITIS,  EMOTIONAL PROBS,  SKIN CONDITIONS,  LUPUS,  THYROID,  ARTIFICIAL ORGAN,  PARKINSONS,  MS") || dr["Symptom"].Equals("/DIALYSIS, NUTRITIONAL DEFICIENCY, HEADACHES, HEARING PROBS, VOCAL PROBS, SMELL PROBS, PROBS W/TASTING, OTHER SURGERIES"))
					{
						_searchingIndex = 1;
						if (objPMHTemp != null && !string.IsNullOrEmpty(objPMHTemp.Symptom) && !string.IsNullOrEmpty(objPMHTemp.Finding))
						{
							objPMHTemp.Add(_appointmentId);
						}
						objPMHTemp = new PastMedicalHistory();
						if (dr["FindingDetail"].ToString().Contains("="))
						{
							_index = dr["FindingDetail"].ToString().IndexOf("=");
							_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
							if (tempControlId != "")
							{
								SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
								if (obj != null)
								{
									_ColumnContent = obj.ReplacedText.ToUpper();
								}
							}
						}
						else
						{
							_ColumnContent = dr["FindingDetail"].ToString();
						}
						objPMHTemp.Symptom = dr["Symptom"].ToString();
						objPMHTemp.Finding = _ColumnContent;
					}
					// History of Present Illness
					else if (dr["Symptom"].Equals("/LOSS OR CHANGE OF VISION") || dr["Symptom"].Equals("/PAIN OR IRRITATION") || dr["Symptom"].Equals("/DISCOLORATION OF EITHER EYE") || dr["Symptom"].Equals("/INJURY TO AFFECTED EYE") || dr["Symptom"].Equals("/PROBLEMS AROUND EYE OR EYELID") || dr["Symptom"].Equals("/CHEST PAIN,  COLD SORES,  DRY MOUTH, SHORTNESS OF BREATH,  WEIGHT CHANGE"))
					{
						_searchingIndex = 2;
						if (objHPITemp1 != null && !string.IsNullOrEmpty(objHPITemp1.Symptom) && !string.IsNullOrEmpty(objHPITemp1.Finding))
						{
							objHPITemp1.Add(_appointmentId);
						}
						objHPITemp1 = new AppointmentHPI();
						if (dr["FindingDetail"].ToString().Contains("="))
						{
							_index = dr["FindingDetail"].ToString().IndexOf("=");
							_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
							if (tempControlId != "")
							{
								SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
								if (obj != null)
								{
									_ColumnContent = obj.ReplacedText.ToUpper();
								}
							}
						}
						else
						{
							_ColumnContent = dr["FindingDetail"].ToString();
						}
						objHPITemp1.Symptom = dr["Symptom"].ToString();
						objHPITemp1.Finding = _ColumnContent;
					}
					// Ocular History
					else if (dr["Symptom"].Equals("/DO YOU WEAR GLASSES") || dr["Symptom"].Equals("/DO YOU WEAR CONTACTS") || dr["Symptom"].Equals("/CATARACT,  GLAUCOMA,  DIABETIC EYE DISEASE,  ARMD,  AMBLYOPIA,  CLOT,  EYE INJURY") || dr["Symptom"].Equals("/UVEITIS,  MUSCLE IMBALANCE,  FACIAL PALSY,  MALIG-GROWTH,  NON-MALIG-GROWTH,  PTOSIS,  EYE CANCER,  FACIAL PAIN,  CONJUN") || dr["Symptom"].Equals("/NIGHT BLINDNESS,  BLINDNESS,  COLOR BLINDNESS,  EYE SOCKET PROBLEMS,  INFLAMMATION ,  OPTIC NERVE BLOOD VESSEL DISEASE") || dr["Symptom"].Equals("/LASIK OR PRK SURGERY,  FOLLOW-UP SURGERY AFTER LASIK,  COSMETIC FACIAL SURGERY,  CORNEAL TRANSPLANT SURGERY"))
					{
						_searchingIndex = 3;
						if (objOHTemp2 != null && !string.IsNullOrEmpty(objOHTemp2.Symptom) && !string.IsNullOrEmpty(objOHTemp2.Finding))
						{
							objOHTemp2.Add(_appointmentId);
						}
						objOHTemp2 = new OcularHistory();
						if (dr["FindingDetail"].ToString().Contains("="))
						{
							_index = dr["FindingDetail"].ToString().IndexOf("=");
							_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
							if (tempControlId != "")
							{
								SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
								if (obj != null)
								{
									_ColumnContent = obj.ReplacedText.ToUpper();
								}
							}
						}
						else
						{
							_ColumnContent = dr["FindingDetail"].ToString();
						}
						objOHTemp2.Symptom = dr["Symptom"].ToString();
						objOHTemp2.Finding = _ColumnContent;
					}

					if (_searchingIndex == 1)
					{
						#region PastMedicalHistory
						if (dr["Symptom"].Equals("?AFFECT SIGHT"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._AffectSight = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._AffectSight = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._WhatType = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._WhatType = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE PAGE ONE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._WhatTypePageOne = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._WhatTypePageOne = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?TYPE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._Type = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._Type = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHEN DIAGNOSED"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._WhenDiagnosed = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._WhenDiagnosed = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?HAVE SURGERY") || dr["Symptom"].Equals("]HAVE SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._HaveSurgery = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._HaveSurgery = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN SURGERY") || dr["Symptom"].Equals("?WHEN SURGERY") || dr["Symptom"].Equals("]WHEN TRANSPLANT SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._WhenSurgery = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._WhenSurgery = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]COMPLICATIONS SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._ComplicationSurgery = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._ComplicationSurgery = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?COMPLICATIONS-DISEASE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._ComplicationDisease = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._ComplicationDisease = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?CONTROLLED"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._Controlled = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._Controlled = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHEN BEGUN"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._WhenBegun = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._WhenBegun = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
								string tempControlId = dr["FindingDetail"].ToString().Substring(_index + 1).Replace("T", "");
								if (tempControlId != "")
								{
									SecondaryFormControlModel obj = SecondFormControlList.FirstOrDefault(x => x.isActive && x.SecondaryFormId == tempControlId);
									if (obj != null)
									{
										_ColumnContent = obj.ReplacedText.ToUpper();
									}
								}
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objPMHTemp._QuestionMark = dr["ImageDescriptor"].ToString().Trim();
							else
								objPMHTemp._QuestionMark = _ColumnContent;
						}

						#endregion
					}
					if (_searchingIndex == 2)
					{
						#region History Of Present Illness
						if (dr["Symptom"].Equals("?CONDITION"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objHPITemp1._Condition = dr["ImageDescriptor"].ToString().Trim();
							else
								objHPITemp1._Condition = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?HOW LONG"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objHPITemp1._HowLong = dr["ImageDescriptor"].ToString().Trim();
							else
								objHPITemp1._HowLong = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?PAIN TYPE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objHPITemp1._PainType = dr["ImageDescriptor"].ToString().Trim();
							else
								objHPITemp1._PainType = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objHPITemp1._WhatType = dr["ImageDescriptor"].ToString().Trim();
							else
								objHPITemp1._WhatType = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHICH EYE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objHPITemp1._WhichEye = dr["ImageDescriptor"].ToString().Trim();
							else
								objHPITemp1._WhichEye = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHICH EYE  FLASHES"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objHPITemp1._WhichEyeFlashes = dr["ImageDescriptor"].ToString().Trim();
							else
								objHPITemp1._WhichEyeFlashes = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHICH EYE BLURRING"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objHPITemp1._WhichEyeBurning = dr["ImageDescriptor"].ToString().Trim();
							else
								objHPITemp1._WhichEyeBurning = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHICH SIDE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objHPITemp1._WhichSide = dr["ImageDescriptor"].ToString().Trim();
							else
								objHPITemp1._WhichSide = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WEIGHT LOSS GAIN"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objHPITemp1._WeightLossGain = dr["ImageDescriptor"].ToString().Trim();
							else
								objHPITemp1._WeightLossGain = _ColumnContent;
						}

						#endregion
					}
					if (_searchingIndex == 3)
					{
						#region Ocular History 
						if (dr["Symptom"].Equals("?HAVE CONJUNCTIVITIS-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HAVE_CONJUNCTIVITIS_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HAVE_CONJUNCTIVITIS_DID_WEAR = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?HAVE ULCERS-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HAVE_ULCERS_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HAVE_ULCERS_DID_WEAR = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?HOW LONG"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HOW_LONG = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HOW_LONG = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?HOW OFTEN WEAR-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HOW_OFTEN_WEAR_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HOW_OFTEN_WEAR_DID_WEAR = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?MEDICAL LENSES-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._MEDICAL_LENSES_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._MEDICAL_LENSES_DID_WEAR = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE-DID-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHAT_TYPE_DID_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHAT_TYPE_DID_WEAR = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHAT TYPE-DO-WEAR"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHAT_TYPE_DO_WEAR = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHAT_TYPE_DO_WEAR = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHEN"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHEN STOP"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_STOP = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_STOP = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHICH EYE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHICH_EYE_TYPE = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHICH_EYE_TYPE = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHICH KIND"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHICH_KIND = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHICH_KIND = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHICH SIDE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHICH_SIDE = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHICH_SIDE = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("?WHY STOP"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHY_STOP = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHY_STOP = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("[WHEN SURGERY LEFT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_SURGERY_LEFT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_SURGERY_LEFT_BOTH = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("[WHEN SURGERY LEFT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_SURGERY_LEFT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_SURGERY_LEFT_ONLY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("[WHEN SURGERY RIGHT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_SURGERY_RIGHT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_SURGERY_RIGHT_BOTH = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("[WHEN SURGERY RIGHT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_SURGERY_RIGHT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_SURGERY_RIGHT_ONLY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]COMPLICATIONS SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._COMPLICATIONS_SURGERY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._COMPLICATIONS_SURGERY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("[HAVE SURGERY LEFT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HAVE_SURGERY_LEFT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HAVE_SURGERY_LEFT_BOTH = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("[HAVE SURGERY LEFT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HAVE_SURGERY_LEFT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HAVE_SURGERY_LEFT_ONLY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("[HAVE SURGERY RIGHT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HAVE_SURGERY_RIGHT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HAVE_SURGERY_RIGHT_BOTH = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("[HAVE SURGERY RIGHT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HAVE_SURGERY_RIGHT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HAVE_SURGERY_RIGHT_ONLY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]HOW MANY TIMES SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HOW_MANY_TIMES_SURGERY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HOW_MANY_TIMES_SURGERY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]HOW OFTEN BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HOW_OFTEN_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HOW_OFTEN_BOTH = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]HOW OFTEN LEFT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HOW_OFTEN_LEFT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HOW_OFTEN_LEFT_BOTH = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]HOW OFTEN RIGHT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HOW_OFTEN_RIGHT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HOW_OFTEN_RIGHT_BOTH = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]HOW OFTEN RIGHT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._HOW_OFTEN_RIGHT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._HOW_OFTEN_RIGHT_ONLY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]PATCH RIGHT"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._PATCH_RIGHT = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._PATCH_RIGHT = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]TYPE EYE CANCER RIGHT"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._TYPE_EYE_CANCER_RIGHT = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._TYPE_EYE_CANCER_RIGHT = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN DIAGNOSED LEFT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_DIAGNOSED_LEFT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_DIAGNOSED_LEFT_BOTH = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN DIAGNOSED LEFT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_DIAGNOSED_LEFT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_DIAGNOSED_LEFT_ONLY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN DIAGNOSED RIGHT BOTH"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_DIAGNOSED_RIGHT_BOTH = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_DIAGNOSED_RIGHT_BOTH = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN DIAGNOSED RIGHT ONLY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_DIAGNOSED_RIGHT_ONLY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_DIAGNOSED_RIGHT_ONLY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN LAST  SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_LAST__SURGERY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_LAST__SURGERY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHEN SURGERY"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHEN_SURGERY = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHEN_SURGERY = _ColumnContent;
						}
						else if (dr["Symptom"].Equals("]WHICH EYE"))
						{
							if (dr["FindingDetail"].ToString().Contains("="))
							{
								_index = dr["FindingDetail"].ToString().IndexOf("=");
								_ColumnContent = dr["FindingDetail"].ToString().Substring(0, _index);
							}
							else
							{
								_ColumnContent = dr["FindingDetail"].ToString();
							}
							if (dr["ImageDescriptor"].ToString().Trim() != "")
								objOHTemp2._WHICH_EYE = dr["ImageDescriptor"].ToString().Trim();
							else
								objOHTemp2._WHICH_EYE = _ColumnContent;
						}

						#endregion
					}
				}

				if (objPMHTemp != null && !string.IsNullOrEmpty(objPMHTemp.Symptom) && !string.IsNullOrEmpty(objPMHTemp.Finding))
				{
					objPMHTemp.Add(_appointmentId);
				}

				if (objHPITemp1 != null && !string.IsNullOrEmpty(objHPITemp1.Symptom) && !string.IsNullOrEmpty(objHPITemp1.Finding))
				{
					objHPITemp1.Add(_appointmentId);
				}

				if (objOHTemp2 != null && !string.IsNullOrEmpty(objOHTemp2.Symptom) && !string.IsNullOrEmpty(objOHTemp2.Finding))
				{
					objOHTemp2.Add(_appointmentId);
				}
			}
        }

        public static void ExtractCheifComplaint(string _appointmentId)
        {
            try
            {
                string _insertQry = string.Empty;
                string _name = string.Empty;
                string _value = string.Empty;

                DataTable dtFindingDetail = new DataTable();
                string _selectQry = "Select * From dbo.PatientClinical Where ClinicalType = 'H' and Symptom like '%CHIEFCOMPLAINTS%' AND AppointmentId = '" + _appointmentId + "'";
                using (SqlConnection con = new SqlConnection(Util._connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                        {
                            ds.Fill(dtFindingDetail);
                        }
                    }
                }

                foreach (DataRow dr in dtFindingDetail.Rows)
                {
                    if (dr["Symptom"].ToString().Contains("/CHIEFCOMPLAINTS"))
                    {
                        _name = dr["Symptom"].ToString();
                        _value = dr["FindingDetail"].ToString();
                        _insertQry = "Insert into ExtractedCheifComplaintDetails(AppointmentId, Symptom, FindingDetail) Values ('" + _appointmentId + "', '" + _name + "', '" + _value + "')";
                        using (SqlConnection con = new SqlConnection(Util._connectionString))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand(_insertQry, con))
                            {
                                cmd.ExecuteNonQuery();
                            }
                            con.Close();
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public static void ExtractFamilyHistory(string _appointmentId)
        {
            try
            {
                DataTable dtFindingDetail = new DataTable();
                string _selectQry = "Select * From dbo.PatientClinical Where ClinicalType = 'H' and Symptom like '%Family%' AND AppointmentId = '" + _appointmentId + "'";
                using (SqlConnection con = new SqlConnection(Util._connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(_selectQry, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter ds = new SqlDataAdapter(cmd))
                        {
                            ds.Fill(dtFindingDetail);
                        }
                    }
                }

                string Relationship2Patient = "";
                string DiseaseCaused = "";
                string _insertQry = "";

                foreach (DataRow dr in dtFindingDetail.Rows)
                {
                    if (dr["Symptom"].ToString().Contains("?WHICH FAMILY"))
                    {
                        DiseaseCaused = dr["FindingDetail"].ToString();
                        if (DiseaseCaused.Contains("-"))
                        {
                            Relationship2Patient = DiseaseCaused.Split('-')[0];
                            DiseaseCaused = DiseaseCaused.Split('-')[1].Split('=')[0];
                        }
                        _insertQry = "Insert into dbo.ExtractedFamilyHistoryDetails(AppointmentId, Relationship2Patient, DiseaseCaused) Values ('" + _appointmentId + "', '" + Relationship2Patient + "', '" + DiseaseCaused + "')";
                        using (SqlConnection con = new SqlConnection(Util._connectionString))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand(_insertQry, con))
                            {
                                cmd.ExecuteNonQuery();
                            }
                            con.Close();
                        }
                    }
                }
            }
            catch
            {

            }
        }
    }
}
